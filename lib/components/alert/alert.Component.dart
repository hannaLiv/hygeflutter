import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'box/boxAlert.Component.dart';
import 'box/boxQuestionAlert.Component.dart';
// import 'package:vector_math/vector_math.dart' as math;

class Alert {
 static Future<bool> showAlertDialogConfirm(BuildContext context,String title,String content) async{
   bool flag = false;
  await showDialog(
      context: context,
      builder: (context) =>Center(
        child: BoxQuestionAlertComponent(
          title: title,
          description: content,
          onCanceled: (){
            flag=false;
            Navigator.of(context, rootNavigator: true).pop();
          },
          yes: (){
            flag=true;
            Navigator.of(context, rootNavigator: true).pop();
          },
        ),
      )
    );
  return flag;
}

  static Future<void> dialog(BuildContext context, String title, String content, {Function ?callback,String size="s"} ) async {
    Widget continueButton = FlatButton(
      child: Text("OK"),
      onPressed:  () {
        Navigator.of(context, rootNavigator: true).pop();
        //Navigator.of(context).pop();
      },
    );
    showDialog(
      context: context,
      builder: (context) => Center(
        child: BoxAlertComponent(
          title: title,
          description: content,
          style: size,
          onCanceled: (){Navigator.of(context, rootNavigator: true).pop();},
        )
      )
    );
  }
 }