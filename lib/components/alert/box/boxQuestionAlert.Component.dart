import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:twinkle_colors_flutter_app/components/buttons/button.Component.dart';


class BoxQuestionAlertComponent extends StatefulWidget {
  const BoxQuestionAlertComponent({ Key ?key,this.title,this.onCanceled,this.description,this.yes }) : super(key: key);
  final Function ?onCanceled;
  final String ?title;
  final String ?description;
  final Function ?yes;
  @override
  _BoxQuestionAlertComponentState createState() => _BoxQuestionAlertComponentState();
}

class _BoxQuestionAlertComponentState extends State<BoxQuestionAlertComponent> {

  double widthDefault = 0.8;
  double heightDefault =0.4;

   @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width*widthDefault,
      height: size.height*heightDefault,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            width: size.width,
            height: (size.height*heightDefault)-20,
            padding: EdgeInsets.only(
              top: 20,
              bottom: 5,
              left: 10,
              right: 10
            ),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.grey[50]!, Colors.white]
              ),
              border: Border.all(width: 3.0,color: Colors.white),
              color: Color.fromRGBO(242,244,247, 1),
              borderRadius: BorderRadius.circular(40.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(widget.title??"Thông báo",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey[600],fontSize: 21,fontWeight: FontWeight.w600),
                ),
                Expanded( 
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      width: double.infinity,
                      child: Text(widget.description??"no content",
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.clip,
                        style: TextStyle(color: Colors.grey[600],
                          fontWeight: FontWeight.w500,
                          fontSize: 18
                        ),
                      ),
                    ),
                  )
                ),
                Container(
                  margin: EdgeInsets.only(top: 10,bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ButtonComponent(
                        name: "Đồng ý",
                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                        type: 1, 
                        onPress: ()=>widget.yes!()
                      ),
                      ButtonComponent(
                        name: "Từ chối",
                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                        type: 0, 
                        onPress: ()=>widget.onCanceled!()
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 30,
            child: InkWell(
              onTap: (){
                widget.onCanceled!();
              },
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white,width: 1),
                  color: Color.fromRGBO(242,244,247,1),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      offset: Offset(6.0, 6.0),
                      blurRadius: 10.0,
                    ),
                  ],
                ),
                height: 50,
                width: 50,
                child: Center(
                  child: SvgPicture.asset("assets/images/icons/icon_cancel.svg"),
                ),
              ),
            )
           )
        ],
      ),
    );
  }
}