import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BoxAlertComponent extends StatefulWidget {
  const BoxAlertComponent({ Key ?key,this.onCanceled,this.style,this.title,this.description }) : super(key: key);
  final Function ?onCanceled;
  final String ?style;
  final String ?title;
  final String ?description;
  @override
  _BoxAlertComponentState createState() => _BoxAlertComponentState();
}

class _BoxAlertComponentState extends State<BoxAlertComponent> {
  double widthDefault = 0.9;
  double heightDefault =0.7;
  bool showImage=true;
  void getHeightSize(){
    switch(widget.style){
      case "s":
        this.widthDefault = 0.8;
        this.heightDefault = 0.3;
        this.showImage=false;
      break;
      case "m":
        this.widthDefault = 0.9;
        this.heightDefault = 0.6;
        this.showImage=true;
      break;
      case "l":
        this.widthDefault = 0.9;
        this.heightDefault = 0.7;
        this.showImage=true;
      break;
      default: break;
    }
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    getHeightSize();
    return Material(
      color: Colors.transparent,
      child: Container(
        width: size.width*widthDefault,
        height: size.height*heightDefault,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              width: size.width,
              height: (size.height*heightDefault)-20,
              padding: EdgeInsets.only(
                top: 20,
                bottom: 5,
                left: 10,
                right: 10
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [Colors.grey[50]!, Colors.white]
                ),
                border: Border.all(width: 3.0,color: Colors.white),
                color: Color.fromRGBO(242,244,247, 1),
                borderRadius: BorderRadius.circular(40.0),
                boxShadow: [
                  // BoxShadow(
                  //   color: Colors.white.withOpacity(1),
                  //   offset: Offset(-6.0, -6.0),
                  //   blurRadius: 10.0,
                  // ),
                  // BoxShadow(
                  //   color: Colors.black.withOpacity(0.1),
                  //   offset: Offset(6.0, 6.0),
                  //   blurRadius: 10.0,
                  // ),
                ],
              ),
              child: Column(
                children: [
                  Visibility(
                    visible: this.showImage,
                    child: Container(
                      child: Image.asset("assets/images/logo_hyge.png"),
                    ),
                  ),
                  Text(widget.title??"Thông báo",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.orange,fontSize: 21,fontWeight: FontWeight.w600),
                  ),
                  Expanded( 
                    child: SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 10.0),
                        width: double.infinity,
                        child: Text(widget.description??"no content",
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.clip,
                          style: TextStyle(color: Colors.grey[600],
                            fontWeight: FontWeight.w500,
                            fontSize: 18
                          ),
                        ),
                      ),
                    )
                  )
                ],
              ),
            ),
            Positioned(
              top: 0,
              right: 30,
              child: InkWell(
                onTap: (){
                  widget.onCanceled!();
                },
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white,width: 1),
                    color: Color.fromRGBO(242,244,247,1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        offset: Offset(6.0, 6.0),
                        blurRadius: 10.0,
                      ),
                    ],
                  ),
                  height: 50,
                  width: 50,
                  child: Center(
                    child: SvgPicture.asset("assets/images/icon_cancel.svg"),
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}