import 'package:flutter/material.dart';

class ButtonComponent extends StatefulWidget {
  const ButtonComponent({ Key? key,this.name="Xác nhận",required this.onPress,required this.padding,this.type=0,this.width=100}) : super(key: key);
  final double width;
  final EdgeInsetsGeometry padding;
  final String name;
  final int type;
  final Function onPress;
  @override
  _ButtonComponentState createState() => _ButtonComponentState();
}

class _ButtonComponentState extends State<ButtonComponent> {
  Color buttonStyle =Colors.blue;
  Color textColor = Colors.black;

  Color getColorButton(){
    textColor = Colors.white;
    switch(widget.type){
      //primary button
      case 0:
      return Colors.blue;
      //Secondary button
      case 1: return Colors.grey;
      //Success button
      case 2: return Colors.green;
      //Danger button
      case 3: return Colors.red[700]??Colors.red;
      //Warning button
      case 4: 
      return Colors.orange[600]??Colors.orange;
      //Info button
      case 5: return Colors.teal[600]??Colors.teal;
      //Light button
      case 6: 
        textColor = Colors.white;
        return Colors.white70;
      default: return Colors.black;
    }
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(widget.onPress!= null){
          widget.onPress();
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: getColorButton(),
          borderRadius: BorderRadius.all(Radius.circular(10.0))
        ),
        width: widget.width,
        padding: widget.padding,
        child: Center(
          child: Text(
            widget.name,
            style: TextStyle(
              color: textColor,fontSize: 20,fontWeight: FontWeight.w600
            ),
          ),
        ),
      ),
    );
  }
}