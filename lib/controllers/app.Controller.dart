import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/sanPhamBoSungDTO.Model.dart';
import 'package:twinkle_colors_flutter_app/models/thoiGianDTOs.Model.dart';
import 'package:twinkle_colors_flutter_app/models/tuyChonDTO.Model.dart';
import 'package:twinkle_colors_flutter_app/services/httpClient.dart';

class AppController extends GetxController{
  DichVuEntity ?serviceSelected;
  List<ThoiGianDTOsModel> ?listThoiGianDTO;
  List<SanPhamBoSungDTOModel> ?sanPhamBoSungDTO;
  List<TuyChonDTOModel> ?listTuyChonDTO;
  selectService(DichVuEntity ?serviceSelected){
    this.serviceSelected = serviceSelected;
    update();
  }

  Future<bool> getObjectBookingByIdService(int ?idService) async{
    bool result = false;
    try{
        var client = await getApiClient();
       if(client != null){
        var response = await client.get("/api/app/order/full-detail-order-page/"+idService.toString());
         if (response.statusCode == 200){
           this.listThoiGianDTO = ThoiGianDTOsModel.fromJsonList(response.data["thoiGianDTOs"]);
           this.sanPhamBoSungDTO = SanPhamBoSungDTOModel.fromJsonList(response.data["sanPhamBoSungDTOs"]);
            this.listTuyChonDTO =  TuyChonDTOModel.fromJsonList(response.data["tuyChonDTOs"]);
         }
         client.close(force: true);
       }
       else{
       }
    }
    catch(ex){
      print(ex);
    }
    return result;
  }
}