import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppShadow {
  static final boxShadow = [
    BoxShadow(
      color: Color.fromRGBO(104, 108, 143, 0.05),
      blurRadius: 20,
      spreadRadius: 0,
      offset: Offset(0, 0),
    ),
  ];
  static final redShadow = [
    BoxShadow(
      color: Color.fromRGBO(235, 28, 36, 0.13),
      spreadRadius: 15,
      blurRadius: 10,
    ),
  ];

  static final violetShadow = [
    BoxShadow(
      color: Color.fromRGBO(173, 142, 196, 0.2),
      spreadRadius: 0,
      offset: Offset(0, 10),
      blurRadius: 15,
    ),
  ];

  static final whiteShadow = [
    BoxShadow(
      color: Color.fromRGBO(160, 179, 203, 0.15),
      spreadRadius: 0,
      offset: Offset(0, 10),
      blurRadius: 30,
    ),
  ];

  /// Home Reason
  static final whiteReasonShadow = [
    BoxShadow(
      color: Color.fromRGBO(119, 137, 160, 0.1),
      spreadRadius: 0,
      offset: Offset(0, 10),
      blurRadius: 30,
    ),
  ];
  static final plusButtonShadow = [
    BoxShadow(
      color: Color.fromRGBO(173, 142, 196, 0.2),
      spreadRadius: 0,
      offset: Offset(0, 10),
      blurRadius: 15,
    ),
  ];

  /// Life style item shadow
  static final lifeStyleItemShadow = [
    BoxShadow(
      color: Color.fromRGBO(160, 179, 203, 0.15),
      spreadRadius: 0,
      offset: Offset(0, 10),
      blurRadius: 30,
    ),
  ];
  /// Home image select shadow
  static final homeImageSelectShadow = [
    BoxShadow(
      color: Color.fromRGBO(119, 137, 160, 0.1),
      spreadRadius: 0,
      offset: Offset(0, 10),
      blurRadius: 30,
    ),
  ];
}
