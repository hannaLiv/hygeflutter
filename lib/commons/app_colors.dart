import 'dart:ui';

import 'package:flutter/cupertino.dart';

class AppColors {
  ///Common
  static const Color primary = Color(0xFF1a222d);
  static const Color secondary = Color(0xFFd74315);
  static const Color accent = Color(0xFFd74315);
  static const Color purple = Color(0xff5c4db1);
  static const Color pink = Color(0xffdc4f89);

  ///Background
  static const Color background = Color(0xFFFFFFFF);
  static const Color backgroundLighter = Color(0xFF1f2837);
  static const Color backgroundDarker = Color(0xff111821);
  static const Color backgroundBlack = Color(0xFF221F1F);
  static const Color textBackground = Color(0xFFF8F9FC);
  static const Color checkBoxBackground = Color(0xFFC5ADD7);
  static const Color blueBackground = Color(0xFF72D2F4);
  static const Color violetBackground = Color(0xFF9F73BF);
  static const Color violetAD8EC4 = Color(0xFFAD8EC4);
  static const Color backgroundGreen00BC62 = Color(0xFF00BC62);
  static const Color backgroundRedFA0202 = Color(0xFFFA0202);
  static const LinearGradient gradientBackground = LinearGradient(
    colors: [background, Color(0xFFF4F7FA)],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
  static const LinearGradient cameraScanGradientBackground = LinearGradient(
    colors: [Color(0xFFCCA9E6), Color(0xFFB18BCC)],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
  static const LinearGradient galleryScanGradientBackground = LinearGradient(
    colors: [Color(0xFFBA91D9), Color(0xFF9D68C4)],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );

  ///Shadow
  static const Color shadow = Color(0x25606060);

  ///Border
  static const Color border = Color(0xFF606060);
  static const Color borderBlack = Color(0xFF221F1F);

  ///Divider
  static const Color divider = Color(0xFF606060);

  ///Text
  static const Color textWhite = Color(0xFFFFFFFF);
  static const Color textBlack221F1F = Color(0xFF221F1F);
  static const Color textBlack343434 = Color(0xFF343434);
  static const Color textGrey9099B0 = Color(0xFF9099B0);
  static const Color textWhiteB2B9CB = Color(0xFFB2B9CB);
  static const Color textE9ECF3 = Color(0xFFE9ECF3);
  static const Color textF8F9FC = Color(0xFFF8F9FC);
  static const Color textVioletAD8EC4 = Color(0xFFAD8EC4);

  ///TextField
  static const Color textFieldEnabledBorder = Color(0xFF919191);
  static const Color textFieldFocusedBorder = Color(0xFFd74315);
  static const Color textFieldDisabledBorder = Color(0xFF919191);
  static const Color textFieldCursor = Color(0xFF919191);

  ///Button
  static const Color buttonBGWhite = Color(0xFFcdd0d5);
  static const Color buttonBGTint = Color(0xFFd74315);
  static const Color buttonBorder = Color(0xFFd74315);
  static const Color buttonBGRed = Color(0xFFF85757);
  static const Color buttonViolet = Color(0xFFAD8EC4);
  static const Color buttonLightViolet = Color(0xFFC5ADD7);

  /// Tabs
  static const Color imageBG = Color(0xFF919191);

  ///BottomNavigationBar
  static const Color bottomNavigationBar = Color(0xFF919191);

  /// Other Colors
  static const Color lightGreyColor = Color(0xFF7583CA);
  static const Color greyColor = Color(0xFFC2CCD8);
  static const Color indicatorRedColor = Color(0xFFFF6161);
  static const Color lightVioletColor = Color(0xFF9099B0);
  static const Color greyTextColor = Color(0xFFB2B9CB);
  static const Color greyBgColor = Color(0xFFF1F4F9);
  static const Color pinkTextColor = Color(0xFFFFB1B4);
  static const Color greySettingColor = Color(0xFF343434);
  static const Color greyItemColor = Color(0xFFE9ECF3);
  static const Color disableBorder = Color(0xFFE5EAF3);
  static const Color whiteColor = Color(0xFFFFFFFF);

  /// Star color
  static const Color blueStar = Color(0xFF11CBBD);
  static const Color greenStar = Color(0xFF00BC62);
  static const Color yellowStar = Color(0xFFF0E924);
  static const Color orangeStar = Color(0xFFF08E57);
  static const Color lightBrownStar = Color(0xFFE0B99C);
  static const Color brownStar = Color(0xFFC19778);
  static const Color lightBlueStar = Color(0xFF8FE9EF);
  static const Color darkBlueStar = Color(0xFF6383C2);
}
