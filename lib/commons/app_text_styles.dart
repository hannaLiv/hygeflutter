import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

import 'app_colors.dart';

class AppTextStyle {
  ///Black
  static final black = TextStyle(color: AppColors.textBlack221F1F);

  //s10
  static final blackS10 = black.copyWith(fontSize: 10);
  static final blackS10Bold = blackS10.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS10W500 = blackS10.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);

  //s12
  static final blackS12 = black.copyWith(fontSize: 12);
  static final blackS12Bold = blackS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS12W400 = blackS12.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final blackS12W600 = blackS12.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final blackS12W800 = blackS12.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s13
  static final blackS13 = black.copyWith(fontSize: 13);
  static final blackS13Bold = blackS13.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS13W500 = blackS13.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final blackS13W600 = blackS13.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s14
  static final blackS14 = black.copyWith(fontSize: 14);
  static final blackS14Bold = blackS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS14W600 = blackS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final blackS14W800 = blackS14.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s15
  static final blackS15 = black.copyWith(fontSize: 15);
  static final blackS15Bold = blackS15.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS15W400 = blackS15.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);

  //s16
  static final blackS16 = black.copyWith(fontSize: 16);
  static final blackS16Bold = blackS16.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS16W600 = blackS16.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final blackS16W800 = blackS16.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s18
  static final blackS18 = black.copyWith(fontSize: 18);
  static final blackS18Bold = blackS18.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS18W600 = blackS18.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final blackS18W800 = blackS18.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s20
  static final blackS20 = black.copyWith(fontSize: 20);
  static final blackS20W600 = blackS20.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s22
  static final blackS22 = black.copyWith(fontSize: 22);
  static final blackS22Bold = blackS22.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS22W600 = blackS22.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final blackS22W800 = blackS22.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s24
  static final blackS24 = black.copyWith(fontSize: 24);
  static final blackS24Bold = blackS24.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final blackS24W300 = blackS24.copyWith(fontWeight: FontWeight.w300, fontFamily: AppThemes.appFont);
  static final blackS24W600 = blackS24.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final blackS24W800 = blackS24.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  ///White
  static final white = TextStyle(color: AppColors.textWhite);

  //s12
  static final whiteS12 = white.copyWith(fontSize: 12);
  static final whiteS12Bold = whiteS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final whiteS12W400 = whiteS12.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final whiteS12W600 = whiteS12.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final whiteS12W800 = whiteS12.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s13
  static final whiteS13 = white.copyWith(fontSize: 13);
  static final whiteS13Bold = whiteS13.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final whiteS13W500 = whiteS13.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final whiteS13W600 = whiteS13.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s14
  static final whiteS14 = white.copyWith(fontSize: 14);
  static final whiteS14Bold = whiteS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final whiteS14W400 = whiteS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final whiteS14W500 = whiteS14.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final whiteS14W600 = whiteS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final whiteS14W800 = whiteS14.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s16
  static final whiteS16 = white.copyWith(fontSize: AppDimens.fontLarge16);
  static final whiteS16Bold = whiteS16.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final whiteS16W600 = whiteS16.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final whiteS16W800 = whiteS16.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s18
  static final whiteS18 = white.copyWith(fontSize: 18);
  static final whiteS18Bold = whiteS18.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final whiteS18W600 = whiteS18.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final whiteS18W800 = whiteS18.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s20
  static final whiteS20 = white.copyWith(fontSize: 20);
  static final whiteS20W600 = whiteS20.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s22
  static final whiteS22 = white.copyWith(fontSize: 22);
  static final whiteS22Bold = whiteS22.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final whiteS22W600 = whiteS22.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final whiteS22W800 = whiteS22.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s24
  static final whiteS24 = white.copyWith(fontSize: 24);
  static final whiteS24W300 = whiteS24.copyWith(fontWeight: FontWeight.w300, fontFamily: AppThemes.appFont);
  static final whiteS24W600 = whiteS24.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s25
  static final whiteS25 = white.copyWith(fontSize: 25);
  static final whiteS25W600 = whiteS25.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  /// NearBlack
  static final nearBlack = TextStyle(color: AppColors.textBlack343434);

  //s11
  static final nearBlackS11 = nearBlack.copyWith(fontSize: 11);
  static final nearBlackS11W500 = nearBlackS12.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);

  //s12
  static final nearBlackS12 = nearBlack.copyWith(fontSize: 12);
  static final nearBlackS12Bold = nearBlackS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final nearBlackS12W400 = nearBlackS12.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final nearBlackS12W600 = nearBlackS12.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final nearBlackS12W800 = nearBlackS12.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s13
  static final nearBlackS13 = nearBlack.copyWith(fontSize: 13);
  static final nearBlackS13Bold = nearBlackS13.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final nearBlackS13W600 = nearBlackS13.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final nearBlackS13W600UnderLine = nearBlackS13.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont, decoration: TextDecoration.underline);

  //s14
  static final nearBlackS14 = nearBlack.copyWith(fontSize: 14);
  static final nearBlackS14Bold = nearBlackS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final nearBlackS14W400 = nearBlackS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final nearBlackS14W600 = nearBlackS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final nearBlackS14W800 = nearBlackS14.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s15
  static final nearBlackS15 = nearBlack.copyWith(fontSize: 15);
  static final nearBlackS15W400 = nearBlackS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);

  //s16
  static final nearBlackS16 = nearBlack.copyWith(fontSize: 16);
  static final nearBlackS16Bold = nearBlackS16.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final nearBlackS16W600 = nearBlackS16.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s18
  static final nearBlackS18 = nearBlack.copyWith(fontSize: 18);
  static final nearBlackS18Bold = nearBlackS18.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final nearBlackS18W400 = nearBlackS18.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final nearBlackS18W600 = nearBlackS18.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final nearBlackS18W800 = nearBlackS18.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  ///Gray
  static final grey = TextStyle(color: AppColors.textGrey9099B0);

  //s12
  static final greyS12 = grey.copyWith(fontSize: 12);
  static final greyS12Bold = greyS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final greyS12W400 = greyS12.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final greyS12W600 = greyS12.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final greyS12W800 = greyS12.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s13
  static final greyS13 = grey.copyWith(fontSize: 13);
  static final greyS13Bold = greyS13.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final greyS13W400 = greyS13.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final greyS13W600 = greyS13.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final greyS13W800 = greyS13.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s14
  static final greyS14 = grey.copyWith(fontSize: 14);
  static final greyS14Bold = greyS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final greyS14W400 = greyS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final greyS14W500 = greyS14.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final greyS14W600 = greyS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final greyS14W800 = greyS14.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s15
  static final greyS15 = grey.copyWith(fontSize: 15);
  static final greyS15Bold = greyS15.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final greyS15W400 = greyS15.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final greyS15W500 = greyS15.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);

  //s16
  static final greyS16 = grey.copyWith(fontSize: 16);
  static final greyS16Bold = greyS16.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final greyS16W500 = greyS16.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final greyS16W600 = greyS16.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final greyS16W800 = greyS16.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s18
  static final greyS18 = grey.copyWith(fontSize: 18);
  static final greyS18Bold = greyS18.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final greyS18W800 = greyS18.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  /// Text sliver
  static final sliver = TextStyle(color: AppColors.textWhiteB2B9CB);

  //s12
  static final sliverS12 = grey.copyWith(fontSize: 12);
  static final sliverS12Bold = sliverS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final sliverS12W400 = sliverS12.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final sliverS12W600 = sliverS12.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final sliverS12W800 = sliverS12.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  ///  violet
  static final textViolet = TextStyle(color: AppColors.textVioletAD8EC4);

  //s12
  static final textVioletS12 = textViolet.copyWith(fontSize: 12);
  static final textVioletS12Bold = textVioletS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final textVioletS12W400 = textVioletS12.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final textVioletS12W600 = textVioletS12.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s13
  static final textVioletS13 = textViolet.copyWith(fontSize: 13);
  static final textVioletS13Bold = textVioletS13.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final textVioletS13W500 = textVioletS13.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final textVioletS13W600 = textVioletS13.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s14
  static final textVioletS14 = textViolet.copyWith(fontSize: 14);
  static final textVioletS14Bold = textVioletS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final textVioletS14W400 = textVioletS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final textVioletS14W500 = textVioletS14.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final textVioletS14W600 = textVioletS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s15
  static final textVioletS15 = textViolet.copyWith(fontSize: 15);
  static final textVioletS15Bold = textVioletS15.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final textVioletS15W400 = textVioletS15.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);

  //s16
  static final textVioletS16 = textViolet.copyWith(fontSize: 16);
  static final textVioletS16Bold = textVioletS15.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final textVioletS16W600 = textVioletS15.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s18
  static final textVioletS18 = textViolet.copyWith(fontSize: 18);
  static final textVioletS18Bold = textVioletS18.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final textVioletS18W500 = textVioletS18.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);

  ///Tint
  static final tint = TextStyle(color: AppColors.secondary);

  //s12
  static final tintS12 = tint.copyWith(fontSize: 12);
  static final tintS12Bold = tintS12.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final tintS12W800 = tintS12.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s14
  static final tintS14 = tint.copyWith(fontSize: 14);
  static final tintS14Bold = tintS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final tintS14W800 = tintS14.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s16
  static final tintS16 = tint.copyWith(fontSize: 16);
  static final tintS16Bold = tintS16.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final tintS16W800 = tintS16.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  //s18
  static final tintS18 = tint.copyWith(fontSize: 18);
  static final tintS18Bold = tintS18.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final tintS18W800 = tintS18.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);

  /// Red
  static final redText = TextStyle(color: AppColors.buttonBGRed);

  //s12
  static final redTextS12 = redText.copyWith(fontSize: 12);
  static final redTextS12W400 = redTextS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);

  //s13
  static final redTextS13 = redText.copyWith(fontSize: 13);
  static final redTextS13W500 = redTextS13.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);

  //s14
  static final redTextS14 = redText.copyWith(fontSize: 14);
  static final redTextS14W600 = redTextS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  //s15
  static final redTextS15 = redText.copyWith(fontSize: 15);
  static final redTextS15W500 = redTextS15.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);

  //s16
  static final redTextS16 = redText.copyWith(fontSize: 16);
  static final redTextS16W400 = redTextS16.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final redTextS16W500 = redTextS16.copyWith(fontWeight: FontWeight.w500, fontFamily: AppThemes.appFont);
  static final redTextS16W600 = redTextS16.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);

  ///Gray
  static final orange = TextStyle(color: AppColors.accent);

  //s14
  static final orangeS14 = orange.copyWith(fontSize: 14);
  static final orangeS14Bold = orangeS14.copyWith(fontWeight: FontWeight.bold, fontFamily: AppThemes.appFont);
  static final orangeS14W400 = orangeS14.copyWith(fontWeight: FontWeight.w400, fontFamily: AppThemes.appFont);
  static final orangeS14W600 = orangeS14.copyWith(fontWeight: FontWeight.w600, fontFamily: AppThemes.appFont);
  static final orangeS14W800 = orangeS14.copyWith(fontWeight: FontWeight.w800, fontFamily: AppThemes.appFont);
}
