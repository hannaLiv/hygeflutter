class AppDimens {
  AppDimens._(); // this basically makes it so you can instantiate this class
  ///TextFontSize
  static const double fontSmaller = 11.0;
  static const double fontMaxSmall = 10.0;
  static const double fontSmall12 = 12.0;
  static const double fontNormal14 = 14.0;
  static const double fontLarge16 = 16.0;
  static const double fontExtra18 = 18.0;
  static const double fontExtra22 = 22.0;

  static const double buttonHeight = 48;
  static const double buttonCornerRadius50 = 50;
  static const double buttonCornerRadius38 = 38;
  static const double buttonCornerRadius90 = 90;
  static const double cornerRadius15 = 15;
  static const double cornerRadius20 = 20;
  static const double buttonBorderWidth = 1;

  static const double appBarHeight = 56;

  /// Padding and Margin
  static const double paddingNormal = 25;
  static const double paddingSmall = 13;
  static const double padding15 = 15;
  static const double padding18 = 18;
  static const double paddingEdge = 16;

  static const double marginNormal = 20;
  static const double marginLarge = 32;
  static const double marginSmall = 10;

  /// Other size
  static const double marginItem = 18;
  static const double marginLeftBar = 16;
  static const double marginMin = 12;
  static const double divider = 1;

  /// Logo size
  static const double logoWidth = 91;
  static const double logoHeight = 70;
}
