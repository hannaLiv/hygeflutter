
import 'package:dio/dio.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:twinkle_colors_flutter_app/databases/secure_storage_helper.dart';


String csrfToken = "";
dynamic requestInterceptor( RequestOptions options, RequestInterceptorHandler handler) 
{
  // getToken().then((value) 
  // {
  //   if (value!= null && value!="") {
  //     options.headers["Authorization"] = "Bearer " + value;
  //   }
  //   return handler.next(options);
  // });
  SecureStorageHelper.getInstance().getToken().then((value){
    if (value!= null && value.accessToken!="") {
      options.headers["Authorization"] = "Bearer " + value.accessToken!;
    }
    return handler.next(options);
  });
}

dynamic responseInterceptor(Response response,ResponseInterceptorHandler handler) {
  try{
    if (response.data!=null && response.data["sysCode"] != null && response.data["sysCode"] == "1") {
      if (response.data["sysName"]!=null&& response.data["sysName"].toString().contains("syssection")) {
        //Alert.toastShow("Xác thực hết hạn, vui lòng đăng nhập lại!", Colors.red);
        //saveToken("");
        //navService.pushNamedAndRemoveUntil('/welcome', args: 'From every where');
      }
    }
  }
  catch(ex)
  {
    print("Lỗi http:"+ex.toString());
  }
  return handler.next(response); // continue
}

dynamic errorInterceptor(DioError dioError,ErrorInterceptorHandler handler) {
  if (dioError.response?.statusCode == 403) {
  } else if (dioError.response?.statusCode == 401) {
    //Alert.toastShow("Xác thực hết hạn, vui lòng đăng nhập lại!", Colors.red);
    //saveToken("");
    //navService.pushNamedAndRemoveUntil('/login', args: 'From every where');
  } 
  else if(dioError.response?.statusCode == 503){
    //Alert.toastShow(dioError.response.statusMessage, Colors.red);
  }
  handler.next(dioError);
}

// BaseOptions option = new BaseOptions(
//     connectTimeout: AppServiceConfig.connectTimeout,
//     receiveTimeout: AppServiceConfig.receiveTimeout,
//     baseUrl: AppServiceConfig.serverURL,
//     headers: {
//       "Content-Type": "application/json",
//     });
  
BaseOptions setupOptionDio(String serverURL){
  return new BaseOptions(
    //connectTimeout: AppServiceConfig.connectTimeout,
    //receiveTimeout: AppServiceConfig.receiveTimeout,
    baseUrl: serverURL,
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Accept":"*/*",
      "Access-Control-Allow-Origin": "*",
      "Connection":"keep-alive"
    });
}

Future<Dio> getApiClient() async 
{
  var result = true; //await checkInternetConnection();
  if (result) 
  {
    Dio dio = new Dio(setupOptionDio(AppConfigs.baseUrl));
    dio.interceptors.clear();
    dio.interceptors.add(InterceptorsWrapper(
        onRequest:(RequestOptions options, RequestInterceptorHandler handler)=> requestInterceptor(options,handler),
        onResponse: (Response response,ResponseInterceptorHandler handler)=>responseInterceptor(response,handler),
        onError: (DioError e, handler)=>errorInterceptor(e,handler)));
    return dio;
  } else {
    //Alert.toastShow("Vui lòng kết nối với internet!", Colors.red);
    return new Dio();
  }
}
