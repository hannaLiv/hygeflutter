import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_infor_response.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_information_follow_response.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_response.dart';
import 'package:twinkle_colors_flutter_app/networks/api_client.dart';
import 'package:twinkle_colors_flutter_app/networks/api_util.dart';

part 'auth_api.dart';

class ApiService extends GetxService {
  late ApiClient _apiClient;

  Future<ApiService> init() async {
    _apiClient = ApiUtil.getApiClient();
    return this;
  }
}
