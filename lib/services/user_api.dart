part of 'user_api_service.dart';

extension UserApi on UserApiService {
  /// Get user information for first sign up
  Future<UserInformationResponse> getUserInformationForSignUp() async {
    return await _apiClient.getUserInformationForStep();
  }

  /// Update user avatar
  Future<UserInformationEntity> uploadUserAvatar(XFile file) async {
    return await _apiClient.uploadAvatar(MyFormData.FormData.fromMap({
      "image": MyFormData.MultipartFile.fromFile(
        file.path,
      ),
    }));
  }

  /// Get Categories
  Future<CategoriesResponses> getCategories({int? skipCount, int? maxResultCount, String? sorting}) async {
    return await _apiClient.getCategories({
      "SkipCount": skipCount,
      "MaxResultCount": maxResultCount,
      "Sorting": sorting,
    });
  }

  /// Get Category by id
  Future<UserInformationEntity> getCategoryById({int? id}) async {
    return await _apiClient.getCategoryById(id);
  }

  /// Get DichVus
  Future<DichVuResponses> getDichVus({int? skipCount, int? maxResultCount, String? sorting}) async {
    return await _apiClient.getDichVus({
      "SkipCount": skipCount,
      "MaxResultCount": maxResultCount,
      "Sorting": sorting,
    });
  }

  /// Get Combos
  Future<DichVuResponses> getCombos({int? skipCount, int? maxResultCount, String? sorting}) async {
    return await _apiClient.getCombos({
      "SkipCount": skipCount,
      "MaxResultCount": maxResultCount,
      "Sorting": sorting,
    });
  }

  /// Get DichVuDetail by id
  Future<DichVuDetailResponses> getDichVuDetails({int? id}) async {
    return await _apiClient.getDichVuDetails(id);
  }

  /// Get Products
  Future<DichVuResponses> getProducts() async {
    return await _apiClient.getProducts();
  }

  /// Get Products with id
  Future<DichVuResponses> getProductsWithId(int id) async {
    return await _apiClient.getProductsWithId(id);
  }

  /// Get Products with id
  Future<TokenEntity> getUserInfo() async {
    return await _apiClient.getUserInfo();
  }

  /// Order
  Future<bool> sendOrder(Map<String, dynamic> body) async {
    return await _apiClient.pushOrder(body);
  }
}
