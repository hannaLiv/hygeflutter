import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/databases/secure_storage_helper.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';

class AuthService extends GetxService {
  Rxn<TokenEntity> token = Rxn<TokenEntity>();

  // Rxn<UserEntity> user = Rxn<UserEntity>();

  Future<AuthService> init() async {
    /// Get current token
    final currentToken = await SecureStorageHelper.getInstance().getToken();
    token.value = currentToken;
    return this;
  }

  /// Handle save/remove Token
  void saveToken(TokenEntity token) {
    return SecureStorageHelper.getInstance().saveToken(token);
  }

  void removeToken() {
    return SecureStorageHelper.getInstance().removeToken();
  }

  /// User
  void updateUser(TokenEntity user) {
    this.token.value = user;
  }

  void deleteUser() {
    this.token.value = null;
  }

  /// SignOut
  void signOut() async {
    removeToken();
    //deleteUser();
  }
}
