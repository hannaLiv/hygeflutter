import 'package:dio/dio.dart' as MyFormData;
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';
import 'package:twinkle_colors_flutter_app/models/responses/categories_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_detail_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_information_response.dart';
import 'package:twinkle_colors_flutter_app/networks/api_client.dart';
import 'package:twinkle_colors_flutter_app/networks/api_util.dart';

part 'user_api.dart';

class UserApiService extends GetxService {
  late ApiClient _apiClient;

  Future<UserApiService> init() async {
    _apiClient = ApiUtil.getApiClient();
    return this;
  }
}
