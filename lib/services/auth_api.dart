part of 'api_service.dart';

extension AuthApiService on ApiService {
  /// Sign in with Customer account
  Future<TokenEntity?> signInWithCustomerAccount(String username, String password) async {
    return _apiClient.signInWithCustomerAccount({
      "username": username,
      "password": password,
    });
  }

  /// Sign in with Google
  Future<TokenEntity?> signInWithGoogle(String provider, String idToken) async {
    return _apiClient.signInWithGoogle({
      "provider": provider,
      "idToken": idToken,
    });
  }

  /// Sign in with Facebook
  Future<TokenEntity?> signInWithFacebook(String provider, String idToken) async {
    return _apiClient.signInWithFacebook({
      "provider": provider,
      "idToken": idToken,
    });
  }

  /// Sign in with Facebook
  Future<TokenEntity?> signInWithFirebase(String userId) async {
    return _apiClient.signInWithFirebase({
      "userId": userId,
    });
  }

  /// Sign up with Customer account
  Future<TokenEntity?> signUpWithCustomerAccount(String emailAddress, String username, String password, String phoneNumber, String maGioiThieu, String surname) async {
    return _apiClient.signUpWithCustomerAccount({
      'userName': username,
      "emailAddress": emailAddress,
      "passWord": password,
      "phoneNumber":phoneNumber,
      "maGioiThieu":maGioiThieu,
      "surname":surname,
    });
  }

  /// Sign up with Customer account
  Future<TokenEntity?> checkOTPSignIn(String phoneNumber, String confirmToken) async {
    return _apiClient.checkOTPSignIn({
      'phoneNumber': phoneNumber,
      "confirmToken": confirmToken
    });
  }

  /// Get info user by id
  Future<UserInfoResponse> getUserInformationById() async {
    return _apiClient.getUserInformationById();
  }

  /// Update info user by id
  Future<bool> updateUserInformationById(
    Map<String, dynamic> infoUser,
  ) async {
    final result = await _apiClient.updateUserInformationById(infoUser);
    return result;
  }

  /// Get user information follow
  Future<UserInformationFollow> getUserInFormationFollow() async {
    return _apiClient.getUserInformationFollow();
  }
}
