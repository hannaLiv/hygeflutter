import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/sign_in_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/otp/otp_screen.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/splash/splash_page.dart';

class RoutesConfig {
  ///main_page page
  static final String splash = "/splash";
  static final String main = "/main_page";
  static final String signIn = "/sign_in";
  static final String otp = "/otp";

  static final List<GetPage> getPages = [
    GetPage(name: splash, page: () => SplashPage()),
    GetPage(name: main, page: () => MainPage()),
    GetPage(name: signIn, page: () => SignInPage()),
    GetPage(name: otp, page: () => OtpScreen()),
  ];
}
