import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/pickers/image_picker.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/pickers/picker_number.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/pickers/picker_text.dart';

typedef OnCallBackDateSelected(DateTime dateSelected);
typedef OnImagePicked(XFile image);

class PickerUtils {
  static void showDateTimePicker(
    BuildContext context, {
    OnCallBackDateSelected? onDateSelected,
    DateTime? currentTime,
  }) {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: AppConfigs.birthMinDate,
      maxTime: AppConfigs.birthMaxDate,
      theme: DatePickerTheme(
        headerColor: AppColors.background,
        itemHeight: 20,
        itemStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
        doneStyle: TextStyle(color: Colors.black, fontSize: 16),
      ),
      onConfirm: (date) => onDateSelected!(date),
      currentTime: currentTime ?? DateTime.now(),
    );
  }

  static void showPickerText(
    BuildContext context, {
    required List<String> itemsString,
    int? selectedIndex,
    ItemPicked? onItemSelected,
    double? itemExtent,
  }) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return PickerText(
            itemsString,
            itemExtent: itemExtent,
            onItemSelected: onItemSelected,
            selectedIndex: selectedIndex,
          );
        });
  }

  static void showPickerNumber(
    BuildContext context, {
    int? startNumber,
    int? endNumber,
    double? selectedValue,
    OnNumberPicked? onItemSelected,
  }) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return PickerNumber(
            startNumber ?? 0,
            endNumber ?? 0,
            onItemSelected: onItemSelected,
            selectedValue: selectedValue,
          );
        });
  }

  static void showImagePickType(BuildContext context, {OnImagePicked? onImagePicked}) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return ImagePickerWidget(
            onImagePicked: onImagePicked,
          );
        });
  }

  /// Image picker
  static Future<XFile?> pickerImage(BuildContext context, {bool isCamera = false}) async {
    var photoPermission = await Permission.photos.request();
    var cameraPermission = await Permission.camera.request();
    final ImagePicker _picker = ImagePicker();
    if (photoPermission != null || cameraPermission != null) {
      final imagePicked = isCamera ? await _picker.pickImage(source: ImageSource.camera) : await _picker.pickImage(source: ImageSource.gallery);
      return imagePicked ?? XFile("");
    }
  }

  /// Datetime picker
  static void showDatePicker({
    required BuildContext context,
    ValueChanged<DateTime>? onChanged,
    DateTime? currentTime,
  }) {
    AppDialog.showDatePicker(
      context,
      maxTime: AppConfigs.birthMaxDate,
      minTime: AppConfigs.birthMinDate,
      onConfirm: (dateTime) => onChanged?.call(dateTime),
      currentTime: currentTime ?? DateTime.now(),
      locale: LocaleType.en,
    );
  }
}
