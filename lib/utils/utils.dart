import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_decode_entity.dart';

class Utils {
  /// Checks if string is email.
  static bool isEmail(String email) => GetUtils.isEmail(email);

  /// Checks if string is phone number.
  static bool isPhoneNumber(String email) => GetUtils.isPhoneNumber(email);

  /// Checks if string is URL.
  static bool isURL(String url) => GetUtils.isURL(url);

  /// Hidden keyboard
  static void hideKeyBoard({required BuildContext context}) {
    FocusScope.of(context).unfocus();
  }

  /// Check code sign in
  static bool checkCodeSignIn(String code) {
    if (code == "201" || code == "202" || code == "203") {
      return true;
    } else {
      return false;
    }
  }

  /// Check code sign up
  static bool checkCodeSignUp(String code) {
    bool passTooShort = code == "PasswordTooShort";
    bool passRequite = code == "PasswordRequiresNonAlphanumeric";
    bool dupEmail = code == "DuplicateEmail";
    bool dupUsername = code == "DuplicateUserName";
    bool invalidEmail = code == "InvalidEmail";
    bool invalidUserName = code == "InvalidUserName";
    if (passTooShort || passRequite || dupEmail || dupUsername || invalidEmail || invalidUserName) {
      return true;
    } else {
      return false;
    }
  }

  /// Get current location
  // static Future<Position> getCurrentPosition() async {
  //   bool serviceEnabled;
  //   LocationPermission permission;
  //
  //   serviceEnabled = await Geolocator.isLocationServiceEnabled();
  //   if (!serviceEnabled) {
  //     return Future.error('Location services are disabled.');
  //   }
  //
  //   permission = await Geolocator.checkPermission();
  //   if (permission == LocationPermission.denied) {
  //     permission = await Geolocator.requestPermission();
  //     if (permission == LocationPermission.denied) {
  //       return Future.error('Location permissions are denied');
  //     }
  //   }
  //
  //   if (permission == LocationPermission.deniedForever) {
  //     return Future.error('Location permissions are permanently denied, we cannot request permissions.');
  //   }
  //   return await Geolocator.getCurrentPosition();
  // }

  static String getUserId({required String accessToken}) {
    if (accessToken == '') {
      return '';
    }
    final Map<String, dynamic> payload = Jwt.parseJwt(accessToken);
    final tokenDecode = TokenDecodeResponse.fromJson(payload);
    return tokenDecode.sub ?? '';
  }
}
