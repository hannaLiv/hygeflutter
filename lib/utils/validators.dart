import 'package:flutter/cupertino.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';

class Validators {
  static bool validateEmail(String email) => RegExp(r"^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$").hasMatch(email);

  static bool validatePassword(String password) => RegExp(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$").hasMatch(password);

  static String? validatorUsername(String username, BuildContext context) {
    if (username.isEmpty) {
      return S.of(context).common_error_empty_username;
    }
    return '';
  }

  static String validatorPassword(String password, BuildContext context) {
    if (password.isEmpty) {
      return S.of(context).common_error_empty_password;
    }
    return '';
  }
}
