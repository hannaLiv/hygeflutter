import 'dart:async';
import 'dart:convert';

import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;


String getPlatform(){
  try{
    if (Platform.isAndroid) {
      return "android";
    } else if (Platform.isIOS) {
       return "ios";
    }
    else{
      return "none";
    }
  }
  catch(ex){
    return "none";
  }
}


String convertImageToBase64(String path) {
  if (path != null && path != "") {
    try {
      var bytesImg = new File(path);
      final bytes = bytesImg.readAsBytesSync();
      String img64 = base64Encode(bytes);
      return img64;
    } catch (ex) {
      return "";
    }
  }
  return "";
}
// handle token

Future<SharedPreferences> createSharedPreferences() async{
  //SharedPreferences.setMockInitialValues({});
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences;
}

Future<bool> saveToken(String value) async 
{
  try
  {
    SharedPreferences preferences = await createSharedPreferences();
    var result = await preferences.setString(AppConfigs.tokenKey, value);
    return result;
  }
  catch(ex)
  {
    return false;
  }
}

Future<String> getToken() async {
  try{
    SharedPreferences preferences = await createSharedPreferences();
    var result = preferences.getString(AppConfigs.tokenKey);
    return result!;
  }
  catch(ex){
    return "";
  }
}

Future<bool> setAuthenStatus(bool value) async 
{
  try
  {
    SharedPreferences preferences = await createSharedPreferences();
    var result = await preferences.setBool(AppConfigs.tokenKey, value);
    return result;
  }
  catch(ex)
  {
    return false;
  }
}

Future<bool> getAuthenStatus() async {
  try{
    SharedPreferences preferences = await createSharedPreferences();
    var result = preferences.getBool(AppConfigs.tokenKey);
    if(result == null){
      return false;
    }
    return result;
  }
  catch(ex){
    return false;
  }
}

Future<bool> saveFCMToken(String value) async 
{
  try
  {
    SharedPreferences preferences = await createSharedPreferences();
    var result = await preferences.setString(AppConfigs.tokenFCMKey, value);
    return result;
  }
  catch(ex)
  {
    return false;
  }
}

Future<String> getFCMToken() async {
  try
  {
    SharedPreferences preferences = await createSharedPreferences();
    var result =  preferences.getString(AppConfigs.tokenFCMKey);
    return result!;
  }
  catch(ex){
    return "";
  }
}

// handle file to DataFile
Future<FormData> convertFileToFromData(
    List<File> listFile, String fileField) async {
  FormData formData = new FormData();
  var _list = List<MultipartFile>.empty(growable: false);
  if (listFile != null && listFile.length > 0) {
    for (var file in listFile) {
      _list.add(MultipartFile.fromFileSync(file.path, filename: file.path));
    }
    // if(_list.length >0){
    //   //formData.files.addAll(_list);
    // }
    return formData;
  } else {
    return formData;
  }
}

// Get location

String generateKeyCode() {
  var uuid = new Uuid();
  return uuid.v4().toString();
}


// get extension by path or url
String checkExtensionFile(String url) {
  try {
    return p.extension(url);
  } catch (ex) {
    return "orther";
  }
}

// list video extension
List<String> listVideoExtension = [".3g2",".3gp",".avi",".flv",".h264",".m4v",".mkv",".mov",".mp4",".mpg",".rm",".swf",".vob",".wmv"];
// kiểm tra video
bool isVideoCheck(String fileExtension){
  var result = listVideoExtension.indexOf(fileExtension);
  if(result>-1){
    return true;
  }
  else{
    return false;
  }
}

//get File by memory
Image getFileByMemory(Uint8List byte){
  return Image.memory(byte);
}

// find item in list Object
int findItemByField(dynamic src,String field,String key){
  if(src != null){
    for(int i=0; i< src.length;i++){
      if(src[i][field] == key){
        return i;
      }
    }
    return -1;
  }
  return -1;
}

// validate number phone
bool phoneNumberValidator(String value) {
    if((value.length ==10 || value.length ==11) && value[0] == '0'){
      try{
         int.tryParse(value);
         return true;
      }
      catch(ex){
        return false;
      }
    }
    else
    {
      return false;
    }
}

Future<bool> chooseFileFileStorage(Permission permission) async{
  try{
    var status = await permission.status;
    if (status.isDenied) {
      var result = await permission.request();
      if(result.isDenied){
        return false;
      }
      return true;
    }
    return true;
  }
  catch(ex){
    return false;
  }
}
bool checkNumberPhone(String number){
  if(number.isNotEmpty && (number.length >=9 && number.length <=10) && number[0] == "0"){
    return true;
  }
  return false;
}

String encodeEmailParameters(Map<String, String> params) {
  return params.entries
      .map((e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
      .join('&');
}

// dd/mm/yyyy
String formatTypeStringDatetime(String ddmmyyyy){
  try{
    String year = ddmmyyyy.substring(6,10);
    String month = ddmmyyyy.substring(3,5);
    String day = ddmmyyyy.substring(0,2);
    return year+"-"+month+"-"+day;
  }
  catch(ex){
    return "";
  }
}

DateTime getCurrentTime(String dateTime){
  try{
    var _temp = formatTypeStringDatetime(dateTime);
    return DateTime.parse(_temp);
  }
  catch(ex){
    return DateTime.now();
  }
}

int pageCalculate(int listLength,int itemPage){
  try{
    if(listLength >0 && itemPage >0){
      double temp = listLength/itemPage;
      if(temp > temp.toInt()){
        return temp.toInt()+1;
      }
      if(temp.toInt()<=0){
        return 1;
      }
      else{
        return temp.toInt();
      }
    }
    else
      return 0;
  }
  catch(ex){
    return 0;
  }
}

String getFormatDateByString(String date){
    try{
      return  date.substring(0,11);
    }
    catch(ex){
      return "n/a";
    }
  }

Future<String> getPathFile({String ?fileName}) async{
  String fullPath="";
  try{
    Directory tempDir = await getTemporaryDirectory();
    fullPath = tempDir.path+"/"+ generateKeyCode() + ".pdf";
  }
  catch(ex){
    throw ex;
  }
  return fullPath;
}


String stripAccents(String s) 
{
    s = s.replaceAll(RegExp("/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g"),"a");
    s = s.replaceAll(RegExp("/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g"),"e");
    s = s.replaceAll(RegExp("/ì|í|ị|ỉ|ĩ/g"),"i");
    s = s.replaceAll(RegExp("/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g"),"o");
    s = s.replaceAll(RegExp("/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g"),"u");
    s = s.replaceAll(RegExp("/ỳ|ý|ỵ|ỷ|ỹ/g"),"y");
    s = s.replaceAll(RegExp("đ"),"d");
    s = s.replaceAll(RegExp("/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g"),"A");
    s = s.replaceAll(RegExp("/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g"),"E");
    s = s.replaceAll(RegExp("/Ì|Í|Ị|Ỉ|Ĩ/g"),"I");
    s = s.replaceAll(RegExp("/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g"),"O");
    s = s.replaceAll(RegExp("/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g"),"U");
    s = s.replaceAll(RegExp("/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g"),"Y");
    s = s.replaceAll(RegExp("Đ"),"D");
    s= s.replaceAll(RegExp(r"\s\b|\b\s"),"");
    return s;
}

