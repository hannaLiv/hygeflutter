import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';

class Authentication {
  /// Initial firebase
  static Future<FirebaseApp> initializeFirebase({
    required BuildContext context,
  }) async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();

    return firebaseApp;
  }

  /// Sign in with google account
  static Future<String?> signInWithGoogle({required BuildContext context}) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    User? user;
    String accessToken = '';

    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );
      //accessToken = googleSignInAuthentication.idToken ?? '';
      print('TOken: ${googleSignInAuthentication.idToken}');
      try {
        final UserCredential userCredential = await auth.signInWithCredential(credential);

        user = userCredential.user;
        print('AToken: ${userCredential.credential?.token}');
        print('id : ${userCredential.user?.uid}');
        accessToken = userCredential.user?.uid ?? '';
      } on FirebaseAuthException catch (e) {
        if (e.code == 'account-exists-with-different-credential') {
          // handle the error here
        } else if (e.code == 'invalid-credential') {
          // handle the error here
        }
      } catch (e) {
        // handle the error here
      }
    }

    return accessToken;
  }

  /// Google sign out
  static Future<void> googleSignOut() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();

    try {
      if (!kIsWeb) {
        await googleSignIn.signOut();
      }

      await FirebaseAuth.instance.signOut();
    } catch (e) {
      logger.e('Error signing out. Try again.');
    }
  }

  /// Sign in with Facebook Account
  static Future<String?> loginWithFacebook() async {
    String token = '';
    final LoginResult result = await FacebookAuth.instance.login();
    if (result.status == LoginStatus.success) {
      final AccessToken accessToken = result.accessToken!;
      print('$accessToken');
      token = accessToken.token;
      return token;
    }
  }

  /// Sign out Facebook
  static Future facebookSignOut() async {
    FacebookAuth.instance.logOut();
  }
}
