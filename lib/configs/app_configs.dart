import 'package:twinkle_colors_flutter_app/utils/date_utils.dart';

class AppConfigs {
  static const String appName = 'flutter getX app';

  /// DEV ENVIRONMENT
  static const baseUrl = "https://apisipm.cagt.top";

  /// STAGING ENVIRONMENT
  static const envName = "Staging";
  static const webUrl = "";

  // static const baseUrl = "https://nguyenhaphamgia.com.vn/";
  static const socketUrl = '';

  /// PRODUCTION ENVIRONMENT
  /// FACEBOOK CONFIGS
  static const facebookClientId = "1310874295998396";
  static const facebookRedirectUrl = "https://www.facebook.com/connect/login_success.html";

  ///Paging

  ///Local
  static const appLocal = 'vi_VN';
  static const appLanguage = 'en';

  ///DateFormat

  static const dateAPIFormat = 'dd/MM/yyyy';
  static const dateDisplayFormat = 'dd/MM/yyyy';
  static const dateFormatYdM = 'yyyy/MM/dd';

  static const dateTimeAPIFormat = "MM/dd/yyyy'T'hh:mm:ss.SSSZ";
  static const dateTimeDisplayFormat = 'dd/MM/yyyy HH:mm';

  ///Date range
  static final identityMinDate = DateTime(1900, 1, 1);
  static final identityMaxDate = DateTime.now();
  static final birthMinDate = DateTime(1900, 1, 1);
  static final birthMaxDate = DateTime.now();

  ///Font
  static const fontFamily = 'Roboto';

  static final String tokenKey ="TOKEN_KEY";
  static final String tokenFCMKey ="FCM_KEY";
}

class FirebaseConfig {
  //Todo
}

class DatabaseConfig {
  //Todo
  static const int version = 1;
}
