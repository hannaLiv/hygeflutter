import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';
import 'package:twinkle_colors_flutter_app/models/responses/categories_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_detail_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_information_response.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_infor_response.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_information_follow_response.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: AppConfigs.baseUrl)
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  ///User
// @POST("/register")
// Future<TokenEntity> authLogin(@Body() Map<String, dynamic> body);
  /// Sign in with account
  @POST("/api/app/s-sOLogin/signin-account")
  Future<TokenEntity> signInWithCustomerAccount(@Body() Map<String, dynamic> body);

  /// Sign in with Google
  @POST("/api/app/s-sOLogin/singin-google")
  Future<TokenEntity> signInWithGoogle(@Body() Map<String, dynamic> body);

  /// Sign in with Facebook
  @POST("/api/app/s-sOLogin/singin-facebook")
  Future<TokenEntity> signInWithFacebook(@Body() Map<String, dynamic> body);

  /// Sign in with Firebase
  @POST("/api/app/s-sOLogin/singin-firebase")
  Future<TokenEntity> signInWithFirebase(@Body() Map<String, dynamic> body);

  /// Sign up with account
  @POST("/api/app/s-sOLogin/register-customer-account")
  Future<TokenEntity> signUpWithCustomerAccount(@Body() Map<String, dynamic> body);

  /// Check OTP
  @POST("/api​/app​/s-sOLogin​/verify-oTPToken")
  Future<TokenEntity> checkOTPSignIn(@Body() Map<String, dynamic> body);

  @GET("/api/app/user-configuration/user-infomation-by-id")
  Future<UserInfoResponse> getUserInformationById();

  @POST("/api/app/user-configuration/update-user-infomation-by-id")
  Future<bool> updateUserInformationById(@Body() Map<String, dynamic> body);

  /// Get user info follow
  @GET("/api/app/user-follow/information-user-follow")
  Future<UserInformationFollow> getUserInformationFollow();

  /// Get user info for first sign up
  @GET("/api/app/shoppings/information-step-by-step")
  Future<UserInformationResponse> getUserInformationForStep();

  /// Upload avatar image
  @POST("/InputFileUpload/UploadFiles")
  Future<UserInformationEntity> uploadAvatar(@Body() FormData body);

  /// Get categories
  @GET("/api/app/categories")
  Future<CategoriesResponses> getCategories(@Body() Map<String, dynamic> body);

  /// Get category by id
  @GET("/api/app/categories/{id}")
  Future<UserInformationEntity> getCategoryById(@Path() int? id);

  /// Get dichVus
  @GET("/api/app/dich-vu")
  Future<DichVuResponses> getDichVus(@Body() Map<String, dynamic> body);

  /// Get combos
  @GET("/api/app/dich-vu")
  Future<DichVuResponses> getCombos(@Body() Map<String, dynamic> body);

  /// Get dichvudetail
  @GET("/api/app/dich-vu/{id}/dich-vu-full-detail")
  Future<DichVuDetailResponses> getDichVuDetails(@Path() int? id);

  /// Get products
  @GET("/api/app/san-pham")
  Future<DichVuResponses> getProducts();

  /// Get product with id
  @GET("/api/app/order/full-detail-order-page/{sanPhamId}")
  Future<DichVuResponses> getProductsWithId(@Path() int? sanPhamId);

  /// Get User infomation
  @GET("/api/app/s-sOLogin/user-full-detail")
  Future<TokenEntity> getUserInfo();

  /// Order
  @POST("/api/app/order/or-update-order-full-detail")
  Future<bool> pushOrder(@Body() Map<String, dynamic> body);
}
