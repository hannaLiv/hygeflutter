import 'package:twinkle_colors_flutter_app/models/sanPhamDTO.Model.dart';
import 'package:twinkle_colors_flutter_app/models/thoiGianDTO.Model.dart';

class TuyChonDTOModel{

  String ?ma;
  String ?ten;
  int ?gia;
  int ?typeId;
  int ?statusId;
  String ?ghiChu;
  DateTime ?creationTime;
  String ?creatorId;
  int ?id;
  TuyChonDTOModel({this.creationTime,this.creatorId,this.ghiChu,this.gia,this.id,this.ma,this.statusId,this.ten,this.typeId});

  factory TuyChonDTOModel.fromJson(Map<String, dynamic> ?json) {
    var result = new TuyChonDTOModel();
    if (json == null) return result;
    result = new TuyChonDTOModel(
      ma: json["ma"],
      ten: json["ten"],
      gia: json["gia"],
      typeId: json["typeId"],
      ghiChu: json["ghiChu"],
      creationTime: json["creationTime"],
      creatorId: json["creatorId"],
      id: json["id"],
    );
    return result;
  }

  static List<TuyChonDTOModel> fromJsonList(List ?list) {
     List<TuyChonDTOModel> listResult = new  List<TuyChonDTOModel>.empty(growable: true);
    if (list == null) return listResult;
    return list.map((item) => TuyChonDTOModel.fromJson(item)).toList();
  }

  static List<Map<String, dynamic>> toJsonList(List<TuyChonDTOModel> ?list) {
    List<Map<String, dynamic>> result =[];
    if (list == null) return result;
    return list.map((item) => item.toJson()).toList();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
     "ma": this.ma,
      "ten": this.ten,
      "gia": this.gia,
      "typeId":this.typeId,
      "ghiChu": this.ghiChu,
      "creationTime": this.creationTime,
      "creatorId": this.creatorId,
      "id":this.id,
    };
    return map;
  }
}