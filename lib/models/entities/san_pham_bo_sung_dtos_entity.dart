import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/san_pham_entity.dart';

part 'san_pham_bo_sung_dtos_entity.g.dart';

@JsonSerializable()
class SanPhamBoSungDtosEntity {
  @JsonKey(name: "sanPhamThoiGianId")
  final int? sanPhamThoiGianId;
  @JsonKey(name: "sanPhamDTO")
  final SanPhamEntity? sanPhamDTO;

  SanPhamBoSungDtosEntity({
    this.sanPhamThoiGianId,
    this.sanPhamDTO,
  });

  factory SanPhamBoSungDtosEntity.fromJson(Map<String, dynamic> json) => _$SanPhamBoSungDtosEntityFromJson(json);

  Map<String, dynamic> toJson() => _$SanPhamBoSungDtosEntityToJson(this);
}
