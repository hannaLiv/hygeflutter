class CommonEntity {
  String? ma;
  int? idSanPham;
  int? idThoiGian;
  double? gia;
  int? typeId;
  int? statusId;
  String? ghiChu;
  String? creationTime;
  String? creatorId;
  int? soGio;
  int? id;
  int? sanPhamThoiGianId;
  String? thoiGian_Ten;
  String? thoiGian_GiaTien;

  CommonEntity(
      {this.ma,
      this.idSanPham,
      this.idThoiGian,
      this.gia,
      this.typeId,
      this.statusId,
      this.ghiChu,
      this.creationTime,
      this.creatorId,
      this.soGio,
      this.id,
      this.sanPhamThoiGianId,
      this.thoiGian_Ten,
      this.thoiGian_GiaTien});

  CommonEntity.fromJson(Map<String, dynamic> json) {
    ma = json['ma'];
    idSanPham = json['idSanPham'];
    idThoiGian = json['idThoiGian'];
    gia = json['gia'];
    typeId = json['typeId'];
    statusId = json['statusId'];
    ghiChu = json['ghiChu'];
    creationTime = json['creationTime'];
    creatorId = json['creatorId'];
    soGio = json['soGio'];
    id = json['id'];
    sanPhamThoiGianId = json['sanPhamThoiGianId'];
    thoiGian_Ten = json['thoiGian_Ten'];
    thoiGian_GiaTien = json['thoiGian_GiaTien'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ma'] = this.ma;
    data['idSanPham'] = this.idSanPham;
    data['idThoiGian'] = this.idThoiGian;
    data['gia'] = this.gia;
    data['typeId'] = this.typeId;
    data['statusId'] = this.statusId;
    data['ghiChu'] = this.ghiChu;
    data['creationTime'] = this.creationTime;
    data['creatorId'] = this.creatorId;
    data['soGio'] = this.soGio;
    data['id'] = this.id;
    data['sanPhamThoiGianId'] = this.sanPhamThoiGianId;
    data['thoiGian_Ten'] = this.thoiGian_Ten;
    data['thoiGian_GiaTien'] = this.thoiGian_GiaTien;
    return data;
  }
}