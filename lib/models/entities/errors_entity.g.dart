// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'errors_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ErrorsEntity _$ErrorsEntityFromJson(Map<String, dynamic> json) {
  return ErrorsEntity(
    code: json['code'] as String?,
    message: json['message'] as String?,
    details: json['details'] as String?,
    validationErrors: json['validationErrors'] == null
        ? null
        : ValidationError.fromJson(
            json['validationErrors'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ErrorsEntityToJson(ErrorsEntity instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'details': instance.details,
      'validationErrors': instance.validationErrors,
    };

ValidationError _$ValidationErrorFromJson(Map<String, dynamic> json) {
  return ValidationError(
    message: json['message'] as String?,
    members:
        (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$ValidationErrorToJson(ValidationError instance) =>
    <String, dynamic>{
      'message': instance.message,
      'members': instance.members,
    };
