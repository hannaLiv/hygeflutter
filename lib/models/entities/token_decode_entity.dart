import 'package:json_annotation/json_annotation.dart';

part 'token_decode_entity.g.dart';

@JsonSerializable()
class TokenDecodeResponse {
  @JsonKey(name: 'nbf')
  final int? nbf;
  @JsonKey(name: 'exp')
  final int? exp;
  @JsonKey(name: 'iss')
  final String? iss;
  @JsonKey(name: 'aud')
  final String? aud;
  @JsonKey(name: 'clientId')
  final String? clientId;
  @JsonKey(name: 'sub')
  final String? sub;
  @JsonKey(name: 'authTime')
  final int? authTime;
  @JsonKey(name: 'idp')
  final String? idp;
  @JsonKey(name: 'role')
  final String? role;
  @JsonKey(name: 'phoneNumberVerified')
  final String? phoneNumberVerified;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'emailVerified')
  final String? emailVerified;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'iat')
  final int? iat;
  @JsonKey(name: 'scope')
  final List<String>? scope;
  @JsonKey(name: 'amr')
  final List<String>? amr;

  TokenDecodeResponse({
    this.nbf,
    this.exp,
    this.iss,
    this.aud,
    this.clientId,
    this.sub,
    this.authTime,
    this.idp,
    this.role,
    this.phoneNumberVerified,
    this.email,
    this.emailVerified,
    this.name,
    this.iat,
    this.scope,
    this.amr,
  });

  factory TokenDecodeResponse.fromJson(Map<String, dynamic> json) => _$TokenDecodeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TokenDecodeResponseToJson(this);
}
