// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'san_pham_bo_sung_dtos_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SanPhamBoSungDtosEntity _$SanPhamBoSungDtosEntityFromJson(
    Map<String, dynamic> json) {
  return SanPhamBoSungDtosEntity(
    sanPhamThoiGianId: json['sanPhamThoiGianId'] as int?,
    sanPhamDTO: json['sanPhamDTO'] == null
        ? null
        : SanPhamEntity.fromJson(json['sanPhamDTO'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SanPhamBoSungDtosEntityToJson(
        SanPhamBoSungDtosEntity instance) =>
    <String, dynamic>{
      'sanPhamThoiGianId': instance.sanPhamThoiGianId,
      'sanPhamDTO': instance.sanPhamDTO,
    };
