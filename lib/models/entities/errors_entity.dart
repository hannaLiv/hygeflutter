import 'package:json_annotation/json_annotation.dart';

part 'errors_entity.g.dart';

@JsonSerializable()
class ErrorsEntity {
  @JsonKey(name: "code")
  final String? code;
  @JsonKey(name: "message")
  final String? message;
  @JsonKey(name: "details")
  final String? details;
  @JsonKey(name: "validationErrors")
  final ValidationError? validationErrors;

  ErrorsEntity({
    this.code,
    this.message,
    this.details,
    this.validationErrors,
  });

  factory ErrorsEntity.fromJson(Map<String, dynamic> json) => _$ErrorsEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ErrorsEntityToJson(this);
}

@JsonSerializable()
class ValidationError {
  @JsonKey(name: "message")
  final String? message;
  @JsonKey(name: "members")
  final List<String>? members;

  ValidationError({
    this.message,
    this.members,
  });
  factory ValidationError.fromJson(Map<String, dynamic> json) => _$ValidationErrorFromJson(json);

  Map<String, dynamic> toJson() => _$ValidationErrorToJson(this);
}
