import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/responses/extra_properties_response.dart';

part 'user_information_entity.g.dart';

@JsonSerializable()
class UserInformationEntity {
  @JsonKey(name: "name")
  final String? name;
  @JsonKey(name: "description")
  final String? description;
  @JsonKey(name: "type")
  final int? type;
  @JsonKey(name: "status")
  final int? status;
  @JsonKey(name: "id")
  final int? id;
  @JsonKey(name: "title")
  final String? title;
  @JsonKey(name: "code")
  final String? code;
  @JsonKey(name: "display")
  final int? display;
  @JsonKey(name: 'tenantId')
  final String? tenantId;
  @JsonKey(name: 'userName')
  final String? userName;
  @JsonKey(name: 'surname')
  final String? surname;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'emailConfirmed')
  final bool? emailConfirmed;
  @JsonKey(name: 'phoneNumber')
  final String? phoneNumber;
  @JsonKey(name: 'phoneNumberConfirmed')
  final bool? phoneNumberConfirmed;
  @JsonKey(name: 'lockoutEnabled')
  final bool? lockoutEnabled;
  @JsonKey(name: 'lockoutEnd')
  final String? lockoutEnd;
  @JsonKey(name: 'concurrencyStamp')
  final String? concurrencyStamp;
  @JsonKey(name: 'isDeleted')
  final bool? isDeleted;
  @JsonKey(name: 'deleterId')
  final String? deleterId;
  @JsonKey(name: 'deletionTime')
  final String? deletionTime;
  @JsonKey(name: 'lastModificationTime')
  final String? lastModificationTime;
  @JsonKey(name: 'lastModifierId')
  final String? lastModifierId;
  @JsonKey(name: 'creationTime')
  final String? creationTime;
  @JsonKey(name: 'creatorId')
  final String? creatorId;
  @JsonKey(name: 'extraProperties')
  final ExtraProperties? extraProperties;
  @JsonKey(name: 'Original')
  final String? original;
  @JsonKey(name: 'Thumb')
  final String? thumb;
  @JsonKey(name: 'avatar')
  final String? avatar;
  @JsonKey(name: 'aboutMe')
  final String? aboutMe;
  @JsonKey(name: 'work')
  final String? work;
  @JsonKey(name: 'school')
  final String? school;
  @JsonKey(name: 'country')
  final String? country;
  @JsonKey(name: 'state')
  final String? state;
  @JsonKey(name: 'city')
  final String? city;
  @JsonKey(name: 'height')
  final String? height;
  @JsonKey(name: 'weight')
  final String? weight;
  @JsonKey(name: 'currentJob')
  final String? currentJob;
  @JsonKey(name: 'lastEducation')
  final String? lastEducation;
  @JsonKey(name: 'birthDate')
  final String? birthDate;
  @JsonKey(name: 'facebookId')
  final int? facebookId;
  @JsonKey(name: 'styleCss')
  final String? styleCss;
  @JsonKey(name: 'shoppingPreferences')
  final List<int>? shoppingPreferences;
  @JsonKey(name: 'imageUrl')
  final String? imageUrl;

  UserInformationEntity({
    this.name,
    this.description,
    this.type,
    this.status,
    this.id,
    this.title,
    this.code,
    this.display,
    this.tenantId,
    this.userName,
    this.surname,
    this.email,
    this.emailConfirmed,
    this.phoneNumber,
    this.phoneNumberConfirmed,
    this.lockoutEnabled,
    this.lockoutEnd,
    this.concurrencyStamp,
    this.isDeleted,
    this.deleterId,
    this.deletionTime,
    this.lastModificationTime,
    this.lastModifierId,
    this.creationTime,
    this.creatorId,
    this.extraProperties,
    this.original,
    this.thumb,
    this.avatar,
    this.aboutMe,
    this.work,
    this.school,
    this.country,
    this.state,
    this.city,
    this.height,
    this.weight,
    this.currentJob,
    this.lastEducation,
    this.birthDate,
    this.facebookId,
    this.styleCss,
    this.shoppingPreferences,
    this.imageUrl
  });

  factory UserInformationEntity.fromJson(Map<String, dynamic> json) => _$UserInformationEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UserInformationEntityToJson(this);
}
