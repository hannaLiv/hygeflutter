// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'san_pham_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SanPhamEntity _$SanPhamEntityFromJson(Map<String, dynamic> json) {
  return SanPhamEntity(
    ma: json['ma'] as String?,
    ten: json['ten'] as String?,
    parentId: json['parentId'] as int?,
    typeId: json['typeId'] as int?,
    statusId: json['statusId'] as int?,
    creationTime: json['creationTime'] as String?,
    creatorId: json['creatorId'] as String?,
    id: json['id'] as int?,
    ghiChu: json['ghiChu'] as String?,
    idDichVu: json['idDichVu'] as int?,
    loaiSanPham: json['loaiSanPham'] as int?,
    giaGoc: (json['giaGoc'] as num?)?.toDouble(),
    maMau: json['maMau'] as String?,
    icon: json['icon'] as String?,
    sanPhamThoiGianId: json['sanPhamThoiGianId'] as int?,
    thoiGianTen: json['thoiGian_Ten'] as String?,
    thoiGianGiaTien: (json['thoiGian_GiaTien'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$SanPhamEntityToJson(SanPhamEntity instance) =>
    <String, dynamic>{
      'ma': instance.ma,
      'ten': instance.ten,
      'parentId': instance.parentId,
      'typeId': instance.typeId,
      'statusId': instance.statusId,
      'creationTime': instance.creationTime,
      'creatorId': instance.creatorId,
      'id': instance.id,
      'ghiChu': instance.ghiChu,
      'idDichVu': instance.idDichVu,
      'loaiSanPham': instance.loaiSanPham,
      'giaGoc': instance.giaGoc,
      'maMau': instance.maMau,
      'icon': instance.icon,
      'sanPhamThoiGianId': instance.sanPhamThoiGianId,
      'thoiGian_Ten': instance.thoiGianTen,
      'thoiGian_GiaTien': instance.thoiGianGiaTien,
    };
