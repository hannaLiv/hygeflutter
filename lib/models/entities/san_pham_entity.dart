import 'package:json_annotation/json_annotation.dart';

part 'san_pham_entity.g.dart';

@JsonSerializable()
class SanPhamEntity {
  @JsonKey(name: "ma")
  final String? ma;
  @JsonKey(name: "ten")
  final String? ten;
  @JsonKey(name: "parentId")
  final int? parentId;
  @JsonKey(name: "typeId")
  final int? typeId;
  @JsonKey(name: "statusId")
  final int? statusId;
  @JsonKey(name: "creationTime")
  final String? creationTime;
  @JsonKey(name: "creatorId")
  final String? creatorId;
  @JsonKey(name: "id")
  final int? id;
  @JsonKey(name: "ghiChu")
  final String? ghiChu;
  @JsonKey(name: "idDichVu")
  final int? idDichVu;
  @JsonKey(name: "loaiSanPham")
  final int? loaiSanPham;
  @JsonKey(name: "giaGoc")
  final double? giaGoc;
  @JsonKey(name: "maMau")
  final String? maMau;
  @JsonKey(name: "icon")
  final String? icon;
  @JsonKey(name: "sanPhamThoiGianId")
  final int? sanPhamThoiGianId;
  @JsonKey(name: "thoiGian_Ten")
  final String? thoiGianTen;
  @JsonKey(name: "thoiGian_GiaTien")
  final double? thoiGianGiaTien;

  SanPhamEntity({
    this.ma,
    this.ten,
    this.parentId,
    this.typeId,
    this.statusId,
    this.creationTime,
    this.creatorId,
    this.id,
    this.ghiChu,
    this.idDichVu,
    this.loaiSanPham,
    this.giaGoc,
    this.maMau,
    this.icon,
    this.sanPhamThoiGianId,
    this.thoiGianTen,
    this.thoiGianGiaTien,
  });

  factory SanPhamEntity.fromJson(Map<String, dynamic> json) => _$SanPhamEntityFromJson(json);

  Map<String, dynamic> toJson() => _$SanPhamEntityToJson(this);
}
