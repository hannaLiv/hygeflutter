// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_information_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInformationEntity _$UserInformationEntityFromJson(
    Map<String, dynamic> json) {
  return UserInformationEntity(
    name: json['name'] as String?,
    description: json['description'] as String?,
    type: json['type'] as int?,
    status: json['status'] as int?,
    id: json['id'] as int?,
    title: json['title'] as String?,
    code: json['code'] as String?,
    display: json['display'] as int?,
    tenantId: json['tenantId'] as String?,
    userName: json['userName'] as String?,
    surname: json['surname'] as String?,
    email: json['email'] as String?,
    emailConfirmed: json['emailConfirmed'] as bool?,
    phoneNumber: json['phoneNumber'] as String?,
    phoneNumberConfirmed: json['phoneNumberConfirmed'] as bool?,
    lockoutEnabled: json['lockoutEnabled'] as bool?,
    lockoutEnd: json['lockoutEnd'] as String?,
    concurrencyStamp: json['concurrencyStamp'] as String?,
    isDeleted: json['isDeleted'] as bool?,
    deleterId: json['deleterId'] as String?,
    deletionTime: json['deletionTime'] as String?,
    lastModificationTime: json['lastModificationTime'] as String?,
    lastModifierId: json['lastModifierId'] as String?,
    creationTime: json['creationTime'] as String?,
    creatorId: json['creatorId'] as String?,
    extraProperties: json['extraProperties'] == null
        ? null
        : ExtraProperties.fromJson(
            json['extraProperties'] as Map<String, dynamic>),
    original: json['Original'] as String?,
    thumb: json['Thumb'] as String?,
    avatar: json['avatar'] as String?,
    aboutMe: json['aboutMe'] as String?,
    work: json['work'] as String?,
    school: json['school'] as String?,
    country: json['country'] as String?,
    state: json['state'] as String?,
    city: json['city'] as String?,
    height: json['height'] as String?,
    weight: json['weight'] as String?,
    currentJob: json['currentJob'] as String?,
    lastEducation: json['lastEducation'] as String?,
    birthDate: json['birthDate'] as String?,
    facebookId: json['facebookId'] as int?,
    styleCss: json['styleCss'] as String?,
    shoppingPreferences: (json['shoppingPreferences'] as List<dynamic>?)
        ?.map((e) => e as int)
        .toList(),
    imageUrl: json['imageUrl'] as String?,
  );
}

Map<String, dynamic> _$UserInformationEntityToJson(
        UserInformationEntity instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'type': instance.type,
      'status': instance.status,
      'id': instance.id,
      'title': instance.title,
      'code': instance.code,
      'display': instance.display,
      'tenantId': instance.tenantId,
      'userName': instance.userName,
      'surname': instance.surname,
      'email': instance.email,
      'emailConfirmed': instance.emailConfirmed,
      'phoneNumber': instance.phoneNumber,
      'phoneNumberConfirmed': instance.phoneNumberConfirmed,
      'lockoutEnabled': instance.lockoutEnabled,
      'lockoutEnd': instance.lockoutEnd,
      'concurrencyStamp': instance.concurrencyStamp,
      'isDeleted': instance.isDeleted,
      'deleterId': instance.deleterId,
      'deletionTime': instance.deletionTime,
      'lastModificationTime': instance.lastModificationTime,
      'lastModifierId': instance.lastModifierId,
      'creationTime': instance.creationTime,
      'creatorId': instance.creatorId,
      'extraProperties': instance.extraProperties,
      'Original': instance.original,
      'Thumb': instance.thumb,
      'avatar': instance.avatar,
      'aboutMe': instance.aboutMe,
      'work': instance.work,
      'school': instance.school,
      'country': instance.country,
      'state': instance.state,
      'city': instance.city,
      'height': instance.height,
      'weight': instance.weight,
      'currentJob': instance.currentJob,
      'lastEducation': instance.lastEducation,
      'birthDate': instance.birthDate,
      'facebookId': instance.facebookId,
      'styleCss': instance.styleCss,
      'shoppingPreferences': instance.shoppingPreferences,
      'imageUrl': instance.imageUrl,
    };
