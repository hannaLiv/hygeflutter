// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TokenEntity _$TokenEntityFromJson(Map<String, dynamic> json) {
  return TokenEntity(
    accessToken: json['access_token'] as String?,
    refreshToken: json['refresh_token'] as String?,
    expiresIn: (json['expires_in'] as num?)?.toDouble(),
    tokenType: json['token_type'] as String?,
    scope: json['scope'] as String?,
    code: json['code'] as String?,
    diaChi: json['diaChi'] as String?,
    maGioiThieu: json['maGioiThieu'] as String?,
    userName: json['userName'] as String?,
    id: json['id'] as String?,
    hResult: json['hResult'] as int?,
    logLevel: json['logLevel'] as int?,
    message: json['message'] as String?,
    name: json['name'] as String?,
    phoneNumber: json['phone_number'] as String?,
  );
}

Map<String, dynamic> _$TokenEntityToJson(TokenEntity instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'refresh_token': instance.refreshToken,
      'expires_in': instance.expiresIn,
      'token_type': instance.tokenType,
      'scope': instance.scope,
      'code': instance.code,
      'logLevel': instance.logLevel,
      'message': instance.message,
      'hResult': instance.hResult,
      'name': instance.name,
      'phone_number': instance.phoneNumber,
      'id': instance.id,
      'diaChi': instance.diaChi,
      'maGioiThieu': instance.maGioiThieu,
      'userName': instance.userName,
    };
