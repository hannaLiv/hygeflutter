// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token_decode_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TokenDecodeResponse _$TokenDecodeResponseFromJson(Map<String, dynamic> json) {
  return TokenDecodeResponse(
    nbf: json['nbf'] as int?,
    exp: json['exp'] as int?,
    iss: json['iss'] as String?,
    aud: json['aud'] as String?,
    clientId: json['clientId'] as String?,
    sub: json['sub'] as String?,
    authTime: json['authTime'] as int?,
    idp: json['idp'] as String?,
    role: json['role'] as String?,
    phoneNumberVerified: json['phoneNumberVerified'] as String?,
    email: json['email'] as String?,
    emailVerified: json['emailVerified'] as String?,
    name: json['name'] as String?,
    iat: json['iat'] as int?,
    scope: (json['scope'] as List<dynamic>?)?.map((e) => e as String).toList(),
    amr: (json['amr'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$TokenDecodeResponseToJson(
        TokenDecodeResponse instance) =>
    <String, dynamic>{
      'nbf': instance.nbf,
      'exp': instance.exp,
      'iss': instance.iss,
      'aud': instance.aud,
      'clientId': instance.clientId,
      'sub': instance.sub,
      'authTime': instance.authTime,
      'idp': instance.idp,
      'role': instance.role,
      'phoneNumberVerified': instance.phoneNumberVerified,
      'email': instance.email,
      'emailVerified': instance.emailVerified,
      'name': instance.name,
      'iat': instance.iat,
      'scope': instance.scope,
      'amr': instance.amr,
    };
