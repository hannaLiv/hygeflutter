// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dich_vu_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DichVuEntity _$DichVuEntityFromJson(Map<String, dynamic> json) {
  return DichVuEntity(
    ma: json['ma'] as String?,
    ten: json['ten'] as String?,
    parentId: json['parentId'] as int?,
    typeId: json['typeId'] as int?,
    statusId: json['statusId'] as int?,
    creationTime: json['creationTime'] as String?,
    creatorId: json['creatorId'] as String?,
    id: json['id'] as int?,
    ghiChu: json['ghiChu'] as String?,
    idDichVu: json['idDichVu'] as int?,
    loaiSanPham: json['loaiSanPham'] as int?,
    giaGoc: (json['giaGoc'] as num?)?.toDouble(),
    idOrder: json['idOrder'] as int?,
    soLuong: json['soLuong'] as int?,
    idSPThoiGian: json['idSPThoiGian'] as int?,
    myProperty: json['myProperty'] as int?,
    idTuyChons: json['idTuyChons'] as String?,
    idComBo: json['idComBo'] as int?,
  );
}

Map<String, dynamic> _$DichVuEntityToJson(DichVuEntity instance) =>
    <String, dynamic>{
      'ma': instance.ma,
      'ten': instance.ten,
      'parentId': instance.parentId,
      'typeId': instance.typeId,
      'statusId': instance.statusId,
      'creationTime': instance.creationTime,
      'creatorId': instance.creatorId,
      'id': instance.id,
      'ghiChu': instance.ghiChu,
      'idDichVu': instance.idDichVu,
      'loaiSanPham': instance.loaiSanPham,
      'giaGoc': instance.giaGoc,
      'idOrder': instance.idOrder,
      'soLuong': instance.soLuong,
      'idSPThoiGian': instance.idSPThoiGian,
      'myProperty': instance.myProperty,
      'idTuyChons': instance.idTuyChons,
      'idComBo': instance.idComBo,
    };
