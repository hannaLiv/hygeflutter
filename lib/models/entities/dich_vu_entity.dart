import 'package:json_annotation/json_annotation.dart';

part 'dich_vu_entity.g.dart';

@JsonSerializable()
class DichVuEntity {
  @JsonKey(name: "ma")
  final String? ma;
  @JsonKey(name: "ten")
  final String? ten;
  @JsonKey(name: "parentId")
  final int? parentId;
  @JsonKey(name: "typeId")
  final int? typeId;
  @JsonKey(name: "statusId")
  final int? statusId;
  @JsonKey(name: "creationTime")
  final String? creationTime;
  @JsonKey(name: "creatorId")
  final String? creatorId;
  @JsonKey(name: "id")
  final int? id;
  @JsonKey(name: "ghiChu")
  final String? ghiChu;
  @JsonKey(name: "idDichVu")
  final int? idDichVu;
  @JsonKey(name: "loaiSanPham")
  final int? loaiSanPham;
  @JsonKey(name: "giaGoc")
  final double? giaGoc;
  @JsonKey(name: "idOrder")
  final int? idOrder;
  @JsonKey(name: "soLuong")
  final int? soLuong;
  @JsonKey(name: "idSPThoiGian")
  final int? idSPThoiGian;
  @JsonKey(name: "myProperty")
  final int? myProperty;
  @JsonKey(name: "idTuyChons")
  final String? idTuyChons;
  @JsonKey(name: "idComBo")
  final int? idComBo;

  DichVuEntity({
    this.ma,
    this.ten,
    this.parentId,
    this.typeId,
    this.statusId,
    this.creationTime,
    this.creatorId,
    this.id,
    this.ghiChu,
    this.idDichVu,
    this.loaiSanPham,
    this.giaGoc,
    this.idOrder,
    this.soLuong,
    this.idSPThoiGian,
    this.myProperty,
    this.idTuyChons,
    this.idComBo,
  });

  factory DichVuEntity.fromJson(Map<String, dynamic> json) => _$DichVuEntityFromJson(json);

  Map<String, dynamic> toJson() => _$DichVuEntityToJson(this);
}
