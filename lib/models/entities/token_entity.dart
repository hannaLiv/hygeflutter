import 'package:json_annotation/json_annotation.dart';

part 'token_entity.g.dart';

@JsonSerializable()
class TokenEntity {
  @JsonKey(name: "access_token")
  final String? accessToken;
  @JsonKey(name: "refresh_token")
  final String? refreshToken;
  @JsonKey(name: "expires_in")
  final double? expiresIn;
  @JsonKey(name: "token_type")
  final String? tokenType;
  @JsonKey(name: "scope")
  final String? scope;
  @JsonKey(name: "code")
  final String? code;

  // @JsonKey(name: "details")
  // String? details;
  @JsonKey(name: "logLevel")
  final int? logLevel;

  // @JsonKey(name: "targetSite")
  // String? targetSite;
  // @JsonKey(name: "stackTrace")
  // String? stackTrace;
  @JsonKey(name: "message")
  final String? message;

  // @JsonKey(name: "data")
  // List<dynamic>? data;
  // @JsonKey(name: "innerException")
  // String? innerException;
  // @JsonKey(name: "helpLink")
  // String? helpLink;
  // @JsonKey(name: "source")
  // String? source;
  @JsonKey(name: "hResult")
  final int? hResult;
  @JsonKey(name: "name")
  final String? name;
  @JsonKey(name: "phone_number")
  final String? phoneNumber;
  @JsonKey(name: "id")
  final String? id;
  @JsonKey(name: "diaChi")
  final String? diaChi;
  @JsonKey(name: "maGioiThieu")
  final String? maGioiThieu;
  @JsonKey(name: "userName")
  final String? userName;

  TokenEntity(
      {this.accessToken = "",
      this.refreshToken = "",
      this.expiresIn = 0,
      this.tokenType = "",
      this.scope = "",
      this.code = '',
      this.diaChi,
      this.maGioiThieu,
      this.userName,
      this.id,
      // this.data,
      // this.helpLink,
      this.hResult,
      // this.innerException,
      this.logLevel,
      this.message,
      this.name,
      this.phoneNumber
      // this.source,
      // this.stackTrace,
      // this.targetSite,
      });

  factory TokenEntity.fromJson(Map<String, dynamic> json) => _$TokenEntityFromJson(json);

  Map<String, dynamic> toJson() => _$TokenEntityToJson(this);
}
