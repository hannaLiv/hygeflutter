class ThoiGianDTOsModel{
  int ?sanPhamThoiGianId;
  String ?thoiGianTen;
  double ?thoiGianGiaTien;

  ThoiGianDTOsModel({this.sanPhamThoiGianId,this.thoiGianGiaTien,this.thoiGianTen});

  factory ThoiGianDTOsModel.fromJson(Map<String, dynamic> ?json) {
    var result = new ThoiGianDTOsModel();
    if (json == null) return result;
    result = new ThoiGianDTOsModel(
      sanPhamThoiGianId: json["sanPhamThoiGianId"],
      thoiGianTen: json["thoiGian_Ten"],
      thoiGianGiaTien: json["thoiGian_GiaTien"],
    );
    return result;
  }

  static List<ThoiGianDTOsModel> fromJsonList(List ?list) {
     List<ThoiGianDTOsModel> listResult = new  List<ThoiGianDTOsModel>.empty(growable: true);
    if (list == null) return listResult;
    return list.map((item) => ThoiGianDTOsModel.fromJson(item)).toList();
  }

  static List<Map<String, dynamic>> toJsonList(List<ThoiGianDTOsModel> ?list) {
    List<Map<String, dynamic>> result =[];
    if (list == null) return result;
    return list.map((item) => item.toJson()).toList();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "sanPhamThoiGianId": this.sanPhamThoiGianId,
      "thoiGianTen": this.thoiGianTen,
      "thoiGianGiaTien": this.thoiGianGiaTien,
    };
    return map;
  }
}