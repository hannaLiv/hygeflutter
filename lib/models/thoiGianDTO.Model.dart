class ThoiGianDTOModel{
  String ?ma;
  String ?ten;
  int ?soGio;
  String ?ghiChu;
  int ?typeId;
  int ?statusId;
  DateTime ?creationTime;
  String ?creatorId;
  int ?id;

  ThoiGianDTOModel({this.creationTime,this.creatorId,this.ghiChu,this.id,this.ma,this.soGio,this.statusId,this.ten,this.typeId});

  factory ThoiGianDTOModel.fromJson(Map<String, dynamic> ?json) {
    var result = new ThoiGianDTOModel();
    if (json == null) return result;
    result = new ThoiGianDTOModel(
      ma: json["ma"],
      ten: json["ten"],
      soGio: json["soGio"],
      ghiChu: json["ghiChu"],
      typeId: json["typeId"],
      statusId: json["statusId"],
      creationTime: json["creationTime"],
      creatorId: json["creatorId"],
      id: json["id"],
    );
    return result;
  }

  static List<ThoiGianDTOModel> fromJsonList(List ?list) {
     List<ThoiGianDTOModel> listResult = new  List<ThoiGianDTOModel>.empty(growable: true);
    if (list == null) return listResult;
    return list.map((item) => ThoiGianDTOModel.fromJson(item)).toList();
  }

  static List<Map<String, dynamic>> toJsonList(List<ThoiGianDTOModel> ?list) {
    List<Map<String, dynamic>> result =[];
    if (list == null) return result;
    return list.map((item) => item.toJson()).toList();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "ma": this.ma,
      "ten": this.ten,
      "soGio": this.soGio,
      "ghiChu": this.ghiChu,
      "typeId": this.typeId,
      "statusId": this.statusId,
      "creationTime": this.creationTime,
      "creatorId": this.creatorId,
      "id": this.id,
    };
    return map;
  }
}