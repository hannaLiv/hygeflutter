import 'package:twinkle_colors_flutter_app/models/sanPhamDTO.Model.dart';
import 'package:twinkle_colors_flutter_app/models/thoiGianDTO.Model.dart';

class SanPhamBoSungDTOModel{

  int ?sanPhamThoiGianId;
  SanPhamDTOModel ?sanPhamDTO;
  ThoiGianDTOModel ?thoiGianDTO;
  SanPhamBoSungDTOModel({this.sanPhamThoiGianId,this.sanPhamDTO,this.thoiGianDTO});

  factory SanPhamBoSungDTOModel.fromJson(Map<String, dynamic> ?json) {
    var result = new SanPhamBoSungDTOModel();
    if (json == null) return result;
    result = new SanPhamBoSungDTOModel(
      sanPhamThoiGianId: json["sanPhamThoiGianId"],
      sanPhamDTO: SanPhamDTOModel.fromJson(json["sanPhamDTO"]),
      thoiGianDTO: ThoiGianDTOModel.fromJson(json["thoiGianDTO"]),
    );
    return result;
  }

  static List<SanPhamBoSungDTOModel> fromJsonList(List ?list) {
     List<SanPhamBoSungDTOModel> listResult = new  List<SanPhamBoSungDTOModel>.empty(growable: true);
    if (list == null) return listResult;
    return list.map((item) => SanPhamBoSungDTOModel.fromJson(item)).toList();
  }

  static List<Map<String, dynamic>> toJsonList(List<SanPhamBoSungDTOModel> ?list) {
    List<Map<String, dynamic>> result =[];
    if (list == null) return result;
    return list.map((item) => item.toJson()).toList();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "sanPhamThoiGianId": this.sanPhamThoiGianId,
      "sanPhamDTO": this.sanPhamDTO!.toJson(),
      "thoiGianDTO": this.thoiGianDTO!.toJson(),
    };
    return map;
  }
}