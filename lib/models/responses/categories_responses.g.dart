// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categories_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoriesResponses _$CategoriesResponsesFromJson(Map<String, dynamic> json) {
  return CategoriesResponses(
    totalCount: json['totalCount'] as int?,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$CategoriesResponsesToJson(
        CategoriesResponses instance) =>
    <String, dynamic>{
      'totalCount': instance.totalCount,
      'items': instance.items,
    };
