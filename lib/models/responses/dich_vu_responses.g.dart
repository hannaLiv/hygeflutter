// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dich_vu_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DichVuResponses _$DichVuResponsesFromJson(Map<String, dynamic> json) {
  return DichVuResponses(
    totalCount: json['totalCount'] as int?,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => DichVuEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    thoiGianDTOs: (json['thoiGianDTOs'] as List<dynamic>?)
        ?.map((e) => SanPhamEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    sanPhamBoSungDTOs: (json['sanPhamBoSungDTOs'] as List<dynamic>?)
        ?.map(
            (e) => SanPhamBoSungDtosEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    tuyChonDTOs: (json['tuyChonDTOs'] as List<dynamic>?)
        ?.map((e) => SanPhamEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    errors: json['error'] == null
        ? null
        : ErrorsEntity.fromJson(json['error'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DichVuResponsesToJson(DichVuResponses instance) =>
    <String, dynamic>{
      'totalCount': instance.totalCount,
      'items': instance.items,
      'thoiGianDTOs': instance.thoiGianDTOs,
      'tuyChonDTOs': instance.tuyChonDTOs,
      'sanPhamBoSungDTOs': instance.sanPhamBoSungDTOs,
      'error': instance.errors,
    };
