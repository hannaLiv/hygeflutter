// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_information_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInformationResponse _$UserInformationResponseFromJson(
    Map<String, dynamic> json) {
  return UserInformationResponse(
    educationLevels: (json['educationLevels'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    lifeStage: (json['lifeStage'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    yourFavoriesShopStores: (json['yourFavoriesShopStores'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    favoriesBrands: (json['favoriesBrands'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    yourFavoriesDesigners: (json['yourFavoriesDesigners'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    yourStyle: (json['yourStyle'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    favoriesFragrances: (json['favoriesFragrances'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UserInformationResponseToJson(
        UserInformationResponse instance) =>
    <String, dynamic>{
      'educationLevels': instance.educationLevels,
      'lifeStage': instance.lifeStage,
      'yourFavoriesShopStores': instance.yourFavoriesShopStores,
      'favoriesBrands': instance.favoriesBrands,
      'yourFavoriesDesigners': instance.yourFavoriesDesigners,
      'yourStyle': instance.yourStyle,
      'favoriesFragrances': instance.favoriesFragrances,
    };
