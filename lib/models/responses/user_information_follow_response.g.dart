// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_information_follow_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInformationFollow _$UserInformationFollowFromJson(
    Map<String, dynamic> json) {
  return UserInformationFollow(
    countMeFollowUser: json['countMeFollowUser'] as int?,
    countUserFollowMe: json['countUserFollowMe'] as int?,
    palletColorByUser: json['palletColorByUser'] as int?,
  );
}

Map<String, dynamic> _$UserInformationFollowToJson(
        UserInformationFollow instance) =>
    <String, dynamic>{
      'countMeFollowUser': instance.countMeFollowUser,
      'countUserFollowMe': instance.countUserFollowMe,
      'palletColorByUser': instance.palletColorByUser,
    };
