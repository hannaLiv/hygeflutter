// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_infor_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfoResponse _$UserInfoResponseFromJson(Map<String, dynamic> json) {
  return UserInfoResponse(
    user: json['user'] == null
        ? null
        : UserInformationEntity.fromJson(json['user'] as Map<String, dynamic>),
    shoppings: (json['shoppings'] as List<dynamic>?)
        ?.map((e) => UserInformationEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UserInfoResponseToJson(UserInfoResponse instance) =>
    <String, dynamic>{
      'shoppings': instance.shoppings,
      'user': instance.user,
    };
