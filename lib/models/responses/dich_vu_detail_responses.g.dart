// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dich_vu_detail_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DichVuDetailResponses _$DichVuDetailResponsesFromJson(
    Map<String, dynamic> json) {
  return DichVuDetailResponses(
    thoiGian: (json['thoiGian'] as List<dynamic>?)
        ?.map((e) => CommonEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    tuyChon: (json['tuyChon'] as List<dynamic>?)
        ?.map((e) => CommonEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    sanPhamThem: (json['sanPhamThem'] as List<dynamic>?)
        ?.map((e) => CommonEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    totalCount: json['totalCount'] as int?,
  );
}

Map<String, dynamic> _$DichVuDetailResponsesToJson(
        DichVuDetailResponses instance) =>
    <String, dynamic>{
      'thoiGian': instance.thoiGian,
      'tuyChon': instance.tuyChon,
      'sanPhamThem': instance.sanPhamThem,
      'totalCount': instance.totalCount,
    };
