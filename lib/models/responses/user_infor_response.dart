import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';

import 'extra_properties_response.dart';

part 'user_infor_response.g.dart';

@JsonSerializable()
class UserInfoResponse {
  @JsonKey(name: "shoppings")
  final List<UserInformationEntity>? shoppings;
  @JsonKey(name: "user")
  final UserInformationEntity? user;

  UserInfoResponse({
    this.user,
    this.shoppings,
  });

  factory UserInfoResponse.fromJson(Map<String, dynamic> json) => _$UserInfoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoResponseToJson(this);
}
