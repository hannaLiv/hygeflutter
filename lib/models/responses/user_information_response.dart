import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';

part 'user_information_response.g.dart';

@JsonSerializable()
class UserInformationResponse {
  @JsonKey(name: "educationLevels")
  final List<UserInformationEntity>? educationLevels;
  @JsonKey(name: "lifeStage")
  final List<UserInformationEntity>? lifeStage;
  @JsonKey(name: "yourFavoriesShopStores")
  final List<UserInformationEntity>? yourFavoriesShopStores;
  @JsonKey(name: "favoriesBrands")
  final List<UserInformationEntity>? favoriesBrands;
  @JsonKey(name: "yourFavoriesDesigners")
  final List<UserInformationEntity>? yourFavoriesDesigners;
  @JsonKey(name: "yourStyle")
  final List<UserInformationEntity>? yourStyle;
  @JsonKey(name: "favoriesFragrances")
  final List<UserInformationEntity>? favoriesFragrances;

  UserInformationResponse({
    this.educationLevels,
    this.lifeStage,
    this.yourFavoriesShopStores,
    this.favoriesBrands,
    this.yourFavoriesDesigners,
    this.yourStyle,
    this.favoriesFragrances,
  });

  factory UserInformationResponse.fromJson(Map<String, dynamic> json) => _$UserInformationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserInformationResponseToJson(this);
}
