import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/errors_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/san_pham_bo_sung_dtos_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/san_pham_entity.dart';

part 'dich_vu_responses.g.dart';

@JsonSerializable()
class DichVuResponses {
  @JsonKey(name: "totalCount")
  final int? totalCount;
  @JsonKey(name: "items")
  final List<DichVuEntity>? items;
  @JsonKey(name: "thoiGianDTOs")
  final List<SanPhamEntity>? thoiGianDTOs;
  @JsonKey(name: "tuyChonDTOs")
  final List<SanPhamEntity>? tuyChonDTOs;
  @JsonKey(name: "sanPhamBoSungDTOs")
  final List<SanPhamBoSungDtosEntity>? sanPhamBoSungDTOs;
  @JsonKey(name: "error")
  final ErrorsEntity? errors;

  DichVuResponses({
    this.totalCount,
    this.items,
    this.thoiGianDTOs,
    this.sanPhamBoSungDTOs,
    this.tuyChonDTOs,
    this.errors,
  });

  factory DichVuResponses.fromJson(Map<String, dynamic> json) => _$DichVuResponsesFromJson(json);

  Map<String, dynamic> toJson() => _$DichVuResponsesToJson(this);
}
