import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/common.entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';

part 'dich_vu_detail_responses.g.dart';

@JsonSerializable()
class DichVuDetailResponses {
  @JsonKey(name: "thoiGian")
  final List<CommonEntity>? thoiGian;
  @JsonKey(name: "tuyChon")
  final List<CommonEntity>? tuyChon;
  @JsonKey(name: "sanPhamThem")
  final List<CommonEntity>? sanPhamThem;
  @JsonKey(name: "totalCount")
  final int? totalCount;

  DichVuDetailResponses({
    this.thoiGian,
    this.tuyChon,
    this.sanPhamThem,
    this.totalCount,
  });

  factory DichVuDetailResponses.fromJson(Map<String, dynamic> json) => _$DichVuDetailResponsesFromJson(json);

  Map<String, dynamic> toJson() => _$DichVuDetailResponsesToJson(this);
}
