import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';

part 'categories_responses.g.dart';

@JsonSerializable()
class CategoriesResponses {
  @JsonKey(name: "totalCount")
  final int? totalCount;
  @JsonKey(name: "items")
  final List<UserInformationEntity>? items;

  CategoriesResponses({
    this.totalCount,
    this.items,
  });

  factory CategoriesResponses.fromJson(Map<String, dynamic> json) => _$CategoriesResponsesFromJson(json);

  Map<String, dynamic> toJson() => _$CategoriesResponsesToJson(this);
}
