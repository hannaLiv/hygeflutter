import 'package:json_annotation/json_annotation.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';

part 'user_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class UserResponse {
  @JsonKey(name: 'totalCount')
  final int? totalCount;
  @JsonKey(name: 'items')
  final List<UserInformationEntity>? items;

  UserResponse({
    this.totalCount,
    this.items,
  });

  factory UserResponse.fromJson(Map<String, dynamic> json) => _$UserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
}
