import 'package:json_annotation/json_annotation.dart';

part 'user_information_follow_response.g.dart';

@JsonSerializable()
class UserInformationFollow {
  @JsonKey(name: 'countMeFollowUser')
  int? countMeFollowUser;
  @JsonKey(name: 'countUserFollowMe')
  int? countUserFollowMe;
  @JsonKey(name: 'palletColorByUser')
  int? palletColorByUser;

  UserInformationFollow({
    this.countMeFollowUser,
    this.countUserFollowMe,
    this.palletColorByUser,
  });

  factory UserInformationFollow.fromJson(Map<String, dynamic> json) => _$UserInformationFollowFromJson(json);

  Map<String, dynamic> toJson() => _$UserInformationFollowToJson(this);
}
