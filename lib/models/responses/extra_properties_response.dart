import 'package:json_annotation/json_annotation.dart';

part 'extra_properties_response.g.dart';

@JsonSerializable()
class ExtraProperties {
  ExtraProperties();

  factory ExtraProperties.fromJson(Map<String, dynamic> json) =>
      _$ExtraPropertiesFromJson(json);

  Map<String, dynamic> toJson() => _$ExtraPropertiesToJson(this);
}
