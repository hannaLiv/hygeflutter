class SanPhamDTOModel{
  String ?ma;
  String ?ten;
  int ?typeId;
  int ?statusId;
  String ?ghiChu;
  int ?idDichVu;
  int  ?loaiSanPham;
  double  ?giaGoc;
  String   ?maMau;
  String  ?icon;
  DateTime  ?creationTime;
  String  ?creatorId;
  int ?id;

  SanPhamDTOModel({this.creationTime,this.creatorId,this.ghiChu,this.giaGoc,this.icon,this.id,this.idDichVu,this.loaiSanPham,this.ma,this.maMau,this.statusId,this.ten,this.typeId});

  factory SanPhamDTOModel.fromJson(Map<String, dynamic> ?json) {
    var result = new SanPhamDTOModel();
    if (json == null) return result;
    result = new SanPhamDTOModel(
      ma: json["ma"],
      ten: json["ten"],
      typeId: json["typeId"],
      statusId: json["statusId"],
      ghiChu: json["ghiChu"],
      idDichVu: json["idDichVu"],
      loaiSanPham: json["loaiSanPham"],
      giaGoc: json["giaGoc"],
      maMau: json["maMau"],
      icon: json["icon"],
      creationTime: json["creationTime"],
      creatorId: json["creatorId"],
      id: json["id"],
    );
    return result;
  }

  static List<SanPhamDTOModel> fromJsonList(List ?list) {
     List<SanPhamDTOModel> listResult = new  List<SanPhamDTOModel>.empty(growable: true);
    if (list == null) return listResult;
    return list.map((item) => SanPhamDTOModel.fromJson(item)).toList();
  }

  static List<Map<String, dynamic>> toJsonList(List<SanPhamDTOModel> ?list) {
    List<Map<String, dynamic>> result =[];
    if (list == null) return result;
    return list.map((item) => item.toJson()).toList();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "ma": this.ma,
      "ten": this.ten,
      "typeId": this.typeId,
      "statusId": this.statusId,
      "ghiChu": this.ghiChu,
      "idDichVu": this.idDichVu,
      "loaiSanPham": this.loaiSanPham,
      "giaGoc": this.giaGoc,
      "maMau": this.maMau,
      "icon": this.icon,
      "creationTime": this.creationTime,
      "creatorId": this.creatorId,
      "id": this.id,
    };
    return map;
  }
}