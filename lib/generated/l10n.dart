// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Settings`
  String get settings_title {
    return Intl.message(
      'Settings',
      name: 'settings_title',
      desc: '',
      args: [],
    );
  }

  /// `What do you want to choose image by ?`
  String get choose_type_image_picker {
    return Intl.message(
      'What do you want to choose image by ?',
      name: 'choose_type_image_picker',
      desc: '',
      args: [],
    );
  }

  /// `Camera`
  String get camera_title {
    return Intl.message(
      'Camera',
      name: 'camera_title',
      desc: '',
      args: [],
    );
  }

  /// `Gallery`
  String get gallery_title {
    return Intl.message(
      'Gallery',
      name: 'gallery_title',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get home_title {
    return Intl.message(
      'Home',
      name: 'home_title',
      desc: '',
      args: [],
    );
  }

  /// `My Twinkle Colors`
  String get my_twinkle_colors_title {
    return Intl.message(
      'My Twinkle Colors',
      name: 'my_twinkle_colors_title',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search_title {
    return Intl.message(
      'Search',
      name: 'search_title',
      desc: '',
      args: [],
    );
  }

  /// `Find Friends`
  String get find_friends_title {
    return Intl.message(
      'Find Friends',
      name: 'find_friends_title',
      desc: '',
      args: [],
    );
  }

  /// `Account`
  String get account_title {
    return Intl.message(
      'Account',
      name: 'account_title',
      desc: '',
      args: [],
    );
  }

  /// `COLOR PALETTES`
  String get color_palettes_title {
    return Intl.message(
      'COLOR PALETTES',
      name: 'color_palettes_title',
      desc: '',
      args: [],
    );
  }

  /// `FOLLOWERS`
  String get followers_title {
    return Intl.message(
      'FOLLOWERS',
      name: 'followers_title',
      desc: '',
      args: [],
    );
  }

  /// `FOLLOWINGS`
  String get following_title {
    return Intl.message(
      'FOLLOWINGS',
      name: 'following_title',
      desc: '',
      args: [],
    );
  }

  /// `Edit info`
  String get edit_info_title {
    return Intl.message(
      'Edit info',
      name: 'edit_info_title',
      desc: '',
      args: [],
    );
  }

  /// `About me`
  String get about_me_title {
    return Intl.message(
      'About me',
      name: 'about_me_title',
      desc: '',
      args: [],
    );
  }

  /// `My work`
  String get my_work_title {
    return Intl.message(
      'My work',
      name: 'my_work_title',
      desc: '',
      args: [],
    );
  }

  /// `School`
  String get school_title {
    return Intl.message(
      'School',
      name: 'school_title',
      desc: '',
      args: [],
    );
  }

  /// `Living in`
  String get living_in_title {
    return Intl.message(
      'Living in',
      name: 'living_in_title',
      desc: '',
      args: [],
    );
  }

  /// `Birthday`
  String get birthday_title {
    return Intl.message(
      'Birthday',
      name: 'birthday_title',
      desc: '',
      args: [],
    );
  }

  /// `Basic information`
  String get basic_information_title {
    return Intl.message(
      'Basic information',
      name: 'basic_information_title',
      desc: '',
      args: [],
    );
  }

  /// `Lifestyle`
  String get life_style_title {
    return Intl.message(
      'Lifestyle',
      name: 'life_style_title',
      desc: '',
      args: [],
    );
  }

  /// `Account Setting`
  String get account_setting_title {
    return Intl.message(
      'Account Setting',
      name: 'account_setting_title',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get username_title {
    return Intl.message(
      'Username',
      name: 'username_title',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password_title {
    return Intl.message(
      'Password',
      name: 'password_title',
      desc: '',
      args: [],
    );
  }

  /// `Phone number`
  String get phone_number_title {
    return Intl.message(
      'Phone number',
      name: 'phone_number_title',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email_title {
    return Intl.message(
      'Email',
      name: 'email_title',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save_title {
    return Intl.message(
      'Save',
      name: 'save_title',
      desc: '',
      args: [],
    );
  }

  /// `Location`
  String get location_title {
    return Intl.message(
      'Location',
      name: 'location_title',
      desc: '',
      args: [],
    );
  }

  /// `Country`
  String get country_title {
    return Intl.message(
      'Country',
      name: 'country_title',
      desc: '',
      args: [],
    );
  }

  /// `State`
  String get state_title {
    return Intl.message(
      'State',
      name: 'state_title',
      desc: '',
      args: [],
    );
  }

  /// `City`
  String get city_title {
    return Intl.message(
      'City',
      name: 'city_title',
      desc: '',
      args: [],
    );
  }

  /// `Use my current location`
  String get use_my_current_location {
    return Intl.message(
      'Use my current location',
      name: 'use_my_current_location',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get notifications_title {
    return Intl.message(
      'Notifications',
      name: 'notifications_title',
      desc: '',
      args: [],
    );
  }

  /// `Push notifications`
  String get push_notifications_title {
    return Intl.message(
      'Push notifications',
      name: 'push_notifications_title',
      desc: '',
      args: [],
    );
  }

  /// `Team MyTwinkleColor`
  String get team_my_twinkle_color {
    return Intl.message(
      'Team MyTwinkleColor',
      name: 'team_my_twinkle_color',
      desc: '',
      args: [],
    );
  }

  /// `Add city`
  String get edit_info_add_city {
    return Intl.message(
      'Add city',
      name: 'edit_info_add_city',
      desc: '',
      args: [],
    );
  }

  /// `About yourself`
  String get edit_info_about_yourself {
    return Intl.message(
      'About yourself',
      name: 'edit_info_about_yourself',
      desc: '',
      args: [],
    );
  }

  /// `Your current job`
  String get edit_info_your_job {
    return Intl.message(
      'Your current job',
      name: 'edit_info_your_job',
      desc: '',
      args: [],
    );
  }

  /// `Your last education`
  String get edit_info_your_education {
    return Intl.message(
      'Your last education',
      name: 'edit_info_your_education',
      desc: '',
      args: [],
    );
  }

  /// `Create your own color palettes`
  String get slide_create_your_own_color_palettes {
    return Intl.message(
      'Create your own color palettes',
      name: 'slide_create_your_own_color_palettes',
      desc: '',
      args: [],
    );
  }

  /// `Lorem ipsum dolor sit amet, consectetur or adipiscing elit. Proin sit amet eros velit.`
  String get slide_description {
    return Intl.message(
      'Lorem ipsum dolor sit amet, consectetur or adipiscing elit. Proin sit amet eros velit.',
      name: 'slide_description',
      desc: '',
      args: [],
    );
  }

  /// `500+ proprietary color library`
  String get slide_title_color_lib {
    return Intl.message(
      '500+ proprietary color library',
      name: 'slide_title_color_lib',
      desc: '',
      args: [],
    );
  }

  /// `Not a member?`
  String get slide_not_a_member {
    return Intl.message(
      'Not a member?',
      name: 'slide_not_a_member',
      desc: '',
      args: [],
    );
  }

  /// `Enter your name! Try again.`
  String get common_error_empty_username {
    return Intl.message(
      'Enter your name! Try again.',
      name: 'common_error_empty_username',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect password! Try again.`
  String get common_error_empty_password {
    return Intl.message(
      'Incorrect password! Try again.',
      name: 'common_error_empty_password',
      desc: '',
      args: [],
    );
  }

  /// `Password must be at least 6 characters with format as Aa@1`
  String get common_error_format_password {
    return Intl.message(
      'Password must be at least 6 characters with format as Aa@1',
      name: 'common_error_format_password',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect email! Try again.`
  String get common_error_empty_email {
    return Intl.message(
      'Incorrect email! Try again.',
      name: 'common_error_empty_email',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect email format !`
  String get common_error_format_email {
    return Intl.message(
      'Incorrect email format !',
      name: 'common_error_format_email',
      desc: '',
      args: [],
    );
  }

  /// `Continue with Facebook`
  String get common_button_continue_with_face {
    return Intl.message(
      'Continue with Facebook',
      name: 'common_button_continue_with_face',
      desc: '',
      args: [],
    );
  }

  /// `Continue with Google`
  String get common_button_continue_with_google {
    return Intl.message(
      'Continue with Google',
      name: 'common_button_continue_with_google',
      desc: '',
      args: [],
    );
  }

  /// `Sign In`
  String get common_button_sign_in {
    return Intl.message(
      'Sign In',
      name: 'common_button_sign_in',
      desc: '',
      args: [],
    );
  }

  /// `Sign Up`
  String get common_button_sign_up {
    return Intl.message(
      'Sign Up',
      name: 'common_button_sign_up',
      desc: '',
      args: [],
    );
  }

  /// `Sign Out`
  String get common_button_sign_out {
    return Intl.message(
      'Sign Out',
      name: 'common_button_sign_out',
      desc: '',
      args: [],
    );
  }

  /// `Loading`
  String get common_button_loading {
    return Intl.message(
      'Loading',
      name: 'common_button_loading',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get common_button_cancel {
    return Intl.message(
      'Cancel',
      name: 'common_button_cancel',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get common_button_next {
    return Intl.message(
      'Next',
      name: 'common_button_next',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get common_button_continue {
    return Intl.message(
      'Continue',
      name: 'common_button_continue',
      desc: '',
      args: [],
    );
  }

  /// `You're done`
  String get common_button_you_done {
    return Intl.message(
      'You\'re done',
      name: 'common_button_you_done',
      desc: '',
      args: [],
    );
  }

  /// `Hoạt động`
  String get common_button_action {
    return Intl.message(
      'Hoạt động',
      name: 'common_button_action',
      desc: '',
      args: [],
    );
  }

  /// `Đặt dịch vụ`
  String get common_button_dat_dich_vu {
    return Intl.message(
      'Đặt dịch vụ',
      name: 'common_button_dat_dich_vu',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get common_text_error {
    return Intl.message(
      'Error',
      name: 'common_text_error',
      desc: '',
      args: [],
    );
  }

  /// `Sign in Failure`
  String get common_text_sign_in_failure {
    return Intl.message(
      'Sign in Failure',
      name: 'common_text_sign_in_failure',
      desc: '',
      args: [],
    );
  }

  /// `Sign up Failure`
  String get common_text_sign_up_failure {
    return Intl.message(
      'Sign up Failure',
      name: 'common_text_sign_up_failure',
      desc: '',
      args: [],
    );
  }

  /// `Having some errors when connect to server`
  String get common_text_error_au_account {
    return Intl.message(
      'Having some errors when connect to server',
      name: 'common_text_error_au_account',
      desc: '',
      args: [],
    );
  }

  /// `Having some errors when connect with Google account`
  String get common_text_error_au_google {
    return Intl.message(
      'Having some errors when connect with Google account',
      name: 'common_text_error_au_google',
      desc: '',
      args: [],
    );
  }

  /// `Having some errors when connect with Facebook account`
  String get common_text_error_au_facebook {
    return Intl.message(
      'Having some errors when connect with Facebook account',
      name: 'common_text_error_au_facebook',
      desc: '',
      args: [],
    );
  }

  /// `Hyge`
  String get common_app_name {
    return Intl.message(
      'Hyge',
      name: 'common_app_name',
      desc: '',
      args: [],
    );
  }

  /// `Welcome Back!`
  String get sign_in_welcome_back {
    return Intl.message(
      'Welcome Back!',
      name: 'sign_in_welcome_back',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password?`
  String get sign_in_forgot_password {
    return Intl.message(
      'Forgot password?',
      name: 'sign_in_forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `Not a member?`
  String get sign_in_not_a_member {
    return Intl.message(
      'Not a member?',
      name: 'sign_in_not_a_member',
      desc: '',
      args: [],
    );
  }

  /// `Or sign in with email`
  String get sign_in_with_email {
    return Intl.message(
      'Or sign in with email',
      name: 'sign_in_with_email',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get sign_up_name {
    return Intl.message(
      'Name',
      name: 'sign_up_name',
      desc: '',
      args: [],
    );
  }

  /// `Email address`
  String get sign_up_email {
    return Intl.message(
      'Email address',
      name: 'sign_up_email',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get sign_up_password {
    return Intl.message(
      'Password',
      name: 'sign_up_password',
      desc: '',
      args: [],
    );
  }

  /// `Create Your Account`
  String get sign_up_create_your_account {
    return Intl.message(
      'Create Your Account',
      name: 'sign_up_create_your_account',
      desc: '',
      args: [],
    );
  }

  /// `Or sign in with email`
  String get sign_up_with_email {
    return Intl.message(
      'Or sign in with email',
      name: 'sign_up_with_email',
      desc: '',
      args: [],
    );
  }

  /// `I have read the`
  String get sign_up_have_read {
    return Intl.message(
      'I have read the',
      name: 'sign_up_have_read',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get sign_up_read_policy {
    return Intl.message(
      'Privacy Policy',
      name: 'sign_up_read_policy',
      desc: '',
      args: [],
    );
  }

  /// `Let's get started`
  String get sign_up_let_started {
    return Intl.message(
      'Let\'s get started',
      name: 'sign_up_let_started',
      desc: '',
      args: [],
    );
  }

  /// `Take a few minutes to tell us about yourself.`
  String get sign_up_take_a_minutes {
    return Intl.message(
      'Take a few minutes to tell us about yourself.',
      name: 'sign_up_take_a_minutes',
      desc: '',
      args: [],
    );
  }

  /// `Hi, Friend! `
  String get sign_up_hi_friend {
    return Intl.message(
      'Hi, Friend! ',
      name: 'sign_up_hi_friend',
      desc: '',
      args: [],
    );
  }

  /// `We 'd love to get to`
  String get sign_up_we_get_to {
    return Intl.message(
      'We \'d love to get to',
      name: 'sign_up_we_get_to',
      desc: '',
      args: [],
    );
  }

  /// `know you`
  String get sign_up_know_you {
    return Intl.message(
      'know you',
      name: 'sign_up_know_you',
      desc: '',
      args: [],
    );
  }

  /// `I’d rather not say`
  String get sign_up_rather_not_say {
    return Intl.message(
      'I’d rather not say',
      name: 'sign_up_rather_not_say',
      desc: '',
      args: [],
    );
  }

  /// `Choose  Your`
  String get sign_up_choose_your {
    return Intl.message(
      'Choose  Your',
      name: 'sign_up_choose_your',
      desc: '',
      args: [],
    );
  }

  /// `Favorite Brands`
  String get sign_up_favorite_brand {
    return Intl.message(
      'Favorite Brands',
      name: 'sign_up_favorite_brand',
      desc: '',
      args: [],
    );
  }

  /// `Already a member?`
  String get sign_up_already_number {
    return Intl.message(
      'Already a member?',
      name: 'sign_up_already_number',
      desc: '',
      args: [],
    );
  }

  /// `My work`
  String get sign_up_my_work {
    return Intl.message(
      'My work',
      name: 'sign_up_my_work',
      desc: '',
      args: [],
    );
  }

  /// `What Is Your `
  String get sign_up_question_what_is_your {
    return Intl.message(
      'What Is Your ',
      name: 'sign_up_question_what_is_your',
      desc: '',
      args: [],
    );
  }

  /// `Current Job?`
  String get sign_up_question_current_job {
    return Intl.message(
      'Current Job?',
      name: 'sign_up_question_current_job',
      desc: '',
      args: [],
    );
  }

  /// `Your current job`
  String get sign_up_current_job {
    return Intl.message(
      'Your current job',
      name: 'sign_up_current_job',
      desc: '',
      args: [],
    );
  }

  /// `My school`
  String get sign_up_my_school {
    return Intl.message(
      'My school',
      name: 'sign_up_my_school',
      desc: '',
      args: [],
    );
  }

  /// `Add Your`
  String get sign_up_add_your {
    return Intl.message(
      'Add Your',
      name: 'sign_up_add_your',
      desc: '',
      args: [],
    );
  }

  /// `Education Level`
  String get sign_up_education_level {
    return Intl.message(
      'Education Level',
      name: 'sign_up_education_level',
      desc: '',
      args: [],
    );
  }

  /// `Living in`
  String get sign_up_living_in {
    return Intl.message(
      'Living in',
      name: 'sign_up_living_in',
      desc: '',
      args: [],
    );
  }

  /// `Where Are You`
  String get sign_up_where_are_you {
    return Intl.message(
      'Where Are You',
      name: 'sign_up_where_are_you',
      desc: '',
      args: [],
    );
  }

  /// `Living In?`
  String get sign_up_question_living_in {
    return Intl.message(
      'Living In?',
      name: 'sign_up_question_living_in',
      desc: '',
      args: [],
    );
  }

  /// `Your City`
  String get sign_up_your_city {
    return Intl.message(
      'Your City',
      name: 'sign_up_your_city',
      desc: '',
      args: [],
    );
  }

  /// `Enter your content`
  String get sign_up_enter_your_content {
    return Intl.message(
      'Enter your content',
      name: 'sign_up_enter_your_content',
      desc: '',
      args: [],
    );
  }

  /// `My birthday`
  String get sign_up_my_birthday {
    return Intl.message(
      'My birthday',
      name: 'sign_up_my_birthday',
      desc: '',
      args: [],
    );
  }

  /// `Birthday?`
  String get sign_up_birthday {
    return Intl.message(
      'Birthday?',
      name: 'sign_up_birthday',
      desc: '',
      args: [],
    );
  }

  /// `Basic Information`
  String get sign_up_basic_information {
    return Intl.message(
      'Basic Information',
      name: 'sign_up_basic_information',
      desc: '',
      args: [],
    );
  }

  /// `Height?`
  String get sign_up_height {
    return Intl.message(
      'Height?',
      name: 'sign_up_height',
      desc: '',
      args: [],
    );
  }

  /// `Weight?`
  String get sign_up_weight {
    return Intl.message(
      'Weight?',
      name: 'sign_up_weight',
      desc: '',
      args: [],
    );
  }

  /// `Lifestyle`
  String get sign_up_lifestyle {
    return Intl.message(
      'Lifestyle',
      name: 'sign_up_lifestyle',
      desc: '',
      args: [],
    );
  }

  /// `Life Stage`
  String get sign_up_life_stage {
    return Intl.message(
      'Life Stage',
      name: 'sign_up_life_stage',
      desc: '',
      args: [],
    );
  }

  /// `Favorite Shops/Stores`
  String get sign_up_favorite_shops {
    return Intl.message(
      'Favorite Shops/Stores',
      name: 'sign_up_favorite_shops',
      desc: '',
      args: [],
    );
  }

  /// `Favorite Designers`
  String get sign_up_favorite_designers {
    return Intl.message(
      'Favorite Designers',
      name: 'sign_up_favorite_designers',
      desc: '',
      args: [],
    );
  }

  /// `Styles`
  String get sign_up_styles {
    return Intl.message(
      'Styles',
      name: 'sign_up_styles',
      desc: '',
      args: [],
    );
  }

  /// `Favorite Fragrances`
  String get sign_up_favorite_fragrances {
    return Intl.message(
      'Favorite Fragrances',
      name: 'sign_up_favorite_fragrances',
      desc: '',
      args: [],
    );
  }

  /// `Enter your City`
  String get sign_up_enter_your_city {
    return Intl.message(
      'Enter your City',
      name: 'sign_up_enter_your_city',
      desc: '',
      args: [],
    );
  }

  /// `Select your birthday`
  String get sign_up_enter_your_birthdate {
    return Intl.message(
      'Select your birthday',
      name: 'sign_up_enter_your_birthdate',
      desc: '',
      args: [],
    );
  }

  /// `Search Color Palette`
  String get home_search_bar_hint {
    return Intl.message(
      'Search Color Palette',
      name: 'home_search_bar_hint',
      desc: '',
      args: [],
    );
  }

  /// `Welcome, `
  String get home_welcome {
    return Intl.message(
      'Welcome, ',
      name: 'home_welcome',
      desc: '',
      args: [],
    );
  }

  /// `Scan the colors when you're shopping`
  String get home_scan_optional {
    return Intl.message(
      'Scan the colors when you\'re shopping',
      name: 'home_scan_optional',
      desc: '',
      args: [],
    );
  }

  /// `Scan by Camera`
  String get home_scan_by_camera {
    return Intl.message(
      'Scan by Camera',
      name: 'home_scan_by_camera',
      desc: '',
      args: [],
    );
  }

  /// `Scan by Gallery`
  String get home_scan_by_gallery {
    return Intl.message(
      'Scan by Gallery',
      name: 'home_scan_by_gallery',
      desc: '',
      args: [],
    );
  }

  /// `Twinkle color categories`
  String get home_Twinkle_color_categories {
    return Intl.message(
      'Twinkle color categories',
      name: 'home_Twinkle_color_categories',
      desc: '',
      args: [],
    );
  }

  /// `Purple Palette`
  String get home_categories_detail_purple_palette {
    return Intl.message(
      'Purple Palette',
      name: 'home_categories_detail_purple_palette',
      desc: '',
      args: [],
    );
  }

  /// `Dịch vụ`
  String get home_services {
    return Intl.message(
      'Dịch vụ',
      name: 'home_services',
      desc: '',
      args: [],
    );
  }

  /// `Xin chào`
  String get home_hello {
    return Intl.message(
      'Xin chào',
      name: 'home_hello',
      desc: '',
      args: [],
    );
  }

  /// `Hôm nay bạn chọn dịch vụ nào của `
  String get home_ask_choosen {
    return Intl.message(
      'Hôm nay bạn chọn dịch vụ nào của ',
      name: 'home_ask_choosen',
      desc: '',
      args: [],
    );
  }

  /// `Ưu đãi`
  String get home_uu_dai {
    return Intl.message(
      'Ưu đãi',
      name: 'home_uu_dai',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'vi'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
