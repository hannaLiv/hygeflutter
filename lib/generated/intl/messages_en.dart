// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about_me_title": MessageLookupByLibrary.simpleMessage("About me"),
        "account_setting_title":
            MessageLookupByLibrary.simpleMessage("Account Setting"),
        "account_title": MessageLookupByLibrary.simpleMessage("Account"),
        "basic_information_title":
            MessageLookupByLibrary.simpleMessage("Basic information"),
        "birthday_title": MessageLookupByLibrary.simpleMessage("Birthday"),
        "camera_title": MessageLookupByLibrary.simpleMessage("Camera"),
        "choose_type_image_picker": MessageLookupByLibrary.simpleMessage(
            "What do you want to choose image by ?"),
        "city_title": MessageLookupByLibrary.simpleMessage("City"),
        "color_palettes_title":
            MessageLookupByLibrary.simpleMessage("COLOR PALETTES"),
        "common_app_name": MessageLookupByLibrary.simpleMessage("Hyge"),
        "common_button_action":
            MessageLookupByLibrary.simpleMessage("Hoạt động"),
        "common_button_cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "common_button_continue":
            MessageLookupByLibrary.simpleMessage("Continue"),
        "common_button_continue_with_face":
            MessageLookupByLibrary.simpleMessage("Continue with Facebook"),
        "common_button_continue_with_google":
            MessageLookupByLibrary.simpleMessage("Continue with Google"),
        "common_button_dat_dich_vu":
            MessageLookupByLibrary.simpleMessage("Đặt dịch vụ"),
        "common_button_loading":
            MessageLookupByLibrary.simpleMessage("Loading"),
        "common_button_next": MessageLookupByLibrary.simpleMessage("Next"),
        "common_button_sign_in":
            MessageLookupByLibrary.simpleMessage("Sign In"),
        "common_button_sign_out":
            MessageLookupByLibrary.simpleMessage("Sign Out"),
        "common_button_sign_up":
            MessageLookupByLibrary.simpleMessage("Sign Up"),
        "common_button_you_done":
            MessageLookupByLibrary.simpleMessage("You\'re done"),
        "common_error_empty_email":
            MessageLookupByLibrary.simpleMessage("Incorrect email! Try again."),
        "common_error_empty_password": MessageLookupByLibrary.simpleMessage(
            "Incorrect password! Try again."),
        "common_error_empty_username":
            MessageLookupByLibrary.simpleMessage("Enter your name! Try again."),
        "common_error_format_email":
            MessageLookupByLibrary.simpleMessage("Incorrect email format !"),
        "common_error_format_password": MessageLookupByLibrary.simpleMessage(
            "Password must be at least 6 characters with format as Aa@1"),
        "common_text_error": MessageLookupByLibrary.simpleMessage("Error"),
        "common_text_error_au_account": MessageLookupByLibrary.simpleMessage(
            "Having some errors when connect to server"),
        "common_text_error_au_facebook": MessageLookupByLibrary.simpleMessage(
            "Having some errors when connect with Facebook account"),
        "common_text_error_au_google": MessageLookupByLibrary.simpleMessage(
            "Having some errors when connect with Google account"),
        "common_text_sign_in_failure":
            MessageLookupByLibrary.simpleMessage("Sign in Failure"),
        "common_text_sign_up_failure":
            MessageLookupByLibrary.simpleMessage("Sign up Failure"),
        "country_title": MessageLookupByLibrary.simpleMessage("Country"),
        "edit_info_about_yourself":
            MessageLookupByLibrary.simpleMessage("About yourself"),
        "edit_info_add_city": MessageLookupByLibrary.simpleMessage("Add city"),
        "edit_info_title": MessageLookupByLibrary.simpleMessage("Edit info"),
        "edit_info_your_education":
            MessageLookupByLibrary.simpleMessage("Your last education"),
        "edit_info_your_job":
            MessageLookupByLibrary.simpleMessage("Your current job"),
        "email_title": MessageLookupByLibrary.simpleMessage("Email"),
        "find_friends_title":
            MessageLookupByLibrary.simpleMessage("Find Friends"),
        "followers_title": MessageLookupByLibrary.simpleMessage("FOLLOWERS"),
        "following_title": MessageLookupByLibrary.simpleMessage("FOLLOWINGS"),
        "gallery_title": MessageLookupByLibrary.simpleMessage("Gallery"),
        "home_Twinkle_color_categories":
            MessageLookupByLibrary.simpleMessage("Twinkle color categories"),
        "home_ask_choosen": MessageLookupByLibrary.simpleMessage(
            "Hôm nay bạn chọn dịch vụ nào của "),
        "home_categories_detail_purple_palette":
            MessageLookupByLibrary.simpleMessage("Purple Palette"),
        "home_hello": MessageLookupByLibrary.simpleMessage("Xin chào"),
        "home_scan_by_camera":
            MessageLookupByLibrary.simpleMessage("Scan by Camera"),
        "home_scan_by_gallery":
            MessageLookupByLibrary.simpleMessage("Scan by Gallery"),
        "home_scan_optional": MessageLookupByLibrary.simpleMessage(
            "Scan the colors when you\'re shopping"),
        "home_search_bar_hint":
            MessageLookupByLibrary.simpleMessage("Search Color Palette"),
        "home_services": MessageLookupByLibrary.simpleMessage("Dịch vụ"),
        "home_title": MessageLookupByLibrary.simpleMessage("Home"),
        "home_uu_dai": MessageLookupByLibrary.simpleMessage("Ưu đãi"),
        "home_welcome": MessageLookupByLibrary.simpleMessage("Welcome, "),
        "life_style_title": MessageLookupByLibrary.simpleMessage("Lifestyle"),
        "living_in_title": MessageLookupByLibrary.simpleMessage("Living in"),
        "location_title": MessageLookupByLibrary.simpleMessage("Location"),
        "my_twinkle_colors_title":
            MessageLookupByLibrary.simpleMessage("My Twinkle Colors"),
        "my_work_title": MessageLookupByLibrary.simpleMessage("My work"),
        "notifications_title":
            MessageLookupByLibrary.simpleMessage("Notifications"),
        "password_title": MessageLookupByLibrary.simpleMessage("Password"),
        "phone_number_title":
            MessageLookupByLibrary.simpleMessage("Phone number"),
        "push_notifications_title":
            MessageLookupByLibrary.simpleMessage("Push notifications"),
        "save_title": MessageLookupByLibrary.simpleMessage("Save"),
        "school_title": MessageLookupByLibrary.simpleMessage("School"),
        "search_title": MessageLookupByLibrary.simpleMessage("Search"),
        "settings_title": MessageLookupByLibrary.simpleMessage("Settings"),
        "sign_in_forgot_password":
            MessageLookupByLibrary.simpleMessage("Forgot password?"),
        "sign_in_not_a_member":
            MessageLookupByLibrary.simpleMessage("Not a member?"),
        "sign_in_welcome_back":
            MessageLookupByLibrary.simpleMessage("Welcome Back!"),
        "sign_in_with_email":
            MessageLookupByLibrary.simpleMessage("Or sign in with email"),
        "sign_up_add_your": MessageLookupByLibrary.simpleMessage("Add Your"),
        "sign_up_already_number":
            MessageLookupByLibrary.simpleMessage("Already a member?"),
        "sign_up_basic_information":
            MessageLookupByLibrary.simpleMessage("Basic Information"),
        "sign_up_birthday": MessageLookupByLibrary.simpleMessage("Birthday?"),
        "sign_up_choose_your":
            MessageLookupByLibrary.simpleMessage("Choose  Your"),
        "sign_up_create_your_account":
            MessageLookupByLibrary.simpleMessage("Create Your Account"),
        "sign_up_current_job":
            MessageLookupByLibrary.simpleMessage("Your current job"),
        "sign_up_education_level":
            MessageLookupByLibrary.simpleMessage("Education Level"),
        "sign_up_email": MessageLookupByLibrary.simpleMessage("Email address"),
        "sign_up_enter_your_birthdate":
            MessageLookupByLibrary.simpleMessage("Select your birthday"),
        "sign_up_enter_your_city":
            MessageLookupByLibrary.simpleMessage("Enter your City"),
        "sign_up_enter_your_content":
            MessageLookupByLibrary.simpleMessage("Enter your content"),
        "sign_up_favorite_brand":
            MessageLookupByLibrary.simpleMessage("Favorite Brands"),
        "sign_up_favorite_designers":
            MessageLookupByLibrary.simpleMessage("Favorite Designers"),
        "sign_up_favorite_fragrances":
            MessageLookupByLibrary.simpleMessage("Favorite Fragrances"),
        "sign_up_favorite_shops":
            MessageLookupByLibrary.simpleMessage("Favorite Shops/Stores"),
        "sign_up_have_read":
            MessageLookupByLibrary.simpleMessage("I have read the"),
        "sign_up_height": MessageLookupByLibrary.simpleMessage("Height?"),
        "sign_up_hi_friend":
            MessageLookupByLibrary.simpleMessage("Hi, Friend! "),
        "sign_up_know_you": MessageLookupByLibrary.simpleMessage("know you"),
        "sign_up_let_started":
            MessageLookupByLibrary.simpleMessage("Let\'s get started"),
        "sign_up_life_stage":
            MessageLookupByLibrary.simpleMessage("Life Stage"),
        "sign_up_lifestyle": MessageLookupByLibrary.simpleMessage("Lifestyle"),
        "sign_up_living_in": MessageLookupByLibrary.simpleMessage("Living in"),
        "sign_up_my_birthday":
            MessageLookupByLibrary.simpleMessage("My birthday"),
        "sign_up_my_school": MessageLookupByLibrary.simpleMessage("My school"),
        "sign_up_my_work": MessageLookupByLibrary.simpleMessage("My work"),
        "sign_up_name": MessageLookupByLibrary.simpleMessage("Name"),
        "sign_up_password": MessageLookupByLibrary.simpleMessage("Password"),
        "sign_up_question_current_job":
            MessageLookupByLibrary.simpleMessage("Current Job?"),
        "sign_up_question_living_in":
            MessageLookupByLibrary.simpleMessage("Living In?"),
        "sign_up_question_what_is_your":
            MessageLookupByLibrary.simpleMessage("What Is Your "),
        "sign_up_rather_not_say":
            MessageLookupByLibrary.simpleMessage("I’d rather not say"),
        "sign_up_read_policy":
            MessageLookupByLibrary.simpleMessage("Privacy Policy"),
        "sign_up_styles": MessageLookupByLibrary.simpleMessage("Styles"),
        "sign_up_take_a_minutes": MessageLookupByLibrary.simpleMessage(
            "Take a few minutes to tell us about yourself."),
        "sign_up_we_get_to":
            MessageLookupByLibrary.simpleMessage("We \'d love to get to"),
        "sign_up_weight": MessageLookupByLibrary.simpleMessage("Weight?"),
        "sign_up_where_are_you":
            MessageLookupByLibrary.simpleMessage("Where Are You"),
        "sign_up_with_email":
            MessageLookupByLibrary.simpleMessage("Or sign in with email"),
        "sign_up_your_city": MessageLookupByLibrary.simpleMessage("Your City"),
        "slide_create_your_own_color_palettes":
            MessageLookupByLibrary.simpleMessage(
                "Create your own color palettes"),
        "slide_description": MessageLookupByLibrary.simpleMessage(
            "Lorem ipsum dolor sit amet, consectetur or adipiscing elit. Proin sit amet eros velit."),
        "slide_not_a_member":
            MessageLookupByLibrary.simpleMessage("Not a member?"),
        "slide_title_color_lib": MessageLookupByLibrary.simpleMessage(
            "500+ proprietary color library"),
        "state_title": MessageLookupByLibrary.simpleMessage("State"),
        "team_my_twinkle_color":
            MessageLookupByLibrary.simpleMessage("Team MyTwinkleColor"),
        "use_my_current_location":
            MessageLookupByLibrary.simpleMessage("Use my current location"),
        "username_title": MessageLookupByLibrary.simpleMessage("Username")
      };
}
