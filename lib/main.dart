import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/routers/route_config.dart';
import 'package:twinkle_colors_flutter_app/services/api_service.dart';
import 'package:twinkle_colors_flutter_app/services/auth_service.dart';
import 'package:twinkle_colors_flutter_app/services/user_api_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/splash/splash_page.dart';

void main() async {
  await initServices();
  runApp(MyApp());
}

Future initServices() async {
  await Get.putAsync(() => ApiService().init());
  await Get.putAsync(() => AuthService().init());
  await Get.putAsync(() => UserApiService().init());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: hideKeyboard,
      child: GetMaterialApp(
        home: SplashPage(),
        theme: AppThemes.lightTheme,
        themeMode: ThemeMode.system,
              //initialRoute: RoutesConfig.otp,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          S.delegate,
        ],
        locale: Locale.fromSubtags(languageCode: 'en'),
        supportedLocales: S.delegate.supportedLocales,
      ),
    );
  }

  /// Hidden keyboard when out tap
  void hideKeyboard() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }
}
