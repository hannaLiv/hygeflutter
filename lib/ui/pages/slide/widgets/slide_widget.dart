import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';

class SlideWidget extends StatelessWidget {
  final int? indexIndicator;
  final String? title;
  final String? description;
  final String? imageLink;
  final VoidCallback? onNextTap;
  final bool isShowNextTap;
  final VoidCallback? onSignInTap;
  final VoidCallback? onSignUpTap;

  const SlideWidget({
    this.indexIndicator,
    this.title,
    this.description,
    this.onNextTap,
    this.isShowNextTap = true,
    this.onSignInTap,
    this.onSignUpTap,
    this.imageLink,
  });

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height / 2.2,
          margin: EdgeInsets.only(bottom: 45),
          child: Image.asset(
            imageLink ?? AppImages.firstSlideImage,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(height: 45),
        _buildTextWidget(title: title),
        _buildTextWidget(
          title: description,
          textStyle: AppTextStyle.greyS15W400,
          padding: 30,
        ),
        isShowNextTap ? _buildButtonNext() : _buildSignWidget(context),
        SizedBox(height: 35),
      ],
    );
  }

  Widget _buildTextWidget({String? title, TextStyle? textStyle, double? padding}) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      padding: EdgeInsets.symmetric(horizontal: padding ?? 47),
      child: Text(
        title ?? '',
        style: textStyle ?? AppTextStyle.nearBlackS18W600,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildButtonNext() {
    return GestureDetector(
      onTap: onNextTap,
      child: Container(
        width: 80,
        height: 80,
        margin: EdgeInsets.only(top: 45),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(90), color: AppColors.buttonViolet, boxShadow: AppShadow.violetShadow),
        child: Icon(
          Icons.arrow_forward_rounded,
          color: AppColors.background,
        ),
      ),
    );
  }

  Widget _buildSignWidget(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 45, bottom: 50),
          width: 254,
          height: 52,
          child: AppVioletButton(
            title: S.of(context).common_button_sign_up,
            onPressed: onSignInTap,
            cornerRadius: AppDimens.buttonCornerRadius50,
            isEnable: true,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              S.of(context).slide_not_a_member,
              style: AppTextStyle.greyS13W600,
            ),
            SizedBox(width: 5),
            GestureDetector(
              onTap: onSignUpTap,
              child: Container(
                //width: MediaQuery.of(context).size.width*.6,
                child: Text(
                  S.of(context).common_button_sign_in,
                  style: AppTextStyle.textVioletS13W600,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
