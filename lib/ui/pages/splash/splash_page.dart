import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

import 'splash_logic.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  Size? size;
  final SplashLogic logic = Get.put(SplashLogic());

  @override
  void initState() {
    super.initState();
    logic.checkLogin();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.purple,
      body: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Container(
            width: 70,
            height: 70,
            margin: EdgeInsets.only(top: 40),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.whiteColor, width: 1),
                borderRadius: BorderRadius.circular(5),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Colors.white,
                    Colors.white,
                  ],
                ),
                image: DecorationImage(image: AssetImage('assets/images/logo_hyge.png'))),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            "Quản gia toàn năng\nAn tâm giao phó",
            style: TextStyle(fontSize: 30, color: Colors.white, fontWeight: FontWeight.w900),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 60,
          ),
          Container(
            height: 300,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/splash.png'), fit: BoxFit.cover)),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }
}
