import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/databases/share_preferences_helper.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/services/api_service.dart';
import 'package:twinkle_colors_flutter_app/services/auth_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/home/tabs/home_tab/home_tab_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/sign_in_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/slide/slide_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';

import 'splash_state.dart';

class SplashLogic extends GetxController {
  final state = SplashState();
  final apiService = Get.find<ApiService>();
  final authService = Get.find<AuthService>();

  void checkLogin() async {
    await Future.delayed(Duration(seconds: 2));
    final result = await authService.init();
    final checkFirstOpenApp = await SharedPreferencesHelper.getFirstOpenApp();
    if (result.token.value == null) {
      // if (checkFirstOpenApp == "True" ||checkFirstOpenApp == "") {
      //   Get.offAll(SignInPage());
      // } else {
        Get.offAll(SignInPage());
    //  }
    } else {
      try {
        //Profile
        TokenEntity? myProfile = authService.token.value;
        authService.updateUser(myProfile ?? TokenEntity());
      } catch (error) {
        logger.log(error);
        //Check 401
        if (error is DioError) {
          if (error.response?.statusCode == 401) {
            authService.signOut();
            checkLogin();
            return;
          }
        }
        AppDialog.defaultDialog(
          message: "An error happened. Please check your connection!",
          textConfirm: "Retry",
          onConfirm: () {
            checkLogin();
          },
        );
        return;
      }
      Get.offAll(HomeTab());
    }
  }
}
