import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class InformationLifeStyleWidget extends StatelessWidget {
  final String? value;
  final String? icon;
  final VoidCallback? pickerTap;

  InformationLifeStyleWidget({
    this.value,
    this.icon,
    this.pickerTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: pickerTap,
      child: Container(
        width: 77,
        height: 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(60),
          color: AppColors.greyItemColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 10,
              height: 10,
              margin: EdgeInsets.only(right: 6),
              child: Image.asset(icon ?? AppImages.icAvatar),
            ),
            Text(
              value ?? '',
              style: AppTextStyle.blackS10W500,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
