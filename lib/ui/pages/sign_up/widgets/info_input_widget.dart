import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field_corner.dart';

class InfoInputWidget extends StatelessWidget {
  final TextEditingController? controller;
  final String? hintText;
  final String? errorText;
  final String? labelText;
  final TextInputType? inputType;
  final bool? isShowIcon;
  final Widget? trailingIcon;
  final bool? obsCureText;
  final ValueChanged<String>? onChanged;

  InfoInputWidget({
    this.controller,
    this.hintText,
    this.errorText,
    this.labelText,
    this.inputType,
    this.isShowIcon,
    this.trailingIcon,
    this.obsCureText,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppDimens.padding18),
      child: AppTextFieldCorner(
        inputController: controller,
        hintText: hintText,
        bgColor: AppColors.greyBgColor,
        textInputType: inputType,
        errorText: errorText,
        labelText: labelText,
        trailingIcon: trailingIcon ?? Container(),
        isShowBorder: true,
        obscureText: obsCureText,
        onChanged: onChanged,
      ),
    );
  }
}
