import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';

class SignUpState {
  /// Check box status
  final isCheckedPrivacePolicy = false.obs;

  /// TextEditingController
  late TextEditingController registerNameController = TextEditingController();
  late TextEditingController registerPhoneNumberController = TextEditingController();
  late TextEditingController registerPasswordController = TextEditingController();
  late TextEditingController registerCodeIntroduceController = TextEditingController();
  late TextEditingController registerSurnameController = TextEditingController();

  /// Show Icon status
  final showUsernameCheck = false.obs;
  final showEmailCheck = false.obs;
  final showPhoneNumberCheck = false.obs;
  final showIconObscureText = false.obs;
  final showSurnameCheck = false.obs;
  final obscureText = true.obs;

  /// Load api status
  final loadSignUpStatus = LoadStatus.initial.obs;
  final signInWithGoogleStatus = LoadStatus.initial.obs;
  final signInWithFacebookStatus = LoadStatus.initial.obs;

  /// Responses from server
  final signUpResponse = TokenEntity().obs;

  /// Error texts and status
  final usernameErrorText = ''.obs;
  final checkUsernameError = false.obs;
  final passwordErrorText = ''.obs;
  final checkPasswordError = false.obs;
  final emailErrorText = ''.obs;
  final checkEmailError = false.obs;
  final phoneNumberErrorText = ''.obs;
  final checkphoneNumberError = false.obs;
  final surnameErrorText = ''.obs;
  final checkSurnameError = false.obs;

  SignUpState() {
    registerNameController = TextEditingController(text: '');
    registerPhoneNumberController = TextEditingController(text: '');
    registerPasswordController = TextEditingController(text: '');
    registerCodeIntroduceController = TextEditingController(text: '');
    registerSurnameController = TextEditingController(text: '');
  }
}
