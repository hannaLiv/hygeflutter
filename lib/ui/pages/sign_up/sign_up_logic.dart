import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/services/api_service.dart';
import 'package:twinkle_colors_flutter_app/services/auth_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/otp/components/otp_form.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/otp/otp_screen.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_state.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/utils/authentication_firebase.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/utils/utils.dart';
import 'package:twinkle_colors_flutter_app/utils/validators.dart';

import 'sign_up_success/first_sign_up_started_page.dart';

class SignUpLogic extends GetxController {
  final state = SignUpState();
  final ApiService apiService = Get.find();
  final AuthService authService = Get.find();

  /// Sign up customer account
  void signUpCustomerAccount(BuildContext context) async {
    state.loadSignUpStatus.value = LoadStatus.loading;
    try {
      final result = await apiService.signUpWithCustomerAccount(
          null ?? '',
          state.registerNameController.text,
          state.registerPasswordController.text,
          state.registerPhoneNumberController.text,
          state.registerCodeIntroduceController.text,
          state.registerSurnameController.text);
      if (result != null) {
        if (Utils.checkCodeSignUp(result.code ?? '')) {
          AppDialog.errorDialog(
              message: result.message ?? '',
              textCancel: S.current.common_button_cancel);
          state.loadSignUpStatus.value = LoadStatus.initial;
        } else {
          state.loadSignUpStatus.value = LoadStatus.success;
          state.signUpResponse.value = result;
          authService.saveToken(result);
          authService.updateUser(result);
          Get.to(OtpScreen(
            userName: state.registerNameController.text,
            phoneNumber: state.registerPhoneNumberController.text,
            passWord: state.registerPasswordController.text,
          ));
        }
      } else {
        AppDialog.errorDialog(
          title: S.of(context).common_text_sign_up_failure,
          textCancel: S.of(context).common_button_cancel,
          message: S.of(context).common_text_error_au_account,
        );
        state.loadSignUpStatus.value = LoadStatus.failure;
      }
    } catch (e) {
      logger.e("SignUp With customer account: $e");
      state.loadSignUpStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        title: S.of(context).common_text_sign_up_failure,
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_account,
      );
    }
  }

  /// Sign in with google account
  void signInWithGoogleAccount({required BuildContext context}) async {
    state.signInWithGoogleStatus.value = LoadStatus.loading;
    try {
      String? token = await Authentication.signInWithGoogle(context: context);
      if (token == null || token == '') {
        state.signInWithGoogleStatus.value = LoadStatus.initial;
      } else {
        final result = await apiService.signInWithGoogle("google", token);
        if (result != null) {
          if (result.message == null || result.message == '') {
            authService.saveToken(result);
            authService.updateUser(result);
            state.signInWithGoogleStatus.value = LoadStatus.success;
            Get.offAll(MainPage());
          } else {
            AppDialog.errorDialog(
                message: result.message ?? '',
                textCancel: S.current.common_button_cancel);
            state.signInWithGoogleStatus.value = LoadStatus.initial;
          }
        }
      }
    } catch (e) {
      logger.e('Sign in with google : $e');
      state.signInWithGoogleStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_google,
      );
    }
  }

  /// Sign in with facebook account
  void signInWithFacebookAccount({required BuildContext context}) async {
    state.signInWithFacebookStatus.value = LoadStatus.loading;
    try {
      final token = await Authentication.loginWithFacebook();
      if (token == null || token == '') {
        state.signInWithFacebookStatus.value = LoadStatus.initial;
        return;
      }
      final result = await apiService.signInWithFacebook("facebook", token);
      if (result != null) {
        if (result.message == null || result.message == '') {
          authService.saveToken(result);
          authService.updateUser(result);
          state.signInWithFacebookStatus.value = LoadStatus.success;
          Get.offAll(MainPage());
        } else {
          AppDialog.errorDialog(
              message: result.message ?? '',
              textCancel: S.current.common_button_cancel);
          state.signInWithFacebookStatus.value = LoadStatus.initial;
        }
      }
      print('Token: $token');
    } catch (e) {
      logger.e('Sign in with facebook : $e');
      state.signInWithFacebookStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_facebook,
      );
    }
  }

  void checkInputFormat(BuildContext context) {
    final bool checkFormatUserName = checkUsername(context);
    //final bool checkFormatEmail = checkEmail(context);
    final bool checkFormatPassword = checkPassword(context);
    if (!checkFormatUserName) return;

    //if (!checkFormatEmail) return;
    if (!checkFormatPassword) return;

    signUpCustomerAccount(context);
  }

  /// Check format username
  bool checkUsername(BuildContext context) {
    final username = state.registerNameController.text;
    if (username == '' || username.isEmpty) {
      state.usernameErrorText.value = S.of(context).common_error_empty_username;
      state.checkUsernameError.value = true;
      state.showUsernameCheck.value = true;
      return false;
    } else {
      state.usernameErrorText.value = '';
      state.checkUsernameError.value = false;
      state.showUsernameCheck.value = true;
      return true;
    }
  }

  /// Check format phone number
  bool checkPhoneNumber(BuildContext context) {
    final phoneNumber = state.registerPhoneNumberController.text;
    if (phoneNumber == '' || phoneNumber.isEmpty) {
      state.phoneNumberErrorText.value = 'Enter your phone number! Try again.';
      state.checkphoneNumberError.value = true;
      state.showPhoneNumberCheck.value = true;
      return false;
    } else {
      state.phoneNumberErrorText.value = '';
      state.checkphoneNumberError.value = false;
      state.showPhoneNumberCheck.value = true;
      return true;
    }
  }

   /// Check format phone number
  bool checkSurname(BuildContext context) {
    final surname = state.registerSurnameController.text;
    if (surname == '' || surname.isEmpty) {
      state.surnameErrorText.value = 'Enter your phone number! Try again.';
      state.checkSurnameError.value = true;
      state.showSurnameCheck.value = true;
      return false;
    } else {
      state.surnameErrorText.value = '';
      state.checkSurnameError.value = false;
      state.showSurnameCheck.value = true;
      return true;
    }
  }

  /// Check format Code gift
  bool checkCodeGift(BuildContext context) {
    final code = state.registerCodeIntroduceController.text;
    return true;
  }

  /// Check format password
  bool checkPassword(BuildContext context) {
    final password = state.registerPasswordController.text;
    if (password == '' || password.isEmpty) {
      state.passwordErrorText.value = S.of(context).common_error_empty_password;
      state.checkPasswordError.value = true;
      state.showIconObscureText.value = false;
      return false;
    } else if (!Validators.validatePassword(password)) {
      state.passwordErrorText.value =
          S.of(context).common_error_format_password;
      state.checkPasswordError.value = true;
      state.showIconObscureText.value = true;
      return false;
    } else {
      state.passwordErrorText.value = '';
      state.checkPasswordError.value = false;
      state.showIconObscureText.value = true;
      return true;
    }
  }

  void changePrivacePolicyStatus({bool? status}) {
    state.isCheckedPrivacePolicy.value = status!;
  }

  void showObscureText(bool isShow) {
    state.obscureText.value = isShow;
  }
}
