import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_dimens.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/sign_in_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/widgets/login_facebook_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/widgets/login_google_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/otp/otp_screen.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_logic.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_state.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up_started_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/widgets/info_input_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/checkbox/app_checkbox.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/images/app_black_logo.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/loading_widget.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final SignUpLogic logic = Get.put(SignUpLogic());
  final SignUpState state = Get.find<SignUpLogic>().state;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    Get.delete<SignUpLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Obx(() {
          return Stack(
            children: [
              ListView(
                shrinkWrap: true,
                children: [
                  _buildImageLogo(),
                  LoginFacebookWidget(
                    onFacebookTap: () => logic.signInWithFacebookAccount(context: context),
                  ),
                  LoginGoogleWidget(
                    onGoogleTap: () => logic.signInWithGoogleAccount(context: context),
                  ),
                  _buildSuggestSignUpText(),
                  _buildUserNameWidget(),
                  SizedBox(height: 12),
                  _buildSurnameWidget(),
                  SizedBox(height: 12),
                  _buildPhoneNumberWidget(),
                  SizedBox(height: 12),
                  _buildPasswordWidget(),
                  SizedBox(height: 12),
                  _buildCodeIntroWidget(),
                  SizedBox(height: 12),
                  _buildPrivacyPolicy(),
                  _buildSignUpButton(),
                  Spacer(),
                  _buildSignInWidget(),
                ],
              ),
              _buildLoadingWidget(),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildImageLogo() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 50, bottom: 20),
          child: AppBlackLogo(),
        ),
        Text(
          S.of(context).sign_up_create_your_account,
          style: AppTextStyle.blackS22W600,
        ),
      ],
    );
  }

  Widget _buildSuggestSignUpText() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.padding15, horizontal: 20),
      child: Text(
        S.of(context).sign_up_with_email,
        style: AppTextStyle.greyS16W600,
      ),
    );
  }

  Widget _buildPrivacyPolicy() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppDimens.padding18),
      child: Row(
        children: [
          Obx(() {
            return AppCheckBox(
              value: state.isCheckedPrivacePolicy.value,
              onChanged: (bool? value) {
                if (state.isCheckedPrivacePolicy.value != value) {
                  logic.changePrivacePolicyStatus(status: value);
                }
              },
            );
          }),
          SizedBox(width: 10),
          Text(S.of(context).sign_up_have_read, style: AppTextStyle.greyS12W600),
          SizedBox(width: 5),
          GestureDetector(
            onTap: () => print('Privace Policy'),
            child: Text(S.of(context).sign_up_read_policy, style: AppTextStyle.textVioletS12W600),
          ),
        ],
      ),
    );
  }

  Widget _buildSignUpButton() {
    /// Check enable button Sign in
    final bool isEnableButton = state.registerNameController.text != '' && state.registerPhoneNumberController.text != '' && state.registerPhoneNumberController.text != '' && state.registerPasswordController.text != '' && state.isCheckedPrivacePolicy.value == true;

    /// Check loading button sign in
    final bool isLoading = state.loadSignUpStatus.value == LoadStatus.loading || state.signInWithGoogleStatus.value == LoadStatus.loading || state.signInWithFacebookStatus.value == LoadStatus.loading;
    return Column(
      children: [
        Container(
          width: 254,
          height: 54,
          margin: EdgeInsets.only(top: 18),
          child: AppVioletButton(
            title: S.of(context).common_button_sign_up,
            cornerRadius: AppDimens.buttonCornerRadius50,
            textStyle: AppTextStyle.whiteS16W600,
            isEnable: isEnableButton,
            //isEnable: true,
            isLoading: isLoading,
            onPressed: () {
              logic.signUpCustomerAccount(context);

              // logic.checkInputFormat(context);
              // logic.signUpCustomerAccount(context);
            },
          ),
        ),
        SizedBox(height: 15),
        GestureDetector(
          onTap: () => Get.to(FirstSignUpStartedPage()),
          child: Text(
            S.of(context).sign_in_forgot_password,
            style: AppTextStyle.nearBlackS13W600UnderLine,
          ),
        )
      ],
    );
  }

  Widget _buildLoadingWidget() {
    return Obx(() {
      if (state.loadSignUpStatus.value == LoadStatus.loading) {
        return LoadingIndicatorWidget();
      } else {
        return SizedBox();
      }
    });
  }

  void _onObscureIconTap() {
    if (state.obscureText.value != !state.obscureText.value) {
      logic.showObscureText(!state.obscureText.value);
    }
  }

  Widget _buildUserNameWidget() {
    return InfoInputWidget(
      controller: state.registerNameController,
      hintText: "Tài khoản",
      inputType: TextInputType.text,
      isShowIcon: true,
      labelText: "Tài khoản",
      errorText: state.checkUsernameError.value ? state.usernameErrorText.value : null,
      trailingIcon: state.showUsernameCheck.value
          ? state.showUsernameCheck.value && state.checkUsernameError.value
              ? Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.icClose),
                )
              : Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.iconCheck),
                )
          : SizedBox(),
      onChanged: (value) => logic.checkUsername(context),
    );
  }

  Widget _buildPasswordWidget() {
    return InfoInputWidget(
      controller: state.registerPasswordController,
      hintText: "Mật khẩu",
      inputType: TextInputType.text,
      isShowIcon: true,
      obsCureText: state.obscureText.value,
      labelText: "Mật khẩu",
      errorText: state.checkPasswordError.value ? state.passwordErrorText.value : null,
      trailingIcon: state.showIconObscureText.value
          ? state.showIconObscureText.value && state.checkPasswordError.value
              ? Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.icClose),
                )
              : state.obscureText.value
                  ? GestureDetector(
                      onTap: _onObscureIconTap,
                      child: Container(
                        width: 15,
                        height: 12,
                        child: Image.asset(AppImages.iconHidePass),
                      ),
                    )
                  : GestureDetector(
                      onTap: _onObscureIconTap,
                      child: Container(
                        width: 15,
                        height: 12,
                        child: Image.asset(AppImages.icShowPass),
                      ),
                    )
          : SizedBox(),
      onChanged: (value) => logic.checkPassword(context),
    );
  }

  Widget _buildPhoneNumberWidget() {
    return InfoInputWidget(
      controller: state.registerPhoneNumberController,
      hintText: /*S.of(context).sign_up_phone_number*/ 'Số điện thoại',
      inputType: TextInputType.phone,
      isShowIcon: true,
      labelText: /*S.of(context).sign_up_phone_number*/ 'Số điện thoại',
      errorText: state.checkphoneNumberError.value ? state.phoneNumberErrorText.value : null,
      trailingIcon: state.showPhoneNumberCheck.value
          ? state.showPhoneNumberCheck.value && state.checkphoneNumberError.value
              ? Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.icClose),
                )
              : Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.iconCheck),
                )
          : SizedBox(),
      onChanged: (value) => logic.checkPhoneNumber(context),
    );
  }

  Widget _buildCodeIntroWidget() {
    return InfoInputWidget(
      controller: state.registerCodeIntroduceController,
      hintText: /*S.of(context).sign_up_code*/ "Mã giới thiệu",
      inputType: TextInputType.text,
      isShowIcon: true,
      labelText: /* S.of(context).sign_up_code*/ "Mã giới thiệu",
      //errorText: state.checkUsernameError.value ? state.usernameErrorText.value : null,
      trailingIcon: SizedBox(),
      onChanged: (value) => logic.checkCodeGift(context),
    );
  }

  Widget _buildSurnameWidget() {
    return InfoInputWidget(
      controller: state.registerSurnameController,
      hintText: /*S.of(context).surname_title*/ "Tên thật",
      inputType: TextInputType.text,
      isShowIcon: true,
      labelText: /* S.of(context).surname_title*/ 'Tên thật',
      errorText: state.checkSurnameError.value ? state.surnameErrorText.value : null,
      trailingIcon: state.showSurnameCheck.value
          ? state.showSurnameCheck.value && state.checkSurnameError.value
              ? Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.icClose),
                )
              : Container(
                  width: 14,
                  height: 10,
                  child: Image.asset(AppImages.iconCheck),
                )
          : SizedBox(),
      onChanged: (value) => logic.checkSurname(context),
    );
  }

  Widget _buildSignInWidget() {
    return Container(
      margin: EdgeInsets.only(bottom: 35),
      padding: EdgeInsets.symmetric(horizontal: AppDimens.padding18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(S.of(context).sign_up_already_number, style: AppTextStyle.greyS12W600),
          SizedBox(width: 5),
          GestureDetector(
            onTap: () => Get.to(SignInPage()),
            child: Text(S.of(context).common_button_sign_in, style: AppTextStyle.textVioletS12W600),
          ),
        ],
      ),
    );
  }
}
