import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_dimens.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/services/api_service.dart';
import 'package:twinkle_colors_flutter_app/services/auth_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up_started_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/utils/constants.dart';
import 'package:twinkle_colors_flutter_app/utils/size_config.dart';
import 'package:twinkle_colors_flutter_app/utils/utils.dart';

class OtpForm extends StatefulWidget {
  final String? phoneNumber;
  final String? passWord;
  final String? userName;
  const OtpForm({Key? key, this.phoneNumber, this.passWord, this.userName}) : super(key: key);

  @override
  _OtpFormState createState() => _OtpFormState(phoneNumber: phoneNumber, passWord: passWord, userName: userName);
}

class _OtpFormState extends State<OtpForm> {
  final String? phoneNumber;
  final String? passWord;
  final String? userName;
  _OtpFormState({this.phoneNumber, this.passWord, this.userName});

  final ApiService apiService = Get.find();
  final AuthService authService = Get.find();
  FocusNode? pin2FocusNode;
  FocusNode? pin3FocusNode;
  FocusNode? pin4FocusNode;
  String pinCode = '';

  @override
  void initState() {
    super.initState();
    pin2FocusNode = FocusNode();
    pin3FocusNode = FocusNode();
    pin4FocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    pin2FocusNode!.dispose();
    pin3FocusNode!.dispose();
    pin4FocusNode!.dispose();
  }

  void nextField(String value, FocusNode? focusNode) {
    if (value.length == 1) {
      focusNode!.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: getProportionateScreenWidth(60),
                child: TextFormField(
                  autofocus: true,
                  obscureText: true,
                  style: TextStyle(fontSize: 24),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: otpInputDecoration,
                  onChanged: (value) {
                    nextField(value, pin2FocusNode);
                    pinCode = value.toString();
                  },
                ),
              ),
              SizedBox(
                width: getProportionateScreenWidth(60),
                child: TextFormField(
                  focusNode: pin2FocusNode,
                  obscureText: true,
                  style: TextStyle(fontSize: 24),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: otpInputDecoration,
                  onChanged: (value) {
                    nextField(value, pin3FocusNode);
                    pinCode += value.toString();
                  },
                ),
              ),
              SizedBox(
                width: getProportionateScreenWidth(60),
                child: TextFormField(
                  focusNode: pin3FocusNode,
                  obscureText: true,
                  style: TextStyle(fontSize: 24),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: otpInputDecoration,
                  onChanged: (value) {
                    nextField(value, pin4FocusNode);
                    pinCode += value.toString();
                  },
                ),
              ),
              SizedBox(
                width: getProportionateScreenWidth(60),
                child: TextFormField(
                  focusNode: pin4FocusNode,
                  obscureText: true,
                  style: TextStyle(fontSize: 24),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: otpInputDecoration,
                  onChanged: (value) {
                    if (value.length == 1) {
                      pin4FocusNode!.unfocus();
                      pinCode += value.toString();
                      // Then you need to check is the code is correct or not
                    }
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.screenHeight * 0.15),
          AppVioletButton(
            title: S.of(context).common_button_continue,
            cornerRadius: AppDimens.buttonCornerRadius50,
            textStyle: AppTextStyle.whiteS16W600,
            //isEnable: isEnableButton,
            isEnable: true,
            onPressed: () {
              if (pinCode != "" && phoneNumber != null) {
                _signUpByToken(context);
              }
            },
          ),
        ],
      ),
    );
  }

  void _signUpByToken(BuildContext context) async {
    try {
      final result = await apiService.checkOTPSignIn(phoneNumber!, pinCode);
      if (result != null) {
        if (Utils.checkCodeSignUp(result.code ?? '')) {
          AppDialog.errorDialog(message: result.message ?? '', textCancel: S.current.common_button_cancel);
        } else {
          signInWithCustomerAccount(context: context);
        }
      } else {
        Get.to(SignUpPage());
      }
    } catch (e) {}
  }

   void signInWithCustomerAccount({required BuildContext context}) async {

    try {
      final result = await apiService.signInWithCustomerAccount(userName!, passWord!);
      if (result != null) {
        /// Check code to show dialog when sign in failure
        final bool checkCodeLogin = Utils.checkCodeSignIn(result.code ?? '');
        if (checkCodeLogin) {
          AppDialog.errorDialog(message: result.message ?? '', textCancel: S.current.common_button_cancel);
        } else {
          Get.to(FirstSignUpStartedPage());
          // authService.saveToken(result);
          // authService.updateUser(result);
          // print('Token: ${authService.token.value}');
          // Get.offAll(MainPage());
        }
      }
    } catch (e) {
      AppDialog.errorDialog(
        title: S.of(context).common_text_sign_in_failure,
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_account,
      );
    }
  }
}
