import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/utils/size_config.dart';

import 'components/body.dart';

class OtpScreen extends StatelessWidget {
  final String? phoneNumber;
  final String? passWord;
  final String? userName;
  OtpScreen({
    this.phoneNumber,
    this.passWord,
    this.userName
  });
  static String routeName = "/otp";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("OTP Verification"),
      ),
      body: Body(phoneNumber: phoneNumber,passWord: passWord, userName: userName),
    );
  }
}
