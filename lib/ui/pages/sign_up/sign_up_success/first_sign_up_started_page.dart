import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_dimens.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/sign_in_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/appbar/app_bar_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/images/app_white_logo.dart';

import '../sign_up_logic.dart';
import '../sign_up_state.dart';
import 'first_sign_up/about_me_page.dart';

class FirstSignUpStartedPage extends StatefulWidget {
  const FirstSignUpStartedPage({Key? key}) : super(key: key);

  @override
  _FirstSignUpStartedPageState createState() => _FirstSignUpStartedPageState();
}

class _FirstSignUpStartedPageState extends State<FirstSignUpStartedPage> {
  final SignUpLogic logic = Get.put(SignUpLogic());
  final SignUpState state = Get.find<SignUpLogic>().state;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 3000), (){
        Get.off(SignInPage());
      });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          brightness: Brightness.dark,
          backgroundColor: AppColors.violetBackground,
        ),
        body: _buildBodyWidget());
  }

  Widget _buildBodyWidget() {
    return Container(
      decoration: BoxDecoration(color: AppColors.violetBackground),
      child: Column(
        children: [
          _buildImageLogo(),
          _buildWelcomeTitle(),
          SizedBox(height: 20),
          _buildSubtitle(),
          SizedBox(height: 50),
          //_buildStartButton(),
        ],
      ),
    );
  }

  Widget _buildImageLogo() {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          AppWhiteLogo(),
          Container(
            margin: EdgeInsets.symmetric(vertical: 50),
            child: Image.asset(AppImages.imgSignUpSuccess),
          ),
        ],
      ),
    );
  }

  Widget _buildWelcomeTitle() {
    return Text(
      "${S.of(context).sign_up_hi_friend}\n${S.of(context).sign_up_we_get_to} \n ${S.of(context).sign_up_know_you}.",
      style: AppTextStyle.whiteS25W600,
      textAlign: TextAlign.center,
    );
  }

  Widget _buildSubtitle() {
    return Text(
      S.of(context).sign_up_take_a_minutes,
      style: AppTextStyle.whiteS14W500,
      textAlign: TextAlign.center,
    );
  }

  Widget _buildStartButton() {
    return Column(
      children: [
        Container(
          width: 254,
          height: 52,
          child: AppRedButton(
            title: S.of(context).sign_up_let_started,
            cornerRadius: AppDimens.buttonCornerRadius50,
            textStyle: AppTextStyle.blackS16W600,
            backgroundColor: AppColors.textWhite,
            onPressed: () {
              Get.to(AboutMePage());
            },
          ),
        ),
        SizedBox(height: 40),
      ],
    );
  }
}
