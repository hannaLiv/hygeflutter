import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';

typedef OnLifeStyleCallBack(String name);

class LifeStyleItemCorner extends StatelessWidget {
  final UserInformationEntity? data;
  final String? textItemSelected;
  final OnLifeStyleCallBack? onTap;

  LifeStyleItemCorner({
    this.data,
    this.textItemSelected,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final checkSelected = textItemSelected != null && textItemSelected != ''
        ? textItemSelected == data?.name
            ? true
            : false
        : false;
    return GestureDetector(
      onTap: () => onTap?.call(data?.name ?? ''),
      child: Container(
        width: 254,
        height: 52,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(AppDimens.buttonCornerRadius50),
          boxShadow: AppShadow.lifeStyleItemShadow,
          color: checkSelected ? AppColors.buttonViolet : AppColors.background,
        ),
        child: Center(
          child: Text(
            data?.name ?? "",
            style: checkSelected ? AppTextStyle.whiteS16W600 : AppTextStyle.greyS16W600,
          ),
        ),
      ),
    );
  }
}
