import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/checkbox/app_checkbox.dart';

typedef OnLifeStyleCheckCallBack(UserInformationEntity entity);

class LifeStyleCheckItem extends StatelessWidget {
  final UserInformationEntity? data;
  final List<int>? itemChecked;
  final ValueChanged<bool?>? onChanged;
  final OnLifeStyleCheckCallBack? itemTap;
  final int? indexSelected;

  LifeStyleCheckItem({
    this.data,
    this.itemChecked,
    this.onChanged,
    this.itemTap,
    this.indexSelected,
  });

  @override
  Widget build(BuildContext context) {
    bool _itemChecked = false;
    try {
      final result = itemChecked?.firstWhere((element) => element == indexSelected);
      result != null ? _itemChecked = true : _itemChecked = false;
    } catch (e) {}
    return Container(
      margin: EdgeInsets.only(bottom: 32),
      child: Row(
        children: [
          Container(
            height: 23,
            width: 23,
            margin: EdgeInsets.only(right: 12),
            child: Image.asset(AppImages.icAvatar),
          ),
          Text(
            data?.name ?? "",
            style: AppTextStyle.nearBlackS15W400,
          ),
          Spacer(),
          AppCheckBox(
            onChanged: (value) {
              onChanged?.call(value);
              itemTap?.call(data ?? UserInformationEntity());
            },
            value: _itemChecked,
          ),
        ],
      ),
    );
  }
}
