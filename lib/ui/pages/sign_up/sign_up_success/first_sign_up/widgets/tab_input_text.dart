import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/infomation_header_title_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field_corner.dart';

class TabInputText extends StatelessWidget {
  final String title;
  final String subTitle;
  final String placeHolderText;
  final TextEditingController? inputController;
  final ValueChanged<String>? onChanged;
  final Widget? trailingIcon;
  final bool isEnableInput;

  TabInputText({
    this.title = '',
    this.subTitle = '',
    this.placeHolderText = '',
    this.inputController,
    this.onChanged,
    this.trailingIcon,
    this.isEnableInput = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: AppDimens.padding18),
        child: Column(
          children: [
            InfoHeaderTitleWidget(
              title: title,
              sub: subTitle,
            ),
            SizedBox(height: 30),
            AppTextFieldCorner(
              inputController: inputController,
              hintStyle: AppTextStyle.greyS14W400,
              bgColor: AppColors.greyBgColor,
              textInputType: TextInputType.text,
              onChanged: (value) => onChanged?.call(value),
              trailingIcon: trailingIcon ?? SizedBox(),
              labelText: placeHolderText,
              isEnable: isEnableInput,
            ),
          ],
        ));
  }
}
