import 'package:flutter/cupertino.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';

class InfoHeaderTitleWidget extends StatelessWidget {
  final String? title;
  final String? sub;

  InfoHeaderTitleWidget({this.title, this.sub});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title ?? '', style: AppTextStyle.textVioletS16W600),
          SizedBox(height: 5),
          Text(sub ?? '', style: AppTextStyle.blackS22W600),
        ],
      ),
    );
  }
}
