import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class CustomIndicatorPage extends StatelessWidget {
  final int pageIndex;

  CustomIndicatorPage({required this.pageIndex});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        AnimatedContainer(
          duration: const Duration(milliseconds: 2000),
          curve: Curves.fastOutSlowIn,
          margin: EdgeInsets.only(bottom: 30),
          width: MediaQuery.of(context).size.width / 7 * pageIndex,
          height: 4,
          color: AppColors.borderBlack,
        ),
      ],
    );
  }
}
