import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';

class BottomWidgets extends StatelessWidget {
  final bool isHiddenButton;
  final String? buttonTitle;
  final VoidCallback? onButtonTap;
  final VoidCallback? onAvoidStepTap;

  BottomWidgets({
    this.isHiddenButton = false,
    this.buttonTitle,
    this.onButtonTap,
    this.onAvoidStepTap,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 254,
          height: 54,
          child: isHiddenButton
              ? SizedBox()
              : AppVioletButton(
                  title: buttonTitle ?? S.of(context).common_button_continue,
                  cornerRadius: AppDimens.buttonCornerRadius50,
                  textStyle: AppTextStyle.whiteS16W600,
                  onPressed: onButtonTap,
                  isEnable: true,
                ),
        ),
        SizedBox(height: 15),
        GestureDetector(
          onTap: onAvoidStepTap,
          child: Text(
            S.of(context).sign_up_rather_not_say,
            style: AppTextStyle.textVioletS14W600,
          ),
        ),
        SizedBox(height: 35),
      ],
    );
  }
}
