import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/services/api_service.dart';
import 'package:twinkle_colors_flutter_app/services/user_api_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/utils/date_utils.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';
import 'about_me_state.dart';

class AboutMeLogic extends GetxController {
  final state = AboutMeState();
  final UserApiService userService = Get.find();
  final ApiService apiService = Get.find();

  /// Get user informations for sign up
  void getUserInformation() async {
    state.loadUserInfoStatus.value = LoadStatus.loading;
    try {
      final result = await userService.getUserInformationForSignUp();
      state.userInformationResponse.value = result;
      state.loadUserInfoStatus.value = LoadStatus.success;
    } catch (e) {
      state.loadUserInfoStatus.value = LoadStatus.failure;
      logger.e("sign up get info: $e");
    }
  }

  /// Update user information
  void updateUserInformation() async {
    state.loadUpdateUserStatus.value = LoadStatus.loading;

    try {
      /// Await to map data
      final lsLifeStylesId = await getIdRequestServer();
      final result = await apiService.updateUserInformationById({
        "currentJob": state.jobController.text,
        "lastEducation": state.educationSelected.value,
        "city": state.livingInController.text,
        "birthDate": state.birthdayController.text,
        "height": state.heightController.text,
        "weight": state.weightController.text,
        "shoppingPreferenceIds": lsLifeStylesId,
      });
      state.updateUserResponse.value = result;
      state.loadUpdateUserStatus.value = LoadStatus.success;
      Get.to(MainPage());
    } catch (e) {
      state.loadUpdateUserStatus.value = LoadStatus.failure;
      logger.e("sign up update user: $e");
    }
  }

  /// Get id of lifestyles from item selected
  Future<List> getIdRequestServer() async {
    final List<int?> lifeStyles = [];
    UserInformationEntity? getIdLifeStage = UserInformationEntity();
    UserInformationEntity? getIdStyles = UserInformationEntity();
    final data = state.userInformationResponse.value;
    try {
      getIdLifeStage = data.lifeStage?.firstWhere((element) => element.name == state.lifeStageSelected.value);
      state.shopCheckedIndex.forEach((element) {
        lifeStyles.add(data.yourFavoriesShopStores?[element].id);
      });
      state.fragrancesSelectedIndex.forEach((element) {
        lifeStyles.add(data.favoriesFragrances?[element].id);
      });
      state.designersSelectedIndex.forEach((element) {
        lifeStyles.add(data.yourFavoriesDesigners?[element].id);
      });
      getIdStyles = data.yourStyle?.firstWhere((element) => element.name == state.stylesSelected.value);
      state.fragrancesSelectedIndex.forEach((element) {
        lifeStyles.add(data.favoriesFragrances?[element].id);
      });
    } catch (e) {}
    if (getIdLifeStage?.id != null) {
      lifeStyles.add(getIdLifeStage?.id);
    }
    if (getIdStyles?.id != null) {
      lifeStyles.add(getIdStyles?.id);
    }

    return lifeStyles;
  }

  /// Brand check box
  void checkedBrand(List<int> index) {
    state.brandCheckedIndex.value = index;
  }

  /// Brand check box
  void checkedShop(List<int> index) {
    state.shopCheckedIndex.value = index;
  }

  /// Designers selected
  void designersSelected(int index) {
    if (state.designersSelectedIndex.length > 0) {
      final checkExistIndex = state.designersSelectedIndex.contains(index);
      if (checkExistIndex) {
        state.designersSelectedIndex.remove(index);
      } else {
        state.designersSelectedIndex.add(index);
      }
    } else {
      state.designersSelectedIndex.add(index);
    }
  }

  /// Fragrances selected
  void selectedFragrances(int index) {
    if (state.fragrancesSelectedIndex.length > 0) {
      final checkExistIndex = state.fragrancesSelectedIndex.contains(index);
      if (checkExistIndex) {
        state.fragrancesSelectedIndex.remove(index);
      } else {
        state.fragrancesSelectedIndex.add(index);
      }
    } else {
      state.fragrancesSelectedIndex.add(index);
    }
  }

  /// Change page index
  void changePageIndex({required int pageIndex}) {
    state.pageIndex.value = pageIndex;
    state.pageViewController.jumpToPage(state.pageIndex.value);
  }

  /// Check enable button
  void checkEnableButton(bool isEnable) {
    state.isEnable.value = isEnable;
  }

  /// Education Level selected
  void educationLevelSelected(String value) {
    state.educationSelected.value = value;
  }

  /// Change Life Stage
  void changeLifeStageSelect(String value) {
    state.lifeStageSelected.value = value;
  }

  /// Change Life Stage
  void changeStylesSelect(String value) {
    state.stylesSelected.value = value;
  }

  /// Birth Date picker
  void birthDatePicker(DateTime dateSelected) {
    final parseDate = AppDateUtils.toDateString(dateSelected, format: AppConfigs.dateFormatYdM);
    state.birthdayController.text = parseDate;
  }
}
