import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/infomation_header_title_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/lifestyle_item_corner.dart';

import '../about_me_logic.dart';

class LifeStyleYourStyles extends StatelessWidget {
  const LifeStyleYourStyles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: AppDimens.padding18),
      child: Column(
        children: [
          InfoHeaderTitleWidget(
            title: S.of(context).sign_up_lifestyle,
            sub: "${S.of(context).sign_up_choose_your} \n ${S.of(context).sign_up_life_stage}",
          ),
          SizedBox(height: 30),

          /// List life stage
          _buildStylesWidget(),
        ],
      ),
    );
  }
  Widget _buildStylesWidget() {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;
    final widgets = state.userInformationResponse.value.yourStyle
        ?.map((e) => Obx(() {
      return LifeStyleItemCorner(
        data: e,
        textItemSelected: state.stylesSelected.value,
        onTap: (value) {
          logic.changeStylesSelect(value);
          logic.changePageIndex(pageIndex: state.pageIndex.value + 1);
        },
      );
    }))
        .toList() ?? [] as List<Widget>;
    return Column(
      children: widgets,
    );
  }
}
