import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/tab_input_text.dart';
import 'package:twinkle_colors_flutter_app/utils/date_utils.dart';
import 'package:twinkle_colors_flutter_app/utils/picker_utils.dart';

import '../about_me_logic.dart';

class MyBirthDayWidget extends StatelessWidget {
  const MyBirthDayWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;

    return TabInputText(
      title: S.of(context).sign_up_my_birthday,
      subTitle: '${S.of(context).sign_up_question_what_is_your}\n${S.of(context).sign_up_birthday}',
      placeHolderText: S.of(context).sign_up_enter_your_birthdate,
      inputController: state.birthdayController,
      onChanged: (value) => logic.checkEnableButton(state.birthdayController.text != ''),
      trailingIcon: GestureDetector(
        onTap: () => PickerUtils.showDatePicker(
          context: context,
          onChanged: (dateSelected) => logic.birthDatePicker(dateSelected),
          currentTime: AppDateUtils.fromString(state.birthdayController.text, format: AppConfigs.dateFormatYdM),
        ),
        child: Container(
          width: 14,
          height: 14,
          child: Image.asset(AppImages.icDate,color: AppColors.textBlack343434,),
        ),
      ),
    );
  }
}
