import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/infomation_header_title_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/gridview/app_gridview.dart';

import '../about_me_logic.dart';

typedef OnDesignersSelected(int index);

class LifeStyleFavoriteDesigners extends StatelessWidget {
  const LifeStyleFavoriteDesigners({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
          gradient: AppColors.gradientBackground,
        ),
        margin: EdgeInsets.symmetric(horizontal: AppDimens.paddingNormal),
        child: Column(
          children: [
            InfoHeaderTitleWidget(
              title: S.of(context).sign_up_lifestyle,
              sub: "${S.of(context).sign_up_choose_your}\n${S.of(context).sign_up_favorite_designers}",
            ),
            SizedBox(height: 30),
            Container(
              height: MediaQuery.of(context).size.height,
              child: _buildDesignersWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDesignersWidget() {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;
    return AppGridview(
        crossAxisCount: 2,
        crossAxisSpacing: 0,
        mainAxisSpacing: 0,
        childAspectRatio: 2.5,
        items: state.userInformationResponse.value.yourFavoriesDesigners ?? [],
        itemBuilder: (context, index) {
          final item = state.userInformationResponse.value.yourFavoriesDesigners?[index] ?? UserInformationEntity();
          return Obx(() {
            return designerItem(
              data: item,
              index: index,
              onTap: (indexSelected) {
                logic.designersSelected(indexSelected);
              },
              lsItemSelected: state.designersSelectedIndex,
            );
          });
        });
  }

  Widget designerItem({
    required UserInformationEntity data,
    List<int>? lsItemSelected,
    int? index,
    OnDesignersSelected? onTap,
  }) {
    bool isSelected = false;
    if (lsItemSelected != []) {
      for (int i = 0; i < lsItemSelected!.length; i++) {
        if (lsItemSelected[i] == index) {
          isSelected = true;
        }
      }
    }
    return GestureDetector(
      onTap: () => onTap?.call(index ?? 0),
      child: Container(
        height: 45,
        margin: EdgeInsets.only(right: 10, bottom: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          boxShadow: AppShadow.whiteShadow,
          color: isSelected ? AppColors.textBlack343434 : AppColors.background,
        ),
        child: Center(
          child: Text(
            data.name ?? '',
            style: isSelected ? AppTextStyle.whiteS14W500 : AppTextStyle.greyS14W500,
          ),
        ),
      ),
    );
  }
}
