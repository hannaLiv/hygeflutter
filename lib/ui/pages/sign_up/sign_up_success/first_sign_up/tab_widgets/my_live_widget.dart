import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/tab_input_text.dart';

import '../about_me_logic.dart';

class MyLiveWidget extends StatelessWidget {
  const MyLiveWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;

    return TabInputText(
      title: S.of(context).sign_up_living_in,
      subTitle: '${S.of(context).sign_up_where_are_you}\n${S.of(context).sign_up_living_in}',
      placeHolderText: S.of(context).sign_up_enter_your_city,
      inputController: state.livingInController,
      onChanged: (value) => logic.checkEnableButton(state.livingInController.text != ''),
      trailingIcon: Container(
        margin: EdgeInsets.only(right: 15),
        width: 8,
        height: 14,
        child: Image.asset(AppImages.icDropDown),
      ),
    );
  }
}
