import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/models/entities/user_information_entity.dart';

import '../about_me_logic.dart';
import '../widgets/infomation_header_title_widget.dart';

typedef OnFragrancesCallBack(int index);

class LifeStyleFavoriteFragrances extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: AppDimens.paddingNormal),
        child: Column(
          children: [
            InfoHeaderTitleWidget(
              title: S.of(context).sign_up_lifestyle,
              sub: '${S.of(context).sign_up_lifestyle}\n ${S.of(context).sign_up_favorite_fragrances}',
            ),
            Obx(() {
              final items = state.userInformationResponse.value.favoriesFragrances ?? [];
              return Container(
                width: double.infinity,
                child: Wrap(
                  spacing: 8.0,
                  runSpacing: 8.0,
                  children: List<Widget>.generate(
                      items.length,
                      (index) => _buildChip(
                          data: items[index],
                          index: index,
                          lsItemSelected: state.fragrancesSelectedIndex,
                          onItemTap: (backIndex) {
                            logic.selectedFragrances(backIndex);
                          })).toList(),
                ),
              );
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildChip({
    required UserInformationEntity data,
    List<int>? lsItemSelected,
    int? index,
    OnFragrancesCallBack? onItemTap,
  }) {
    bool isSelected = false;
    if (lsItemSelected != []) {
      for (int i = 0; i < lsItemSelected!.length; i++) {
        if (lsItemSelected[i] == index) {
          isSelected = true;
        }
      }
    }
    return Container(
        margin: EdgeInsets.only(top: 30),
        child: GestureDetector(
          onTap: () => onItemTap?.call(index ?? 0),
          child: Chip(
            labelPadding: EdgeInsets.only(top: 10, bottom: 10, right: 13),
            label: Text(
              data.name ?? '',
              textAlign: TextAlign.center,
              style: isSelected ? AppTextStyle.whiteS14W500 : AppTextStyle.greyS14W500,
            ),
            backgroundColor: isSelected ? AppColors.borderBlack : AppColors.background,
            elevation: 0,
            shadowColor: Color.fromRGBO(160, 179, 203, 0.1),
            avatar: isSelected
                ? Container(
                    width: 12,
                    height: 12,
                    child: Image.asset(
                      AppImages.iconCheck,
                      color: AppColors.background,
                    ),
                  )
                : Container(
                    width: 12,
                    height: 12,
                    child: Image.asset(
                      AppImages.icAvatar,
                      color: AppColors.textGrey9099B0,
                    ),
                  ),
          ),
        ));
  }
}
