import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/infomation_header_title_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/life_style_check_item.dart';

import '../about_me_logic.dart';

class LifeStyleFavoriteShops extends StatelessWidget {
  const LifeStyleFavoriteShops({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(left: AppDimens.padding18, right: AppDimens.padding18, bottom: 40),
        child: Column(
          children: [
            InfoHeaderTitleWidget(
              title: S.of(context).sign_up_lifestyle,
              sub: "${S.of(context).sign_up_choose_your}\n${S.of(context).sign_up_favorite_shops}",
            ),
            SizedBox(height: 30),
            _buildListShops(),
          ],
        ),
      ),
    );
  }
}

Widget _buildListShops() {
  final logic = Get.find<AboutMeLogic>();
  final state = Get.find<AboutMeLogic>().state;
  List<int> lsIndex = [];
  final widget = state.userInformationResponse.value.yourFavoriesShopStores
          ?.map((e) => LifeStyleCheckItem(
                data: e,
                itemTap: (value) {
                  final index = state.userInformationResponse.value.yourFavoriesShopStores?.indexOf(value) ?? 0;
                  lsIndex.add(index);
                  logic.checkedShop(lsIndex);
                },
                itemChecked: state.shopCheckedIndex,
                indexSelected: state.shopCurrentIndex.value,
              ))
          .toList() ??
      [] as List<Widget>;
  return Column(
    children: widget,
  );
}
