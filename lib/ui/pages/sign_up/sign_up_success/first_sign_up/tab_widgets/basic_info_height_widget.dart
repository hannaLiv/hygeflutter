import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/tab_input_text.dart';

import '../about_me_logic.dart';

class BasicInfoHeightWidget extends StatelessWidget {
  const BasicInfoHeightWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;
    return TabInputText(
      title: S.of(context).sign_up_basic_information,
      subTitle: '${S.of(context).sign_up_question_what_is_your}\n${S.of(context).sign_up_height}',
      placeHolderText: S.of(context).sign_up_enter_your_content,
      inputController: state.heightController,
      onChanged: (value) => logic.checkEnableButton(state.heightController.text != ''),
    );
  }
}
