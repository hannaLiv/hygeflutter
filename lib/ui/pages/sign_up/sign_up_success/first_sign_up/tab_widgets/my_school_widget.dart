import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/lifestyle_item_corner.dart';

import '../about_me_logic.dart';
import '../widgets/infomation_header_title_widget.dart';

typedef OnItemTap(String title);

class MySchoolWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: AppDimens.paddingNormal),
        child: Column(
          children: [
            InfoHeaderTitleWidget(
              title: S.of(context).sign_up_my_school,
              sub: "${S.of(context).sign_up_add_your}\n${S.of(context).sign_up_education_level}",
            ),
            SizedBox(height: 30),
            _buildEducationLevelItems(),
          ],
        ),
      ),
    );
  }

  Widget _buildEducationLevelItems() {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;
    final widget = state.userInformationResponse.value.educationLevels
            ?.map(
              (e) => Obx(() {
                return LifeStyleItemCorner(
                  data: e,
                  onTap: (value) {
                    logic.educationLevelSelected(value);
                    logic.changePageIndex(pageIndex: state.pageIndex.value + 1);
                  },
                  textItemSelected: state.educationSelected.value,
                );
              }),
            )
            .toList() ??
        [] as List<Widget>;
    return Column(
      children: widget,
    );
  }

  Widget educationItem({
    String? title,
    OnItemTap? onItemTap,
    String? selectedValue,
  }) {
    final checkSelectedItem = selectedValue != null && selectedValue != ''
        ? selectedValue == title
            ? true
            : false
        : false;
    return GestureDetector(
      onTap: () => onItemTap?.call(title ?? ''),
      child: Container(
        width: 254,
        height: 54,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: checkSelectedItem ? AppColors.buttonViolet : AppColors.background,
          boxShadow: !checkSelectedItem ? AppShadow.whiteShadow : null,
        ),
        child: Center(
          child: Text(
            title ?? '',
            style: checkSelectedItem ? AppTextStyle.whiteS16W600 : AppTextStyle.greyS16W600,
          ),
        ),
      ),
    );
  }
}
