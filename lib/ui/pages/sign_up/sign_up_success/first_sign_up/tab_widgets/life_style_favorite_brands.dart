import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/widgets/life_style_check_item.dart';
import '../about_me_logic.dart';
import '../widgets/infomation_header_title_widget.dart';

class Brand {
  String img;
  String title;
  bool choose;

  Brand({this.img = '', this.title = '', this.choose = false});
}

typedef OnBrandCallBack(Brand brand);

class LifeStyleFavoriteBrands extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(left: AppDimens.padding18, right: AppDimens.padding18, bottom: 40),
        child: Column(
          children: [
            InfoHeaderTitleWidget(
              title: S.of(context).sign_up_lifestyle,
              sub: "${S.of(context).sign_up_choose_your}\n${S.of(context).sign_up_favorite_brand}",
            ),
            SizedBox(height: 30),
            _buildListBrand(),
          ],
        ),
      ),
    );
  }

  Widget _buildListBrand() {
    final logic = Get.find<AboutMeLogic>();
    final state = Get.find<AboutMeLogic>().state;
    List<int> lsIndex = [];
    final widget = state.userInformationResponse.value.favoriesBrands
            ?.map((e) => LifeStyleCheckItem(
                  data: e,
                  itemTap: (value) {
                    final index = state.userInformationResponse.value.favoriesBrands?.indexOf(value) ?? 0;
                    lsIndex.add(index);
                    logic.checkedBrand(lsIndex);
                  },
                  itemChecked: state.brandCheckedIndex,
                  indexSelected: state.brandCurrentIndex.value,
                ))
            .toList() ??
        [] as List<Widget>;
    return Column(
      children: widget,
    );
  }
}
