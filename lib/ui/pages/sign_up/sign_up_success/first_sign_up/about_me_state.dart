import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_information_response.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/tab_widgets/life_style_favorite_shops.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/tab_widgets/my_birthday_widget.dart';
import 'tab_widgets/basic_info_height_widget.dart';
import 'tab_widgets/basic_info_weight_widget.dart';
import 'tab_widgets/life_style_favorite_brands.dart';
import 'tab_widgets/life_style_favorite_designers.dart';
import 'tab_widgets/life_style_stage_widget.dart';
import 'tab_widgets/life_style_your_styles.dart';
import 'tab_widgets/my_live_widget.dart';
import 'tab_widgets/my_school_widget.dart';
import 'tab_widgets/life_style_favorite_fragrances.dart';
import 'tab_widgets/my_work_widget.dart';

class AboutMeState {
  /// TextEditing Controller
  late TextEditingController jobController = TextEditingController();
  late TextEditingController livingInController = TextEditingController();
  late TextEditingController birthdayController = TextEditingController();
  late TextEditingController heightController = TextEditingController();
  late TextEditingController weightController = TextEditingController();

  /// Brand Checked
  final RxList<int> brandCheckedIndex = <int>[].obs;
  final brandCurrentIndex = 0.obs;

  /// Shops Checked
  final RxList<int> shopCheckedIndex = <int>[].obs;
  final shopCurrentIndex = 0.obs;

  /// Designers Selected
  final RxList<int> designersSelectedIndex = <int>[].obs;

  /// Education Level Selected
  final educationSelected = ''.obs;

  /// Favorite Fragrances selected
  final RxList<int> fragrancesSelectedIndex = <int>[].obs;

  /// LifeStyle Stage
  final lifeStageSelected = "".obs;

  /// LifeStyle styles
  final stylesSelected = "".obs;

  /// Page view
  final pageViewController = PageController();
  final pageIndex = 0.obs;

  /// Enable Button
  final isEnable = false.obs;

  /// List Page view
  late List<Widget> pageList;

  /// Load api status
  final loadUserInfoStatus = LoadStatus.initial.obs;
  final loadUpdateUserStatus = LoadStatus.initial.obs;

  /// Responses from server
  final userInformationResponse = UserInformationResponse().obs;
  final updateUserResponse = false.obs;

  /// Initial data
  AboutMeState() {
    pageList = [
      //    MyWorkWidget(),
      //   MySchoolWidget(),
      MyBirthDayWidget(),
      MyLiveWidget(),
      //  BasicInfoHeightWidget(),
      //  BasicInfoWeightWidget(),
      LifeStyleStageWidget(),
      LifeStyleFavoriteShops(),
      LifeStyleFavoriteDesigners(),
      LifeStyleYourStyles(),
      LifeStyleFavoriteBrands(),
      //LifeStyleFavoriteFragrances(),
    ];
    birthdayController = TextEditingController(text: '');
  }
}
