import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_success/first_sign_up/about_me_state.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/appbar/app_bar_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/loading_widget.dart';
import 'about_me_logic.dart';
import 'widgets/bottom_widgets.dart';
import 'widgets/custom_indicator_page.dart';

class AboutMePage extends StatefulWidget {
  const AboutMePage({Key? key}) : super(key: key);

  @override
  _AboutMePageState createState() => _AboutMePageState();
}

class _AboutMePageState extends State<AboutMePage> {
  final logic = Get.put(AboutMeLogic());

  AboutMeState get state => logic.state;

  @override
  void initState() {
    super.initState();

    /// Fetch initial data
    logic.getUserInformation();
  }

  @override
  void dispose() {
    Get.delete<AboutMeLogic>();
    state.pageViewController.dispose();
    state.jobController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          brightness: Brightness.light,
          automaticallyImplyLeading: false,
          toolbarHeight: 20,
        ),
        body: _buildBodyWidget());
  }

  Widget _buildBodyWidget() {
    return Obx(() {
      return Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: AppColors.gradientBackground,
            ),
            child: Column(
              children: [
                CustomIndicatorPage(
                  pageIndex: state.pageIndex.value + 1,
                ),
                _buildBody(),
                _buildBottomControl()
              ],
            ),
          ),
          _buildLoadingWidget(),
        ],
      );
    });
  }

  Widget _buildBody() {
    return Expanded(
        child: PageView(
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      controller: state.pageViewController,
      onPageChanged: (pageIndex) => logic.changePageIndex(pageIndex: pageIndex),
      children: state.pageList,
    ));
  }

  /// Check show bottom map with page index
  Widget _buildBottomControl() {
    switch (state.pageIndex.value) {
      case 0:
        return BottomWidgets(
          onButtonTap: onContinueTap,
          onAvoidStepTap: onAVoidButtonTap,
        );
      case 1:
        return BottomWidgets(
          onButtonTap: onContinueTap,
          onAvoidStepTap: onAVoidButtonTap,
        );
      case 2:
        return BottomWidgets(
          isHiddenButton: true,
          onAvoidStepTap: onAVoidButtonTap,
        );
      case 3:
        return BottomWidgets(
          onButtonTap: onContinueTap,
          onAvoidStepTap: onAVoidButtonTap,
        );
      case 4:
        return BottomWidgets(
          onButtonTap: onContinueTap,
          onAvoidStepTap: onAVoidButtonTap,
        );
      case 5:
        return BottomWidgets(
          isHiddenButton: true,
          onAvoidStepTap: onAVoidButtonTap,
        );
      case 6:
        return BottomWidgets(
          onAvoidStepTap: onAVoidButtonTap,
          onButtonTap: onAVoidButtonTap,
        );
      // case 7:
      //   return BottomWidgets(
      //     isEnableButton: true,
      //     onButtonTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //     onAvoidStepTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //   );
      // case 8:
      //   return BottomWidgets(
      //     isEnableButton: true,
      //     onButtonTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //     onAvoidStepTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //   );
      // case 9:
      //   return BottomWidgets(
      //     isEnableButton: true,
      //     onButtonTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //     onAvoidStepTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //   );
      // case 10:
      //   return BottomWidgets(
      //     isHiddenButton: true,
      //     onAvoidStepTap: () => logic.changePageIndex(pageIndex: state.pageIndex.value + 1),
      //   );
      // case 11:
      //   return BottomWidgets(
      //     buttonTitle: S.of(context).common_button_you_done,
      //     isEnableButton: true,
      //     onAvoidStepTap: () => logic.updateUserInformation(),
      //     onButtonTap: () => logic.updateUserInformation(),
      //   );
      default:
        return Container();
    }
  }

  /// I'd rather not say tap
  void onAVoidButtonTap() {
    logic.updateUserInformation();
  }

  /// All Button Continue tap avoid final position
  void onContinueTap() {
    logic.changePageIndex(pageIndex: state.pageIndex.value + 1);
  }

  Widget _buildLoadingWidget() {
    return Obx(() {
      final loadUserInfoStatus = state.loadUserInfoStatus.value == LoadStatus.loading;
      final loadUpdateUserStatus = state.loadUpdateUserStatus.value == LoadStatus.loading;

      if (loadUserInfoStatus || loadUpdateUserStatus) {
        return LoadingIndicatorWidget();
      } else {
        return SizedBox();
      }
    });
  }
}
