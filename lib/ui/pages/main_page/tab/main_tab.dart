import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';

enum MainTab {
  home,
  history,
  booking,
  notification,
  account
}

extension MainTabExtension on MainTab {
  Widget get page {
    switch (this) {
      case MainTab.home:
      case MainTab.history:
      case MainTab.booking:
      case MainTab.notification:
      case MainTab.account:
    }
    return Container();
  }

  BottomNavigationBarItem get tab {
    switch (this) {
      case MainTab.home:
        return BottomNavigationBarItem(
          icon: Column(
            children: [
              ImageIcon(
                AssetImage(AppImages.icDash),
                color: AppColors.background,
              ),
              ImageIcon(
                AssetImage(AppImages.icHomeBar),
                color: AppColors.greyTextColor,
              ),
            ],
          ),
          activeIcon: Column(
            children: [
              ImageIcon(
                AssetImage(AppImages.icDash),
                color: AppColors.buttonViolet,
              ),
              ImageIcon(
                AssetImage(AppImages.icHomeBar),
                color: AppColors.buttonViolet,
              ),
            ],
          ),
          label: 'Home',
        );
      case MainTab.history:
        return BottomNavigationBarItem(
          icon: Column(
            children: [
              ImageIcon(
                AssetImage(AppImages.icDash),
                color: AppColors.background,
              ),
              ImageIcon(
                AssetImage(AppImages.icHomeBar),
                color: AppColors.greyTextColor,
              ),
            ],
          ),
          activeIcon: Column(
            children: [
              ImageIcon(
                AssetImage(AppImages.icDash),
                color: AppColors.buttonViolet,
              ),
              ImageIcon(
                AssetImage(AppImages.icHomeBar),
                color: AppColors.buttonViolet,
              ),
            ],
          ),
          label: 'History',
        );
      case MainTab.booking:
        return BottomNavigationBarItem(
            icon: Column(
              children: [
                ImageIcon(
                  AssetImage(AppImages.icDash),
                  color: AppColors.background,
                ),
                ImageIcon(
                  AssetImage(AppImages.icTwinkleColors),
                  color: AppColors.greyTextColor,
                ),
              ],
            ),
            activeIcon: Column(
              children: [
                ImageIcon(
                  AssetImage(AppImages.icDash),
                  color: AppColors.buttonViolet,
                ),
                ImageIcon(
                  AssetImage(AppImages.icTwinkleColors),
                  color: AppColors.buttonViolet,
                ),
              ],
            ),
            label: 'Booking');
      case MainTab.notification:
        return BottomNavigationBarItem(
            icon: Column(
              children: [
                ImageIcon(
                  AssetImage(AppImages.icDash),
                  color: AppColors.background,
                ),
                ImageIcon(
                  AssetImage(AppImages.icSearch),
                  color: AppColors.greyTextColor,
                ),
              ],
            ),
            activeIcon: Column(
              children: [
                ImageIcon(
                  AssetImage(AppImages.icDash),
                  color: AppColors.buttonViolet,
                ),
                ImageIcon(
                  AssetImage(AppImages.icSearch),
                  color: AppColors.buttonViolet,
                ),
              ],
            ),
            label: 'Notification');
     case MainTab.account:
        return BottomNavigationBarItem(
          icon: Column(
            children: [
              ImageIcon(
                AssetImage(AppImages.icDash),
                color: AppColors.background,
              ),
              ImageIcon(
                AssetImage(AppImages.icSearch),
                color: AppColors.greyTextColor,
              ),
            ],
          ),
          activeIcon: Column(
            children: [
              ImageIcon(
                AssetImage(AppImages.icDash),
                color: AppColors.buttonViolet,
              ),
              ImageIcon(
                AssetImage(AppImages.icSearch),
                color: AppColors.buttonViolet,
              ),
            ],
          ),
          label: 'Account');
    }
  }

  String get title {
    switch (this) {
      case MainTab.home:
        return 'Home';
      case MainTab.history:
        return 'History';
      case MainTab.booking:
        return 'Booking';
      case MainTab.notification:
        return 'Notification';
      case MainTab.account:
        return 'Account';
    }
  }
}
