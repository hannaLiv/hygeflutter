import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/san_pham_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_responses.dart';
import 'package:twinkle_colors_flutter_app/services/user_api_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/home/tabs/home_tab/home_tab_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';
import 'package:uuid/uuid.dart';

import 'main_state.dart';

class MainLogic extends GetxController {
  final state = MainState();
  final UserApiService userApiService = Get.find();

  void switchTap(int index) {
    state.selectedIndex.value = index;
    state.pageController.jumpToPage(index);
  }

  /// Get DichVuDetails
  void getDichVuDetail(int productId) async {
    state.loadDichVuDetailStatus.value = LoadStatus.loading;
    try {
      final result = await userApiService.getProductsWithId(productId);
      state.dichVuDetailResponses.value = result;
      state.loadDichVuDetailStatus.value = LoadStatus.success;
    } catch (e) {
      logger.e("Home tab get categories error: $e");
      state.loadDichVuDetailStatus.value = LoadStatus.failure;
    }
  }

  void getUserInfo() async {
    state.loadUserInfoStatus.value = LoadStatus.loading;
    try {
      final result = await userApiService.getUserInfo();
      state.userInfoResponses.value = result;
      state.loadUserInfoStatus.value = LoadStatus.success;
      state.addressController.text = state.userInfoResponses.value.diaChi ?? 'Chưa có vị trí được chọn';
    } catch (e) {
      logger.e("Home tab get user info: $e");
      state.loadUserInfoStatus.value = LoadStatus.failure;
    }
  }

  void saveTimeSelected(int time) {
    SanPhamEntity? filterTime = SanPhamEntity();
    try {
      filterTime = state.dichVuDetailResponses.value.thoiGianDTOs?.firstWhere((element) => element.sanPhamThoiGianId == time);
    } catch (e) {}
    final text = "${(filterTime?.thoiGianTen ?? "")} ${filterTime?.thoiGianGiaTien} đ";
    state.selectedTimeString.value = text;
    if (state.timeMoney.value == 0.0) {
      state.timeMoney.value = filterTime?.thoiGianGiaTien ?? 0;
    }
    if (state.sanPhamIdTime.value != 0 && state.sanPhamIdTime.value == time) {
      state.totalMoney.value += filterTime?.thoiGianGiaTien ?? 0.0;
    } else if (state.sanPhamIdTime.value != 0 && state.sanPhamIdTime.value != time) {
      state.totalMoney.value -= state.timeMoney.value;
      state.totalMoney.value += filterTime?.thoiGianGiaTien ?? 0.0;
    } else {
      state.totalMoney.value += filterTime?.thoiGianGiaTien ?? 0.0;
    }
    state.sanPhamIdTime.value = filterTime?.sanPhamThoiGianId ?? 0;
    state.timeMoney.value = filterTime?.thoiGianGiaTien ?? 0;
  }

  /// Dich vu them selected
  void selectedDichVuThem(int index) {
    if (state.dichVuThemSelectedIndex.length > 0) {
      final checkExistIndex = state.dichVuThemSelectedIndex.contains(index);
      if (checkExistIndex) {
        state.dichVuThemSelectedIndex.remove(index);
        state.totalMoney.value -= state.dichVuDetailResponses.value.sanPhamBoSungDTOs?[index].sanPhamDTO?.giaGoc ?? 0.0;
      } else {
        state.dichVuThemSelectedIndex.add(index);
        state.totalMoney.value += state.dichVuDetailResponses.value.sanPhamBoSungDTOs?[index].sanPhamDTO?.giaGoc ?? 0.0;
      }
    } else {
      state.dichVuThemSelectedIndex.add(index);
      state.totalMoney.value += state.dichVuDetailResponses.value.sanPhamBoSungDTOs?[index].sanPhamDTO?.giaGoc ?? 0.0;
    }
  }

  /// Dich vu them selected
  void switchedIndex(int index) {
    if (state.tuyChonSwichedIndex.length > 0) {
      final checkExistIndex = state.tuyChonSwichedIndex.contains(index);
      if (checkExistIndex) {
        state.tuyChonSwichedIndex.remove(index);
        state.totalMoney.value -= state.dichVuDetailResponses.value.tuyChonDTOs?[index].giaGoc ?? 0.0;
      } else {
        state.tuyChonSwichedIndex.add(index);
        state.totalMoney.value += state.dichVuDetailResponses.value.tuyChonDTOs?[index].giaGoc ?? 0.0;
      }
    } else {
      state.tuyChonSwichedIndex.add(index);
      state.totalMoney.value += state.dichVuDetailResponses.value.tuyChonDTOs?[index].giaGoc ?? 0.0;
    }
  }

  void changeCheckedDieuKhoan(bool value) {
    state.checkedDieuKhoan.value = value;
  }

  void saveDichVuSelected(DichVuEntity dichVu) {
    state.dichVuSelected.value = dichVu;
  }

  /// Order
  void sendOrder() async {
    state.loadOrderStatus.value = LoadStatus.loading;
    try {
      var uuid = Uuid();
      var getLsIdTuyChon = [];
      try {
        state.tuyChonSwichedIndex.forEach((element) {
          getLsIdTuyChon.add(state.dichVuDetailResponses.value.tuyChonDTOs?[element].id);
        });
      } catch (e) {}
      final orderDetailsDTO = {
        "id": 0,
        "idOrder": 0,
        "soLuong": 1,
        "idSPThoiGian": 0,
        "myProperty": 0,
        "idTuyChons": getLsIdTuyChon.join(','),
        "idComBo": 0,
        "typeId": 5,
        "statusId": 5,
        "ghiChu": state.noteController.text.toString(),
      };
      final orderDTO = {
        "id": 0,
        "ma": uuid.v4(),
        "diaChi": state.address.value,
        "soDienThoai": state.userInfoResponses.value.phoneNumber ?? '',
        "ghiChu": state.noteController.text,
        "idVouchers": "",
        "idTuyChons": getLsIdTuyChon.join(','),
        "giaGoc": state.totalMoney.value,
        "giaGiam": 0,
        //  "thoiGian": "${DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now())}",
      };
      final result = await userApiService.sendOrder({
        "orderDTO": orderDTO,
        "orderDetailsDTO": [orderDetailsDTO],
      });
      if (result == true) {
        AppDialog.errorDialog(
          title: "Success",
          textCancel: "OK",
          message: "Order thành công vui lòng kiểm tra lịch sử giao dịch",
          onCancel: () => Get.offAll(HomeTab()),
        );
      }
      state.loadOrderStatus.value = LoadStatus.success;
    } catch (e) {
      state.loadOrderStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        title: "Lỗi",
        textCancel: "Thoát",
        message: "Đã xảy ra lỗi khi order",
        onCancel: () => Get.back(),
      );
    }
  }

  void saveText(String text) {
    state.address.value = text;
  }
}
