import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_responses.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/account/account_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/history/history_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/notification/notification_page.dart';

import 'booking/booking_page.dart';

class MainState {
  ///Select index- responsive
  late RxInt selectedIndex;

  ///PageView page
  late List<Widget> pageList;
  late PageController pageController;

  /// Responses from server
  late Rx<DichVuResponses> dichVuDetailResponses;
  late Rx<DichVuResponses> orderResponses;
  late Rx<TokenEntity> userInfoResponses;

  final loadDichVuDetailStatus = LoadStatus.initial.obs;
  final loadUserInfoStatus = LoadStatus.initial.obs;
  final loadOrderStatus = LoadStatus.initial.obs;

  /// Time selected
  final selectedTimeString = 'Vui lòng chọn'.obs;
  final sanPhamIdTime = 0.obs;
  final timeMoney = 0.0.obs;

  /// Dich vu them selected
  final RxList<int> dichVuThemSelectedIndex = <int>[].obs;
  final RxList<int> tuyChonSwichedIndex = <int>[].obs;

  /// Get Money
  final totalMoney = 0.0.obs;

  /// Dieu khoan
  final checkedDieuKhoan = false.obs;

  /// Dich vu selected
  final Rx<DichVuEntity> dichVuSelected = DichVuEntity().obs;

  /// Text controller
  late TextEditingController addressController = TextEditingController();
  late TextEditingController noteController = TextEditingController();
  late Rx<String> address ;

  MainState() {
    dichVuDetailResponses = DichVuResponses().obs;
    orderResponses = DichVuResponses().obs;
    userInfoResponses = TokenEntity().obs;
    //Initialize index
    selectedIndex = 1.obs;
    //PageView page
    pageList = [
      HistoryPage(),
      BookingPage(),
      NotificationPage(),
      AccountPage(),
    ];
    //Page controller
    pageController = PageController();
    addressController = TextEditingController(text: 'Chưa có vị trí được chọn');
    noteController = TextEditingController(text: '');
    address = addressController.text.obs;
  }
}
