import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';

import 'main_logic.dart';
import 'main_state.dart';
import 'tab/main_tab.dart';

class MainPage extends StatefulWidget {
  final DichVuEntity? productId;

  MainPage({this.productId});

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final MainLogic logic = Get.put(MainLogic());
  final MainState state = Get.find<MainLogic>().state;

  final tabs = [
    MainTab.history,
    MainTab.booking,
    MainTab.notification,
    MainTab.account,
  ];

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      logic.switchTap(1);
    });
    logic.saveDichVuSelected(widget.productId ?? DichVuEntity());
    logic.getDichVuDetail(widget.productId?.idDichVu ?? 0);
    logic.getUserInfo();
  }

  @override
  void dispose() {
    Get.delete<MainLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: Colors.transparent,
      body: _buildPageView(),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  Widget _buildPageView() {
    return PageView(
      controller: state.pageController,
      children: state.pageList,
      physics: new NeverScrollableScrollPhysics(),
      onPageChanged: (index) {
        logic.switchTap(index);
      },
    );
  }

  Widget _buildBottomNavigationBar() {
    final size = MediaQuery.of(context).size;
    return Obx(() {
      return Container(
        width: size.width,
        //height: 100,
        child: BottomNavigationBar(
          backgroundColor: AppColors.background,
          elevation: 0,
          type: BottomNavigationBarType.fixed,
          currentIndex: state.selectedIndex.value,
          unselectedItemColor: AppColors.greyTextColor,
          selectedItemColor: AppColors.buttonViolet,
          items: tabs.map((e) => e.tab).toList(),
          onTap: (index) {
            logic.switchTap(index);
          },
        ),
      );
    });
  }
}
