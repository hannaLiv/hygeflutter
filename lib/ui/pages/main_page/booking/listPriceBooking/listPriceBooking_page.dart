import 'package:flutter/material.dart';

class ListPriceBookingPage extends StatefulWidget {
  const ListPriceBookingPage({ Key? key }) : super(key: key);

  @override
  _ListPriceBookingPageState createState() => _ListPriceBookingPageState();
}

class _ListPriceBookingPageState extends State<ListPriceBookingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Center(child: Icon(Icons.arrow_back,color: Colors.orange,size: 30,),)
              ),
              SizedBox(width: 20,),
              Expanded(
                child: Text("BẢNG GIÁ DỊCH VỤ",
                  style: TextStyle(color: Colors.blue,fontSize: 18,fontWeight: FontWeight.w600),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}