import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/models/entities/san_pham_bo_sung_dtos_entity.dart';

typedef OnDichVuThemCallBack(int index);

class DichVuThemItem extends StatelessWidget {
  final SanPhamBoSungDtosEntity? data;
  final List<int>? itemSelected;
  final OnDichVuThemCallBack? itemTap;
  final int? indexSelected;

  DichVuThemItem({
    this.data,
    this.itemSelected,
    this.itemTap,
    this.indexSelected,
  });

  @override
  Widget build(BuildContext context) {
    bool _itemSelected = false;
    final size = MediaQuery.of(context).size;
    try {
      final result = itemSelected?.firstWhere((element) => element == indexSelected);
      result != null ? _itemSelected = true : _itemSelected = false;
    } catch (e) {}
    final box = _itemSelected ? BoxDecoration(color: Colors.orange, border: Border.all(width: 0.5, color: Colors.grey), borderRadius: BorderRadius.all(Radius.circular(10.0))) : BoxDecoration(border: Border.all(width: 0.5, color: Colors.grey), borderRadius: BorderRadius.all(Radius.circular(10.0)));

    return GestureDetector(
      onTap: () => itemTap?.call(indexSelected ?? 0),
      child: Container(
        decoration: box,
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(right: 10),
        width: size.width * .3,
        height: size.width * .3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              data?.sanPhamDTO?.ten ?? '',
              textAlign: TextAlign.center,
              style: _itemSelected ? AppTextStyle.whiteS18W600 : AppTextStyle.blackS18W600,
            ),
            Text(
              data?.sanPhamDTO?.ghiChu ?? '',
              textAlign: TextAlign.center,
              style: _itemSelected ? AppTextStyle.whiteS14W400 : AppTextStyle.greyS12W400,
            ),
          ],
        ),
      ),
    );
  }
}
