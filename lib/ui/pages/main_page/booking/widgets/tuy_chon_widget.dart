import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/models/entities/san_pham_entity.dart';

import 'dich_vu_them_item.dart';

class TuyChonWidget extends StatelessWidget {
  final SanPhamEntity? data;
  final List<int>? itemSwitched;
  final OnDichVuThemCallBack? itemTap;
  final int? indexSelected;

  TuyChonWidget({
    this.data,
    this.itemSwitched,
    this.itemTap,
    this.indexSelected,
  });

  @override
  Widget build(BuildContext context) {
    bool _itemSwitched = false;
    final size = MediaQuery.of(context).size;
    try {
      final result = itemSwitched?.firstWhere((element) => element == indexSelected);
      result != null ? _itemSwitched = true : _itemSwitched = false;
    } catch (e) {}
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      width: size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
              child: Text(
            data?.ten ?? '',
            maxLines: 2,
            style: TextStyle(color: Colors.grey[600], fontSize: 16),
          )),
          CupertinoSwitch(
            value: _itemSwitched,
            onChanged: (newStatus) {
              itemTap?.call(indexSelected ?? 0);
            },
          )
        ],
      ),
    );
  }
}
