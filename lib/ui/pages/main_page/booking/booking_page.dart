import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/controllers/app.Controller.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/services/user_api_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/booking/widgets/dich_vu_them_item.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_logic.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_state.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/error_list_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/loading_widget.dart';

import 'bookingConfirm/bookingConfirm_page.dart';
import 'listPriceBooking/listPriceBooking_page.dart';
import 'widgets/tuy_chon_widget.dart';

class BookingPage extends StatefulWidget {
  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  final controller = Get.put(AppController());
  final MainLogic logic = Get.put(MainLogic());

  MainState get state => logic.state;

  Size? size;
  bool service_1 = false;
  bool service_2 = false;
  bool service_3 = false;
  bool service_4 = false;

  int selectedItemRoom = -1;
  GlobalKey dropdownButtonKey = GlobalKey();
  AppController appController = Get.find();
  final UserApiService userApiService = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      await appController.getObjectBookingByIdService(appController.serviceSelected?.id);
      this.setState(() {});
      print(appController.listThoiGianDTO?.length);
      selectedItemRoom = (appController.listThoiGianDTO?.first)?.sanPhamThoiGianId ?? -1;
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: _buildHeader(),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Stack(
      children: [
        Obx(() {
          if (state.loadDichVuDetailStatus.value == LoadStatus.failure || state.loadUserInfoStatus.value == LoadStatus.failure) {
            return _buildFailureWidget();
          } else if (state.loadDichVuDetailStatus.value == LoadStatus.loading || state.loadUserInfoStatus.value == LoadStatus.loading) {
            return _buildLoadingWidget();
          } else {
            return SingleChildScrollView(
              child: Column(
                children: [
                  _buildWorkBoxOption(
                    label: "Thời gian",
                    description: "Vui lòng ước tính chính xác diện tích cần dọn dẹp.",
                    widget: _buildSelectDropDownBox(),
                    height: 120,
                  ),
                  _buildWorkBoxOption(
                      label: "Dịch vụ thêm",
                      description: "Bạn có thể chọn thêm dịch vụ đi kèm.",
                      widget: Container(
                        height: 150,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: state.dichVuDetailResponses.value.sanPhamBoSungDTOs?.length ?? 0,
                          itemBuilder: (context, index) {
                            final item = state.dichVuDetailResponses.value.sanPhamBoSungDTOs?[index];
                            return Obx(() {
                              return DichVuThemItem(
                                data: item,
                                indexSelected: index,
                                itemTap: (indexSelected) => logic.selectedDichVuThem(indexSelected),
                                itemSelected: state.dichVuThemSelectedIndex.value,
                              );
                            });
                          },
                        ),
                      ),
                      height: (size!.width * .3) + 40),
                  _buildWorkBoxOption(
                      label: "Tùy chọn",
                      widget: Column(
                        children: [
                          Container(
                            height: 200,
                            child: ListView.builder(
                                itemCount: state.dichVuDetailResponses.value.tuyChonDTOs?.length ?? 0,
                                itemBuilder: (context, index) {
                                  final item = state.dichVuDetailResponses.value.tuyChonDTOs?[index];
                                  return Obx(() {
                                    return TuyChonWidget(
                                      data: item,
                                      itemTap: (switchedIndex) => logic.switchedIndex(switchedIndex),
                                      indexSelected: index,
                                      itemSwitched: state.tuyChonSwichedIndex.value,
                                    );
                                  });
                                }),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                            child: Text("*Lưu ý: Dịch vụ thêm và tùy chọn thêm sẽ phát sinh thêm phụ phí", textAlign: TextAlign.start, style: TextStyle(color: Colors.grey[600], fontSize: 14)),
                          )
                        ],
                      ),
                      height: 260),
                  _buildTotalBox(),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    margin: EdgeInsets.only(bottom: 150),
                    height: 50,
                    child: AppOrangeButton(
                      title: "Xác nhận",
                      onPressed: () => Get.to(
                        BookingConfirmPage(),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        }),
        _buildLoadingWidget(),
      ],
    );
  }

  Widget _buildHeader() {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        GestureDetector(
            onTap: () => Get.back(),
            child: Center(
              child: Icon(
                Icons.arrow_back,
                color: Colors.orange,
                size: 30,
              ),
            )),
        SizedBox(
          width: 10,
        ),
        Container(
          child: Image.asset(
            "assets/images/location.png",
            width: 30,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: GestureDetector(
            onTap: onAddressTap,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Obx(() {
                  return Text(
                    state.address.value,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  );
                }),
                // Text(
                //   "Landmark 81,Điện Biên Phủ,Vinhome",
                //   maxLines: 1,
                //   overflow: TextOverflow.ellipsis,
                //   style: TextStyle(color: Colors.black, fontSize: 14),
                // ),
              ],
            ),
          ),
        )
      ],
    ));
  }

  Widget _buildWorkBoxOption({String? label, Widget? widget, double? height, String? description}) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.all(10),
      //height: height==0?null:height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            label ?? '',
            style: TextStyle(color: Colors.orange[600], fontSize: 18, fontWeight: FontWeight.w600),
          ),
          SizedBox(height: 10),
          description != null
              ? Text(
                  description,
                  style: TextStyle(color: Colors.grey[600], fontSize: 14),
                )
              : SizedBox(),
          SizedBox(height: 10),
          widget ?? SizedBox(),
        ],
      ),
    );
  }

  Widget _buildSelectDropDownBox() {
    return Container(
      width: double.infinity,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        border: Border.all(width: 0.5, color: Colors.grey),
      ),
      child: Row(
        children: [
          Expanded(
              child: Container(
            padding: EdgeInsets.all(10),
            child: Obx(() {
              return DropdownButton(
                onChanged: (value) {
                  logic.saveTimeSelected(int.tryParse(value.toString()) ?? 0);
                },
                key: dropdownButtonKey,
                underline: SizedBox(),
                icon: SizedBox(),
                hint: Text(state.selectedTimeString.value),
                items: state.dichVuDetailResponses.value.thoiGianDTOs?.map(
                  (data) {
                    final text = "${(data.thoiGianTen ?? "")} ${data.thoiGianGiaTien} đ";
                    return DropdownMenuItem(
                      child: Text(text),
                      value: data.sanPhamThoiGianId,
                    );
                  },
                ).toList(),
              );
            }),
          )),
          GestureDetector(
            onTap: () {},
            child: Container(
              width: 50,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.only(topRight: Radius.circular(10.0), bottomRight: Radius.circular(10.0)),
              ),
              child: Center(
                child: Icon(
                  Icons.arrow_drop_down,
                  size: 30,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTotalBox() {
    return Container(
      width: size?.width,
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Column(
        children: [
          Expanded(
              child: Row(
            children: [
              Expanded(
                  child: Text(
                "Tổng phí dịch vụ: ",
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              )),
              GestureDetector(
                  onTap: () => Get.to(ListPriceBookingPage()),
                  child: Row(
                    children: [
                      Obx(() {
                        return Text(
                          "${state.totalMoney.value} vnđ",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        );
                      }),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.info,
                        size: 30,
                        color: Colors.blue,
                      ),
                    ],
                  ))
            ],
          )),
          Divider(
            color: Colors.black,
            thickness: 0.5,
          )
        ],
      ),
    );
  }

  /// Loading view
  Widget _buildLoadingWidget() {
    return Obx(() {
      final loadLsProducts = state.loadDichVuDetailStatus.value == LoadStatus.loading;
      final loadUserInfoStatus = state.loadUserInfoStatus.value == LoadStatus.loading;
      if (loadLsProducts || loadUserInfoStatus) {
        return LoadingIndicatorWidget();
      } else {
        return SizedBox();
      }
    });
  }

  /// Handle when load data failure
  Widget _buildFailureWidget() {
    return ErrorListWidget();
  }

  void onAddressTap() {
    AppDialog.showInputDialog(
      state.addressController,
      onChanged: (value) => logic.saveText(value),
    );
  }
}
