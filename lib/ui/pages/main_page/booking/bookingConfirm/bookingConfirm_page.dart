import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/booking/listPriceBooking/listPriceBooking_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/loading_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field.dart';

import '../../main_logic.dart';
import '../../main_state.dart';

class BookingConfirmPage extends StatefulWidget {
  const BookingConfirmPage({Key? key}) : super(key: key);

  @override
  _BookingConfirmPageState createState() => _BookingConfirmPageState();
}

class _BookingConfirmPageState extends State<BookingConfirmPage> {
  Size? size;
  final MainLogic logic = Get.put(MainLogic());

  MainState get state => logic.state;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            children: [
              GestureDetector(
                  onTap: () => Get.back(),
                  child: Center(
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.orange,
                      size: 30,
                    ),
                  )),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: Text(
                  "XÁC NHẬN DỊCH VỤ",
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.w600),
                ),
              )
            ],
          ),
        ),
      ),
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    return Stack(
      children: [
        Container(
          width: size?.width,
          child: Obx(() {
            return SingleChildScrollView(
              child: Column(
                children: [
                  _buildWorkBoxOption(
                      label: "Thông tin đơn hàng",
                      widget: Column(
                        children: [
                          _buildBoxBill(),
                          SizedBox(
                            height: 20,
                          ),
                          _buildTotalBox(),
                          SizedBox(
                            height: 20,
                          ),
                          _buildTermsAndConditionsBox(),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            margin: EdgeInsets.only(bottom: 150),
                            height: 50,
                            child: AppOrangeButton(
                              title: "Đặt Lịch",
                              isEnable: state.checkedDieuKhoan.value,
                              onPressed: () => logic.sendOrder(),
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            );
          }),
        ),
        _buildLoadingWidget(),
      ],
    );
  }

  Widget _buildWorkBoxOption({String? label, String? description, Widget? widget, double height = 100}) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            label ?? '',
            style: TextStyle(color: Colors.orange[600], fontSize: 18, fontWeight: FontWeight.w600),
          ),
          Text(
            description ?? '',
            style: TextStyle(color: Colors.grey[600], fontSize: 14),
          ),
          widget ?? SizedBox(),
        ],
      ),
    );
  }

  Widget _buildBoxBill() {
    String dichVuName = '';
    List<String> lsDichVuThemName = [];
    List<String> lsDichVuTuyChon = [];
    try {
      final dichVuSelected = state.dichVuDetailResponses.value.thoiGianDTOs?.firstWhere((element) => element.sanPhamThoiGianId == state.sanPhamIdTime.value);
      dichVuName = dichVuSelected?.thoiGianTen ?? '';

      for (int i = 0; i < state.dichVuThemSelectedIndex.length; i++) {
        final dichVuthem = state.dichVuDetailResponses.value.sanPhamBoSungDTOs?[i].sanPhamDTO?.ten;
        lsDichVuThemName.add(dichVuthem ?? '');
      }
      for (int i = 0; i < state.tuyChonSwichedIndex.length; i++) {
        final tuyChon = state.dichVuDetailResponses.value.tuyChonDTOs?[i].ten;
        lsDichVuTuyChon.add(tuyChon ?? '');
      }
    } catch (e) {}
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildItemBill("Khách hàng: ", value: state.userInfoResponses.value.name ?? ''),
          _buildItemBill("SĐT: ", value: state.userInfoResponses.value.phoneNumber ?? ""),
          _buildItemBill("Địa chỉ: ", value: state.userInfoResponses.value.diaChi ?? ""),
          Divider(
            color: Colors.black,
            thickness: 0.5,
          ),
          SizedBox(
            height: 10,
          ),
          _buildItemBill("Dịch vụ: ", value: "${state.dichVuSelected.value.ten} $dichVuName"),
          _buildItemBill(
            "DV thêm: ",
            widget: Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: lsDichVuThemName.map((e) => Text(e)).toList(),
              ),
            ),
          ),
          _buildItemBill(
            "Tùy chọn: ",
            widget: Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: lsDichVuTuyChon.map((e) => Text(e)).toList(),
              ),
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.5,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Ghi chú bổ sung (nếu có): ",
            style: TextStyle(color: Colors.grey[600], fontSize: 16),
          ),
          SizedBox(
            height: 10,
          ),
          AppTextField(
            textFormFieldHeight: 90,
            hintText: "Nhập thông tin",
            inputController: state.noteController,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "*Lưu ý: Dịch vụ thêm và tùy chọn thêm sẽ phát sinh thêm phụ phí.",
            style: TextStyle(color: Colors.grey[600], fontSize: 15),
          )
        ],
      ),
    );
  }

  Widget _buildItemBill(String label, {String? value, Widget? widget}) {
    return Container(
      padding: EdgeInsets.only(bottom: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: TextStyle(color: Colors.grey[600], fontSize: 16),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: value == null || value == ""
                  ? widget ?? SizedBox()
                  : Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text(
                        value,
                        style: TextStyle(color: Colors.grey[600], fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                    ))
        ],
      ),
    );
  }

  Widget _buildTermsAndConditionsBox() {
    return Container(
      width: size?.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Checkbox(
              value: state.checkedDieuKhoan.value,
              onChanged: (value) {
                logic.changeCheckedDieuKhoan(value ?? false);
              }),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: RichText(
            text: TextSpan(
              text: 'Tôi đồng ý với các ',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16, color: Colors.grey[600]),
              children: <TextSpan>[
                TextSpan(text: 'Điều khoản dịch vụ', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.orange)),
                TextSpan(text: ' và'),
                TextSpan(text: ' Chính sách bảo mật', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.orange)),
                TextSpan(text: ' của'),
                TextSpan(text: ' Hyge', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.orange)),
              ],
            ),
          ))
        ],
      ),
    );
  }

  Widget _buildTotalBox() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Voucher",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 16),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: [
                      Text(
                        "Nope",
                        style: TextStyle(color: Colors.grey[600], fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                      Icon(
                        Icons.arrow_right,
                        size: 30,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.5,
          ),
          Container(
            child: Column(
              children: [
                _buildItemBill("Tổng phí dịch vụ", value: "${state.totalMoney.value} vnđ"),
                _buildItemBill("Giảm giá", value: "0 vnđ"),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.5,
          ),
          _buildItemTotalBox()
        ],
      ),
    );
  }

  Widget _buildItemTotalBox() {
    return Container(
        child: Row(
      children: [
        Expanded(
            child: Text(
          "Tổng phí dịch vụ: ",
          style: TextStyle(color: Colors.grey[600], fontSize: 16, fontWeight: FontWeight.w600),
        )),
        GestureDetector(
            onTap: () => Get.to(ListPriceBookingPage()),
            child: Row(
              children: [
                Text(
                  "${state.totalMoney.value} vnđ",
                  style: TextStyle(color: Colors.blue, fontSize: 16, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.info,
                  size: 30,
                  color: Colors.blue,
                ),
              ],
            ))
      ],
    ));
  }

  /// Loading view
  Widget _buildLoadingWidget() {
    return Obx(() {
      final loadOrderStatus = state.loadOrderStatus.value == LoadStatus.loading;
      if (loadOrderStatus) {
        return LoadingIndicatorWidget();
      } else {
        return SizedBox();
      }
    });
  }
}
