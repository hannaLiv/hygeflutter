import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_dimens.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/sign_in_logic.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/widgets/login_facebook_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_in/widgets/login_google_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/sign_up/sign_up_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/images/app_black_logo.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/loading_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field_corner.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final SignInLogic logic = Get.put(SignInLogic());
  SignInState get state => logic.state;
  bool isUserNameValidate = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    Get.delete<SignInLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Obx(() {
          return Stack(
            children: [
              Column(
                children: [
                  _buildImageLogo(),
                  LoginFacebookWidget(
                    onFacebookTap: () => logic.signInWithFacebookAccount(context: context),
                  ),
                  LoginGoogleWidget(
                    onGoogleTap: () => logic.signInWithGoogleAccount(context: context),
                  ),
                  _buildTextSignInWithEmail(),
                  _buildInputWidget(
                    controller: state.emailController,
                    inputType: TextInputType.text,
                    labelText: S.of(context).username_title,
                    hintText: S.of(context).username_title,
                    errorText: state.checkUsernameError.value ? state.usernameErrorText.value : null,
                    onChanged: (value) => logic.checkUsername(context),
                  ),
                  SizedBox(height: 12),
                  _buildInputWidget(
                    controller: state.passwordController,
                    labelText: S.of(context).password_title,
                    hintText: S.of(context).password_title,
                    inputType: TextInputType.visiblePassword,
                    obscureText: true,
                    errorText: state.checkPasswordError.value ? state.passwordErrorText.value : null,
                    onChanged: (value) => logic.checkPassword(context),
                  ),
                  _buildSignInButton(),
                  Spacer(),
                  _buildSignUpWidget(),
                  SizedBox(height: 40),
                ],
              ),
              _buildLoadingWidget(),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildImageLogo() {
    return Column(
      children: [
        Center(
          child: Container(
            margin: EdgeInsets.only(top: 50, bottom: 20),
            child: AppBlackLogo(),
          ),
        ),
        Text(
          S.of(context).sign_in_welcome_back,
          style: AppTextStyle.blackS22W600,
        )
      ],
    );
  }

  Widget _buildTextSignInWithEmail() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.padding15),
      child: Text(
        S.of(context).sign_in_with_email,
        style: AppTextStyle.greyS16W600,
      ),
    );
  }

  Widget _buildInputWidget({
    TextEditingController? controller,
    String? errorText,
    String? hintText,
    String? labelText,
    TextInputType? inputType,
    bool obscureText = false,
    ValueChanged<String>? onChanged,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppDimens.padding18),
      child: AppTextFieldCorner(
        inputController: controller,
        onChanged: onChanged,
        bgColor: AppColors.greyBgColor,
        textInputType: inputType,
        obscureText: obscureText,
        errorText: errorText,
        labelText: labelText,
        hintText: hintText,
        isShowBorder: true,
      ),
    );
  }

  Widget _buildSignInButton() {
    /// Check enable button Sign in
    final bool isEnableButton = state.emailController.text != '' && state.passwordController.text != '';

    /// Check loading button sign in
    final bool isLoading = state.signInWithCustomerAccountStatus.value == LoadStatus.loading || state.signInWithGoogleStatus.value == LoadStatus.loading || state.signInWithFacebookStatus.value == LoadStatus.loading;

    return Column(
      children: [
        Container(
          width: 254,
          height: 52,
          margin: EdgeInsets.only(top: 25),
          child: AppVioletButton(
            title: S.of(context).common_button_sign_in,
            cornerRadius: AppDimens.buttonCornerRadius50,
            textStyle: AppTextStyle.whiteS16W600,
            onPressed: _onSignInTap,
            isEnable: isEnableButton,
            isLoading: isLoading,
          ),
        ),
        SizedBox(height: 15),
        Text(
          S.of(context).sign_in_forgot_password,
          style: AppTextStyle.nearBlackS13W600UnderLine,
        )
      ],
    );
  }

  Widget _buildSignUpWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          S.of(context).sign_in_not_a_member,
          style: AppTextStyle.greyS13W600,
        ),
        SizedBox(width: 5),
        GestureDetector(
          onTap: () => Get.to(SignUpPage()),
          child: Container(
            child: Text(
              S.of(context).common_button_sign_up,
              style: AppTextStyle.textVioletS13W600,
            ),
          ),
        ),
      ],
    );
  }

  void _onSignInTap() {
    logic.signInWithCustomerAccount(context: context);
  }

  Widget _buildLoadingWidget() {
    return Obx(() {
      final signInWithCustomerAccountStatus = state.signInWithCustomerAccountStatus.value == LoadStatus.loading;
      final signInWithGoogleStatus = state.signInWithGoogleStatus.value == LoadStatus.loading;
      final signInWithFacebookStatus = state.signInWithFacebookStatus.value == LoadStatus.loading;
      if (signInWithCustomerAccountStatus || signInWithGoogleStatus || signInWithFacebookStatus) {
        return LoadingIndicatorWidget();
      } else {
        return SizedBox();
      }
    });
  }
}
