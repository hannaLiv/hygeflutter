import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';

class LoginFacebookWidget extends StatelessWidget {
  final VoidCallback? onFacebookTap;

  const LoginFacebookWidget({this.onFacebookTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, bottom: 15),
      height: 52,
      padding: EdgeInsets.symmetric(horizontal: AppDimens.paddingNormal),
      child: AppBlueButton(
        title: S.of(context).common_button_continue_with_face,
        textStyle: AppTextStyle.whiteS14W600,
        cornerRadius: AppDimens.buttonCornerRadius38,
        onPressed: onFacebookTap,
        alignmentItem: MainAxisAlignment.spaceBetween,
        leadingIcon: Container(
          padding: EdgeInsets.only(left: 16),
          width: 50,
          height: 50,
          child: Image.asset(AppImages.facebookIcon),
        ),
        trailingIcon: Container(
          padding: EdgeInsets.only(left: 16),
          width: 50,
          height: 50,
        ),
      ),
    );
  }
}
