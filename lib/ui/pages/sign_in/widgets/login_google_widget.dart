import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/utils/authentication_firebase.dart';

class LoginGoogleWidget extends StatelessWidget {
  final VoidCallback? onGoogleTap;

  const LoginGoogleWidget({this.onGoogleTap});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Authentication.initializeFirebase(context: context),
        builder: (context, snapshot) {
          return Container(
            height: 52,
            padding: EdgeInsets.symmetric(horizontal: AppDimens.paddingNormal),
            child: AppWhiteButton(
              title: S.of(context).common_button_continue_with_google,
              textStyle: AppTextStyle.blackS14W600,
              cornerRadius: AppDimens.buttonCornerRadius38,
              onPressed: () => snapshot.connectionState == ConnectionState.done ? onGoogleTap?.call() : _errorConnectToFirebase(context),
              alignmentItem: MainAxisAlignment.spaceBetween,
              leadingIcon: Container(
                padding: EdgeInsets.only(left: 16),
                width: 50,
                height: 50,
                child: Image.asset(AppImages.googleIcon),
              ),
              trailingIcon: Container(
                padding: EdgeInsets.only(left: 16),
                width: 50,
                height: 50,
              ),
            ),
          );
        });
  }

  void _errorConnectToFirebase(BuildContext context) {
    AppDialog.errorDialog(message: S.of(context).common_text_error_au_google, textCancel: S.of(context).common_button_cancel);
  }
}
