part of 'sign_in_logic.dart';

class SignInState {
  /// Text Controller
  late TextEditingController emailController = TextEditingController();
  late TextEditingController passwordController = TextEditingController();

  /// Api status
  final signInWithCustomerAccountStatus = LoadStatus.initial.obs;
  final signInWithGoogleStatus = LoadStatus.initial.obs;
  final signInWithFacebookStatus = LoadStatus.initial.obs;

  /// Responses from server
  late Rxn<TokenEntity> userInfo;

  /// Check errors validate text
  final usernameErrorText = ''.obs;
  final checkUsernameError = false.obs;
  final passwordErrorText = ''.obs;
  final checkPasswordError = false.obs;

  /// Initial data
  SignInState() {
    final AuthService authService = Get.find<AuthService>();
    emailController = TextEditingController(text: '');
    passwordController = TextEditingController(text: '');
    userInfo = authService.token;
  }
}
