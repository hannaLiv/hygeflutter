import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/models/entities/token_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/services/api_service.dart';
import 'package:twinkle_colors_flutter_app/services/auth_service.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/home/tabs/home_tab/home_tab_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_commons/app_dialog.dart';
import 'package:twinkle_colors_flutter_app/utils/authentication_firebase.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';
import 'package:twinkle_colors_flutter_app/utils/utils.dart';
import 'package:twinkle_colors_flutter_app/utils/validators.dart';

part 'sign_in_state.dart';

class SignInLogic extends GetxController {
  final state = SignInState();
  final ApiService apiService = Get.find();
  final AuthService authService = Get.find();

  

  /// Sign in with customer account
  void signInWithCustomerAccount({required BuildContext context}) async {
    final username = state.emailController.text;
    final password = state.passwordController.text;
    state.signInWithCustomerAccountStatus.value = LoadStatus.loading;

    try {
      final result = await apiService.signInWithCustomerAccount(username, password);
      if (result != null) {
        /// Check code to show dialog when sign in failure
        final bool checkCodeLogin = Utils.checkCodeSignIn(result.code ?? '');
        if (checkCodeLogin) {
          AppDialog.errorDialog(message: result.message ?? '', textCancel: S.current.common_button_cancel);
          state.signInWithCustomerAccountStatus.value = LoadStatus.initial;
        } else {
          state.userInfo.value = result;
          authService.saveToken(result);
          authService.updateUser(result);
          print('Token: ${authService.token.value}');
          state.signInWithCustomerAccountStatus.value = LoadStatus.success;
          Get.offAll(HomeTab());
        }
      }
    } catch (e) {
      logger.e("Sign in with customer account: $e");
      state.signInWithCustomerAccountStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        title: S.of(context).common_text_sign_in_failure,
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_account,
      );
    }
  }

  /// Sign in with google account
  void signInWithGoogleAccount({required BuildContext context}) async {
    state.signInWithGoogleStatus.value = LoadStatus.loading;
    try {
      String? token = await Authentication.signInWithGoogle(context: context);
      if (token == null || token == '') {
        AppDialog.errorDialog(
          message: S.of(context).common_text_error_au_google,
          textCancel: S.of(context).common_button_cancel,
        );
        state.signInWithGoogleStatus.value = LoadStatus.initial;
      } else {
        final result = await apiService.signInWithFirebase(token);
        if (result != null) {
          if (result.message == null || result.message == '') {
            authService.saveToken(result);
            authService.updateUser(result);
            state.signInWithGoogleStatus.value = LoadStatus.success;
            Get.offAll(MainPage());
          } else {
            AppDialog.errorDialog(message: result.message ?? '', textCancel: S.current.common_button_cancel);
            state.signInWithGoogleStatus.value = LoadStatus.initial;
          }
        }
      }
    } catch (e) {
      logger.e('Sign in with google : $e');
      state.signInWithGoogleStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        title: S.of(context).common_text_sign_in_failure,
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_google,
      );
    }
  }

  /// Sign in with facebook account
  void signInWithFacebookAccount({required BuildContext context}) async {
    state.signInWithFacebookStatus.value = LoadStatus.loading;
    try {
      final token = await Authentication.loginWithFacebook();
      if (token == null || token == '') {
        state.signInWithFacebookStatus.value = LoadStatus.initial;
        return;
      } else {
        final result = await apiService.signInWithFacebook("facebook", token);
        if (result != null) {
          if (result.message == null || result.message == '') {
            authService.saveToken(result);
            authService.updateUser(result);
            state.signInWithFacebookStatus.value = LoadStatus.success;
            Get.offAll(MainPage());
          } else {
            AppDialog.errorDialog(message: result.message ?? '', textCancel: S.current.common_button_cancel);
            state.signInWithFacebookStatus.value = LoadStatus.initial;
          }
        }
        print('Token: $token');
      }
    } catch (e) {
      logger.e('Sign in with facebook : $e');
      state.signInWithFacebookStatus.value = LoadStatus.failure;
      AppDialog.errorDialog(
        title: S.of(context).common_text_sign_in_failure,
        textCancel: S.of(context).common_button_cancel,
        message: S.of(context).common_text_error_au_facebook,
      );
    }
  }

  void checkInputFormat(BuildContext context) {
    final bool checkFormatUserName = checkUsername(context);
    final bool checkFormatPassword = checkPassword(context);
    if (!checkFormatUserName) return;

    if (!checkFormatPassword) return;

    signInWithCustomerAccount(context: context);
  }

  /// Check format username
  bool checkUsername(BuildContext context) {
    final username = state.emailController.text;
    if (username == '' || username.isEmpty) {
      state.usernameErrorText.value = S.of(context).common_error_empty_username;
      state.checkUsernameError.value = true;
      return false;
    } else {
      state.usernameErrorText.value = '';
      state.checkUsernameError.value = false;
      return true;
    }
  }

  /// Check format password
  bool checkPassword(BuildContext context) {
    final password = state.passwordController.text;
    if (password == '' || password.isEmpty) {
      state.passwordErrorText.value = S.of(context).common_error_empty_password;
      state.checkPasswordError.value = true;
      return false;
    } else if (!Validators.validatePassword(password)) {
      state.passwordErrorText.value = S.of(context).common_error_format_password;
      state.checkPasswordError.value = true;
      return false;
    } else {
      state.passwordErrorText.value = '';
      state.checkPasswordError.value = false;
      return true;
    }
  }
}
