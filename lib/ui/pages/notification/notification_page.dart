import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({ Key? key }) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  Size ?size;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: size?.height,
          child: Column(
            children: [
              SizedBox(height: 20,),
              Text("THÔNG BÁO",
                style: TextStyle(color: Colors.blue,fontSize: 22,fontWeight: FontWeight.w800),
              ),
              SizedBox(height: 20,),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (BuildContext context,int index){
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 5,horizontal: 20),
                      child: _buildItemHistory(),
                    );
                  }
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  Widget _buildItemHistory(){
    return Container(
      height: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Icon(Icons.notifications,color: Colors.blue,size: 40,),
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Text("Hoàn thành nhiệm vụ",
              style: TextStyle(
                fontSize: 16,color: Colors.black,fontWeight: FontWeight.w600
              ),
            ),
          ),
          Container(
            child: Text("+ 1000 H-point",
              style: TextStyle(
                fontSize: 14,color: Colors.blue,fontWeight: FontWeight.w600
              ),
            ),
          )
        ],
      ),
    );
  }
}