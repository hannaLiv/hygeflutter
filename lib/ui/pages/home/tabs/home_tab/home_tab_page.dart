import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_dimens.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/configs/app_configs.dart';
import 'package:twinkle_colors_flutter_app/controllers/app.Controller.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/models/responses/categories_responses.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/account/account_page.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/home/tabs/home_tab/widgets/categories_item_widget_carousel.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/home/tabs/home_tab/widgets/categories_item_widget_combo.dart';
import 'package:twinkle_colors_flutter_app/ui/pages/main_page/main_page.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/appbar/app_bar_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/buttons/app_button.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/error_list_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/gridview/app_gridview.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/images/app_circle_avatar.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/loading_widget.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/search/app_search_widget.dart';

import 'home_tab_logic.dart';
import 'home_tab_state.dart';
import 'widgets/categories_item_widget.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  HomeTabLogic logic = Get.put(HomeTabLogic());
  final controller = Get.put(AppController());

  HomeTabState get state => logic.state;
  Size? size;

  @override
  void initState() {
    super.initState();
    logic.getInfoUser();
    logic.getCategories();
    logic.getComboCategories();
    logic.getProducts();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {});
  }

  @override
  void dispose() {
    Get.delete<HomeTabLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.purple,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "HYGE APP",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Stack(
      children: [
        Obx(
          () => Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: 20, left: 20, right: 20),
              decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)), color: Colors.white),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // _buildPromotionCategoriesSlider(),
                    _buildWelcomeContainer(),
                    _buildServiceCategories(),
                    _buildExtraServiceCategoriesSlider(),
                    _buildComboCategories(),
                  ],
                ),
              )),
        ),
        _buildLoadingWidget(),
      ],
    );
  }

  Widget _buildWelcomeContainer() {
    return Container(
      width: double.infinity,
      height: 130,
      padding: EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 5),
      decoration: BoxDecoration(
          border: Border.all(
        color: AppColors.greyColor,
        width: 1,
      )),
      child: Column(
        children: [
          _textItem(S.of(context).home_hello, S.of(context).home_hello),
          _textItem(S.of(context).home_ask_choosen, S.of(context).common_app_name),
          Spacer(),
          _buildButtons(),
        ],
      ),
    );
  }

  Widget _textItem(String title, String value) {
    return Row(
      children: [
        Text(
          title,
          style: AppTextStyle.greyS14W400,
        ),
        SizedBox(width: 5),
        Text(
          value,
          style: AppTextStyle.orangeS14W400,
        ),
      ],
    );
  }

  Widget _buildButtons() {
    return Row(
      children: [
        Expanded(
          child: AppOrangeButton(
            title: S.of(context).common_button_dat_dich_vu,
            onPressed: () => print('dat dich vu'),
          ),
        ),
        SizedBox(width: 5),
        Expanded(
          child: AppBlueButton(
            title: S.of(context).common_button_action,
            onPressed: () => print('hoat dong'),
          ),
        ),
      ],
    );
  }

  Widget _buildServiceCategories() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(S.of(context).home_services, style: AppTextStyle.nearBlackS18Bold),
          SizedBox(height: 5),
          AppGridview(
            physics: NeverScrollableScrollPhysics(),
            crossAxisCount: 2,
            crossAxisSpacing: 3,
            mainAxisSpacing: 3,
            childAspectRatio: 1,
            items: state.items.value,
            itemBuilder: (context, index) {
              final item = state.items.value[index];
              return CategoriesItemWidget(
                dichVuCallback: (dichVu) => Get.to(MainPage(
                  productId: dichVu,
                )),
                item: item,
                imageUrl: 'asset/image/img1.png',
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildComboCategories() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Combos', style: AppTextStyle.nearBlackS18Bold),
          SizedBox(height: 20),
          Container(
            height: 200,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: state.categoriesComboResponses.value.items?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  final item = state.categoriesComboResponses.value.items?[index];
                  return CategoriesItemComboWidget(
                    callback: (dichVu) => Get.to(MainPage(
                      productId: dichVu,
                    )),
                    title: item?.ten,
                    description: item?.ghiChu,
                  );
                }),
          )
        ],
      ),
    );
  }

  Widget _buildPromotionCategoriesSlider() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 20),
        Text('Promotions', style: AppTextStyle.nearBlackS18Bold),
        SizedBox(height: 20),
        Container(
            height: 60,
            child: ListView(shrinkWrap: true, scrollDirection: Axis.horizontal, children: [
              CategoriesItemCarouselWidget(
                title: 'Promotion 1',
              ),
              CategoriesItemCarouselWidget(
                title: 'Promotion 2',
              ),
              CategoriesItemCarouselWidget(
                title: 'Promotion 3',
              ),
            ])),
        SizedBox(height: 20),
      ],
    );
  }

  Widget _buildExtraServiceCategoriesSlider() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.of(context).home_uu_dai,
          style: AppTextStyle.nearBlackS18Bold,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            extraWidget("fridge", "Inside Fridge", true),
            extraWidget("organise", "Organise", false),
            extraWidget("blind", "Small Blinds", false),
          ],
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }

  Widget extraWidget(String img, String name, bool isSelected) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              height: 80,
              width: 80,
              decoration: BoxDecoration(shape: BoxShape.circle, color: AppColors.purple),
              child: Container(
                margin: EdgeInsets.all(17),
                decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/icons/$img.png'), fit: BoxFit.contain)),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: (isSelected == true)
                  ? Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                      child: Center(
                        child: Icon(
                          Icons.check_circle,
                          color: AppColors.pink,
                        ),
                      ),
                    )
                  : Container(),
            ),
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          name,
          style: TextStyle(fontWeight: FontWeight.w500),
        )
      ],
    );
  }

  /// Loading view
  Widget _buildLoadingWidget() {
    return Obx(() {
      final loadLsProducts = state.loadLsProducts.value == LoadStatus.loading;
      if (loadLsProducts) {
        return LoadingIndicatorWidget();
      } else {
        return SizedBox();
      }
    });
  }

  /// Handle when load data failure
  Widget _buildFailureWidget() {
    return ErrorListWidget(
      onRefresh: _onRefresh,
    );
  }

  /// Refresh data
  Future<void> _onRefresh() async {
    logic.getInfoUser();
  }
}
