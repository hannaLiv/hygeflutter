import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';

import 'categories_item_widget.dart';

class CategoriesItemComboWidget extends StatelessWidget {
  final Color? backgroundColor;

  final Color? imageColor;
  final String? title;
  final String? description;
  final VoidCallback? onPlusTap;
  final OnDichVuCallBack ?callback;

  CategoriesItemComboWidget({
    this.backgroundColor,
    this.imageColor,
    this.title,
    this.description,
    this.onPlusTap,
    this.callback
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 0, right: 20, bottom: 20.0),
      width: 300.0,
      decoration: BoxDecoration(
          color: AppColors.pink,
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [
            BoxShadow(
                blurRadius: 10.0,
                offset: Offset(0, 25),
                color:
                    AppColors.pink.withOpacity(0.6),
                spreadRadius: -20.0)
          ]),
      child: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title ?? '',
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
          child: Text(
            description ?? '',
            style: TextStyle(color: Colors.white, fontSize: 14.0),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        FlatButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          color: Colors.black.withOpacity(0.1),
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            "Pick now",
            style: TextStyle(color: Colors.white, fontSize: 12.0),
          ),
          onPressed: () {},
        )
      ]),
    );
  }
}
