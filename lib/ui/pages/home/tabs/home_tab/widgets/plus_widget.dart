import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class PlusWidget extends StatelessWidget {
  final VoidCallback? onPlusTap;

  PlusWidget({this.onPlusTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPlusTap,
      child: Container(
        width: 25,
        height: 25,
        decoration: BoxDecoration(
          color: AppColors.buttonLightViolet,
          borderRadius: BorderRadius.circular(90),
          boxShadow: AppShadow.plusButtonShadow,
        ),
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 12,
        ),
      ),
    );
  }
}
