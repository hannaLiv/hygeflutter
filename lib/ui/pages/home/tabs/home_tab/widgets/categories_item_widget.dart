import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';

typedef OnDichVuCallBack(DichVuEntity entity);

class CategoriesItemWidget extends StatelessWidget {
  final Color? backgroundColor;

  final Color? imageColor;
  final VoidCallback? onPlusTap;
  final String? imageUrl;
  final OnDichVuCallBack? dichVuCallback;
  final DichVuEntity? item;

  CategoriesItemWidget({
    this.backgroundColor,
    this.imageColor,
    this.onPlusTap,
    this.imageUrl,
    this.dichVuCallback,
    this.item,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => dichVuCallback?.call(item ?? DichVuEntity()),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            height: 140,
            width: MediaQuery.of(context).size.width * 0.43,
            decoration: BoxDecoration(
                color: Color(0xffdfdeff),
                border: Border.all(color: AppColors.backgroundBlack, width: 0.5),
                image: DecorationImage(
                  image: AssetImage('assets/images/img1.png'),
                ),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [BoxShadow(blurRadius: 10.0, offset: Offset(0, 20), color: AppColors.textBlack221F1F.withOpacity(0.4), spreadRadius: -20.0)]),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            item?.ten ?? '',
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }
}
