import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class CategoriesItemCarouselWidget extends StatelessWidget {
  final Color? backgroundColor;

  final Color? imageColor;
  final String? title;
  final VoidCallback? onPlusTap;

  CategoriesItemCarouselWidget({
    this.backgroundColor,
    this.imageColor,
    this.title,
    this.onPlusTap,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: 180,
          margin: EdgeInsets.only(bottom: 0, right: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: AppColors.textE9ECF3, width: 1),
              gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                AppColors.pink,
                AppColors.pink,
              ],
            )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      title ?? '',
                      textAlign: TextAlign.center,
                      style: AppTextStyle.whiteS16Bold,
                    ),
                  )),
            ],
          ),
        ),
      ],
    );
  }
}
