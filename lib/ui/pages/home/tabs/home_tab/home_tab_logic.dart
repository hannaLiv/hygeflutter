import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/databases/secure_storage_helper.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/services/user_api_service.dart';
import 'package:twinkle_colors_flutter_app/utils/logger.dart';
import 'package:twinkle_colors_flutter_app/utils/picker_utils.dart';

import 'home_tab_state.dart';

class HomeTabLogic {
  final state = HomeTabState();
  final UserApiService userApiService = Get.find();

  /// Get user information initial
  void getInfoUser() async {
    state.getInfoUserStatus.value = LoadStatus.loading;
    try {
      final tokenEntity = await SecureStorageHelper.getInstance().getToken();
    } catch (e) {
      logger.e("HomeTab get user info error: $e");
      state.getInfoUserStatus.value = LoadStatus.failure;
    }
  }

  /// Get Categories
  void getCategories({int? skipCount, int? maxResultCount, String? sorting}) async {
    state.loadCategoriesStatus.value = LoadStatus.loading;
    try {
      final result = await userApiService.getDichVus(
        sorting: sorting,
        maxResultCount: maxResultCount,
        skipCount: skipCount,
      );
      state.categoriesResponses.value = result;
      state.loadCategoriesStatus.value = LoadStatus.success;
    } catch (e) {
      logger.e("Home tab get categories error: $e");
      state.loadCategoriesStatus.value = LoadStatus.failure;
    }
  }

  void getComboCategories({int? skipCount, int? maxResultCount, String? sorting}) async {
    state.loadCategoriesStatus.value = LoadStatus.loading;
    try {
      final result = await userApiService.getCombos(
        sorting: sorting,
        maxResultCount: maxResultCount,
        skipCount: skipCount,
      );
      state.categoriesComboResponses.value = result;
      state.loadCategoriesStatus.value = LoadStatus.success;
    } catch (e) {
      logger.e("Home tab get categories error: $e");
      state.loadCategoriesStatus.value = LoadStatus.failure;
    }
  }

  void scanImage({required BuildContext context, bool isCamera = false}) async {
    final imagePicked = await PickerUtils.pickerImage(context, isCamera: isCamera);
  }

  /// Get user information initial
  void getProducts() async {
    state.loadLsProducts.value = LoadStatus.loading;
    try {
      final result = await userApiService.getProducts();
      List<DichVuEntity>? items = [];
      try {
        result.items?.forEach((element) {
          if (element.typeId == 1) {
            items.add(element);
          }
        });
      } catch (e) {}
      state.productsResponses.value = result;
      state.loadLsProducts.value = LoadStatus.success;
      state.items.value = items;
    } catch (e) {
      logger.e("get product: $e");
      state.loadLsProducts.value = LoadStatus.failure;
    }
  }
}
