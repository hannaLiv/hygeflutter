import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/models/entities/dich_vu_entity.dart';
import 'package:twinkle_colors_flutter_app/models/enums/load_status.dart';
import 'package:twinkle_colors_flutter_app/models/responses/dich_vu_responses.dart';
import 'package:twinkle_colors_flutter_app/models/responses/user_infor_response.dart';

class HomeTabState {
  /// Responses from server
  late Rx<UserInfoResponse> userInfoResponse;
  late Rx<DichVuResponses> categoriesResponses;
  late Rx<DichVuResponses> categoriesComboResponses;
  late Rx<DichVuResponses> productsResponses;
  late RxList<DichVuEntity> items;

  /// Load api status
  final getInfoUserStatus = LoadStatus.initial.obs;
  final loadCategoriesStatus = LoadStatus.initial.obs;
  final loadLsProducts = LoadStatus.initial.obs;

  HomeTabState() {
    userInfoResponse = UserInfoResponse().obs;
    categoriesResponses = DichVuResponses().obs;
    categoriesComboResponses = DichVuResponses().obs;
    productsResponses = DichVuResponses().obs;
    items = <DichVuEntity>[].obs;
  }
}
