import 'package:flutter/material.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({ Key? key }) : super(key: key);

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  Size ?size;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: size?.height,
          child: Column(
            children: [
              SizedBox(height: 20,),
              Text("LỊCH SỬ HOẠT ĐỘNG",
                style: TextStyle(color: Colors.blue,fontSize: 22,fontWeight: FontWeight.w800),
              ),
              SizedBox(height: 20,),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (BuildContext context,int index){
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 5,horizontal: 20),
                      child: _buildItemHistory(),
                    );
                  }
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  Widget _buildItemHistory(){
    return Container(
      height: 80,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Icon(Icons.account_circle,color: Colors.blue,size: 50,),
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Join Dio",
                    style: TextStyle(
                      fontSize: 16,color: Colors.black,fontWeight: FontWeight.w600
                    ),
                  ),
                  Text("Đã hoàn thành",
                    style: TextStyle(
                      fontSize: 14,color: Colors.grey[600],fontWeight: FontWeight.w500
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("07:53 pm",
                    style: TextStyle(
                      fontSize: 14,color: Colors.grey[600],fontWeight: FontWeight.w600
                    ),
                ),
                Text("21/10/2021",
                  style: TextStyle(
                    fontSize: 14,color: Colors.grey[600],fontWeight: FontWeight.w500
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}