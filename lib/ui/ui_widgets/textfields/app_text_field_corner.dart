import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field.dart';

class AppTextFieldCorner extends AppTextField {
  AppTextFieldCorner({
    final TextEditingController? inputController,
    final TextInputType? textInputType,
    final String? hintText,
    final String? errorText,
    final String? labelText,
    final TextStyle? hintStyle,
    final Widget? trailingIcon,
    final Widget? leadingIcon,
    final Color? bgColor,
    final bool? obscureText,
    final ValueChanged<String>? onChanged,
    final bool? isShowBorder,
    final bool? isEnable,
    final EdgeInsetsGeometry? contentPadding,
  }) : super(
          inputController: inputController,
          textInputType: textInputType,
          hintText: hintText,
          hintStyle: hintStyle,
          trailingWidgets: trailingIcon,
          leadingWidgets: leadingIcon,
          errorText: errorText,
          labelText: labelText,
          isEnableInput: isEnable,
          bgColor: bgColor ?? AppColors.textBackground,
          contentPadding: contentPadding ?? EdgeInsets.only(left: 20, bottom: 16, top: 16),
          onChanged: onChanged,
          textFieldBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.textBackground, width: AppDimens.divider),
            borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
          ),
          obscureText: obscureText ?? false,
          isShowBorder: isShowBorder ?? false,
        );
}
