import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field.dart';

class AppTextFieldInput extends AppTextField {
  AppTextFieldInput({
    final TextEditingController? inputController,
    final TextInputType? textInputType,
    final String? hintText,
    final String? descriptionTitle,
    final bool? isRequite,
    final bool? isEnableInput,
    final TextStyle? hintStyle,
    final Widget? trailingWidgets,
    final Widget? leadingWidgets,
    final Widget? leadingText,
    final double? textFormFieldHeight,
  }) : super(
          inputController: inputController,
          textInputType: textInputType,
          hintText: hintText,
          isEnableInput: isEnableInput,
          hintStyle: hintStyle,
          trailingWidgets: trailingWidgets,
          leadingWidgets: leadingWidgets,
          leadingText: leadingText,
          contentPadding: EdgeInsets.only(left: 0),
          bgColor: Colors.white,
          focusTextFieldBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
          ),
          isShowBorder: true,
          descriptionWidgets: Visibility(
            visible: descriptionTitle != null && descriptionTitle != '',
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 8),
              child: _descriptionWidget(descriptionTitle, isRequite),
            ),
          ),
        );

  static Widget _descriptionWidget(String? descriptionTitle, bool? isRequired) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: descriptionTitle ?? '', style: AppTextStyle.nearBlackS14W600),
          if (isRequired ?? false) _requireCharacter(),
        ],
      ),
    );
  }

  static WidgetSpan _requireCharacter() {
    return WidgetSpan(
      child: Transform.translate(
        offset: const Offset(0, 0),
        child: Text(
          '*',
          textScaleFactor: 1,
          style: TextStyle(color: AppColors.buttonBGRed),
        ),
      ),
    );
  }
}
