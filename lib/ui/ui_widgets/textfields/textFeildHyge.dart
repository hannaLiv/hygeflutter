import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFeildComponent extends StatefulWidget {
  const TextFeildComponent({ Key ?key,this.label,this.labelSize=18,@required this.controller,this.isPassword = false,
    this.hintText ="Nhập giá trị",this.onChange,this.maxLine=1,this.enable=true,this.keyboardType = TextInputType.text,
    this.editingSuccess,this.leftChild
  }) : super(key: key);
  final String ?label;
  final double ?labelSize;
  final TextEditingController ?controller;
  final bool ?isPassword;
  final String ?hintText;
  final Function()? onChange;
  final int ?maxLine;
  final bool ?enable;
  final TextInputType ?keyboardType;
  final Void Function() ?editingSuccess;
  final Widget ?leftChild;
  @override
  _TextFeildComponentState createState() => _TextFeildComponentState();
}

class _TextFeildComponentState extends State<TextFeildComponent> {
  Size ?size;
  bool obscureText = false;

  @override
  void initState() {
    super.initState();
    if(widget.isPassword??false){
      obscureText = true;
    }
  }
  void _onPressRightIcon(){
    if(widget.isPassword??false){
      setState(() {
          obscureText = !obscureText;
      });
    }
  }

  
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: (){

      },
      child: Container(
        width: size?.width,
        //padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildLabel(),
            _buildInput()
          ],
        ),
      ),
    );
  }

  Widget _buildLabel(){
    if(widget.label != null && widget.label!.isNotEmpty){
      return Container(
        //margin: EdgeInsets.only(bottom: 10),
        child: Text(widget.label!,
          style: TextStyle(fontSize: widget.labelSize,color: Colors.orange,fontWeight: FontWeight.w600),
        ),
      );
    }
    else{
      return const SizedBox();
    }
  }
  Widget _buildInput(){
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide( //                   <--- left side
            color: Colors.grey[400]!,
            width: 2,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          (widget.leftChild!=null)?widget.leftChild!:SizedBox(),
          Expanded(
            child: TextFormField(
              onChanged:(v){
                widget.onChange!();
                if(widget.isPassword??false){
                }
              },
              inputFormatters: widget.isPassword!?[FilteringTextInputFormatter.allow(RegExp("[A-Za-z0-9\@\#\$]"))]:null,
              controller: widget.controller,
              enabled: widget.enable,
              onEditingComplete: widget.editingSuccess,
              keyboardType: widget.keyboardType,
              maxLines: widget.maxLine,
              style: TextStyle(color: Colors.grey[600],fontSize: 18),
              decoration: InputDecoration(
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                focusedErrorBorder: InputBorder.none,
                border: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                suffixIcon: widget.isPassword!?IconButton(
                  onPressed: _onPressRightIcon,
                  icon:_handleRightIcon() //SvgPicture.asset("assets/images/icons/Vector.svg",width: 21,),
                ):null,
                hintText: widget.hintText,
                hintStyle: TextStyle(color: Colors.grey[300],fontSize: 18,fontWeight: FontWeight.w500),
              ),
            ),
          )
        ],
      )
    );
  }

  Widget _handleRightIcon(){
    if(widget.isPassword!){
      if(obscureText){
        return Icon(Icons.remove_red_eye_outlined,size: 20,);
      }
      else{
        return Icon(Icons.remove_red_eye_rounded,size: 20,);
      }
      
    }
    return const SizedBox();
  }

}