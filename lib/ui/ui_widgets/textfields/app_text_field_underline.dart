import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field.dart';

class AppTextFieldUnderline extends AppTextField {
  AppTextFieldUnderline({
    final TextEditingController? inputController,
    final TextInputType? textInputType,
    final String? hintText,
    final String? descriptionTitle,
    final bool? isRequite,
    final bool? isEnableInput,
    final TextStyle? hintStyle,
    final Widget? trailingWidgets,
    final Widget? leadingWidgets,
    final Widget? leadingText,
  }) : super(
          inputController: inputController,
          textInputType: textInputType,
          hintText: hintText,
          isEnableInput: isEnableInput,
          hintStyle: hintStyle,
          trailingWidgets: trailingWidgets,
          leadingWidgets: leadingWidgets,
          leadingText: leadingText,
          contentPadding: EdgeInsets.zero,
          bgColor: AppColors.background,
          focusTextFieldBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.borderBlack, width: AppDimens.divider),
          ),
          textFieldBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.disableBorder, width: AppDimens.divider),
          ),
          descriptionWidgets: Visibility(
            visible: descriptionTitle != null && descriptionTitle != '',
            child: Container(
              width: double.infinity,
              child: _descriptionWidget(descriptionTitle, isRequite),
            ),
          ),
        );

  static Widget _descriptionWidget(String? descriptionTitle, bool? isRequired) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: descriptionTitle ?? '', style: AppTextStyle.nearBlackS13W600),
          if (isRequired ?? false) _requireCharacter(),
        ],
      ),
    );
  }

  static WidgetSpan _requireCharacter() {
    return WidgetSpan(
      child: Transform.translate(
        offset: const Offset(0, 0),
        child: Text(
          '*',
          textScaleFactor: 1,
          style: TextStyle(color: AppColors.buttonBGRed),
        ),
      ),
    );
  }
}
