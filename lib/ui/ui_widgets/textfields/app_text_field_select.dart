import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field.dart';

class AppTextFieldSelect extends AppTextField {
  AppTextFieldSelect({
    final Widget? trailingIcon,
    final Widget? leadingWidgets,
    final String? hintText,
    final TextStyle? hintStyle,
    final String? descriptionTitle,
    final bool? isRequite,
    final VoidCallback? onTextFieldTap,
    final Widget? leadingText,
    final double? textFieldHeight,
  }) : super(
          trailingWidgets: trailingIcon,
          leadingWidgets: leadingWidgets,
          hintText: hintText,
          leadingText: leadingText,
          hintStyle: hintStyle,
          isEnableInput: false,
          contentPadding: EdgeInsets.only(left: 0),
          onTextFieldTap: onTextFieldTap,
          bgColor: AppColors.background,
          descriptionWidgets: Visibility(
            visible: descriptionTitle != null && descriptionTitle != '',
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 15),
              child: _descriptionWidget(descriptionTitle, isRequite),
            ),
          ),
        );

  static Widget _descriptionWidget(String? descriptionTitle, bool? isRequired) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: descriptionTitle ?? '', style: AppTextStyle.blackS13W600),
          if (isRequired ?? false) _requireCharacter(),
        ],
      ),
    );
  }

  static WidgetSpan _requireCharacter() {
    return WidgetSpan(
      child: Transform.translate(
        offset: const Offset(0, 0),
        child: Text(
          '*',
          textScaleFactor: 1,
          style: TextStyle(color: AppColors.buttonBGRed),
        ),
      ),
    );
  }
}
