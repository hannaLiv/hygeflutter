import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class AppTextField extends StatefulWidget {
  final TextEditingController? inputController;
  final ValueChanged<String>? onChanged;
  final TextInputType? textInputType;
  final String? hintText;
  final String? errorText;
  final String? labelText;
  final Widget? descriptionWidgets;
  final Color? bgColor;
  final Widget? trailingWidgets;
  final Widget? leadingWidgets;
  final Widget? leadingText;
  final bool? isEnableInput;
  final bool? obscureText;
  final TextStyle? hintStyle;
  final InputBorder? textFieldBorder;
  final InputBorder? focusTextFieldBorder;
  final EdgeInsetsGeometry? contentPadding;
  final VoidCallback? onTextFieldTap;
  final FormFieldValidator<String>? validator;
  final List<TextInputFormatter>? inputFormatter;
  final bool isShowBorder;
  final double? textFormFieldHeight;

  AppTextField({
    this.inputController,
    this.onChanged,
    this.textInputType,
    this.hintText,
    this.bgColor,
    this.trailingWidgets,
    this.hintStyle,
    this.leadingWidgets,
    this.descriptionWidgets,
    this.isEnableInput,
    this.textFieldBorder,
    this.contentPadding,
    this.onTextFieldTap,
    this.obscureText,
    this.validator,
    this.inputFormatter,
    this.leadingText,
    this.errorText,
    this.labelText,
    this.isShowBorder = false,
    this.focusTextFieldBorder,
    this.textFormFieldHeight,
  });

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  FocusNode myFocusNode = FocusNode();

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final checkTextStyle = widget.inputController?.text != null && widget.inputController?.text != '';
    return GestureDetector(
      onTap: widget.onTextFieldTap,
      child: Container(
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(color: widget.leadingText != null ? AppColors.disableBorder : AppColors.background, width: AppDimens.divider),
        )),
        child: Column(
          children: [
            widget.descriptionWidgets ?? SizedBox(),
            Row(
              children: [
                Expanded(flex: widget.leadingText != null ? 1 : 0, child: widget.leadingText ?? SizedBox()),
                Expanded(
                  child: Container(

                    height: widget.textFormFieldHeight ?? null,
                    child: TextFormField(
                      controller: widget.inputController,
                      validator: widget.validator,
                      onTap: _changeStyleWhenFocus,
                      focusNode: myFocusNode,
                      inputFormatters: widget.inputFormatter,
                      decoration: InputDecoration(
                        errorText: widget.hintText != '' ? widget.errorText : '',
                        errorStyle: AppTextStyle.redTextS12W400,
                        labelText: widget.hintText != '' ? widget.labelText : '',
                        labelStyle: checkTextStyle || myFocusNode.hasFocus ? AppTextStyle.nearBlackS13W600 : AppTextStyle.greyS14,
                        enabled: widget.isEnableInput ?? true,
                        counterText: '',
                        hintText: widget.hintText ?? '',
                        hintStyle: widget.hintStyle ?? AppTextStyle.greyS14,
                        contentPadding: widget.contentPadding,
                        fillColor: widget.bgColor,
                        filled: true,
                        border: widget.isShowBorder
                            ? OutlineInputBorder(
                                borderSide: BorderSide.none,
                              )
                            : null,
                        errorBorder: widget.errorText == null && widget.errorText == ''
                            ? null
                            : OutlineInputBorder(
                                borderSide: widget.isShowBorder ? BorderSide(color: AppColors.buttonBGRed, width: AppDimens.divider) : BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
                              ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
                          borderSide: widget.isShowBorder ? BorderSide(color: widget.errorText != "" && widget.errorText != null ? AppColors.buttonBGRed : AppColors.buttonViolet, width: AppDimens.divider) : BorderSide.none,
                        ),
                        focusedBorder: widget.focusTextFieldBorder ??
                            OutlineInputBorder(
                              borderSide: BorderSide(color: AppColors.buttonViolet, width: AppDimens.divider),
                              borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
                            ),
                        enabledBorder: widget.textFieldBorder ??
                            OutlineInputBorder(
                              borderSide: widget.isShowBorder ? BorderSide.none : BorderSide(color: AppColors.border, width: AppDimens.divider),
                              borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
                            ),
                        prefixIcon: widget.leadingWidgets,
                        suffixIcon: widget.leadingText != null ? null : widget.trailingWidgets,
                      ),
                      keyboardType: widget.textInputType,
                      onChanged: widget.onChanged,
                      style: AppTextStyle.nearBlackS14W400,
                      obscureText: widget.obscureText ?? false,
                    ),
                  ),
                ),
                widget.leadingText != null ? widget.trailingWidgets ?? SizedBox() : SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _changeStyleWhenFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(myFocusNode);
    });
  }
}

// class AppTextField extends StatelessWidget {
//   final TextEditingController? inputController;
//   final ValueChanged<String>? onChanged;
//   final TextInputType? textInputType;
//   final String? hintText;
//   final String? errorText;
//   final String? labelText;
//   final Widget? descriptionWidgets;
//   final Color? bgColor;
//   final Widget? trailingWidgets;
//   final Widget? leadingWidgets;
//   final Widget? leadingText;
//   final bool? isEnableInput;
//   final bool? obscureText;
//   final TextStyle? hintStyle;
//   final InputBorder? textFieldBorder;
//   final EdgeInsetsGeometry? contentPadding;
//   final VoidCallback? onTextFieldTap;
//   final FormFieldValidator<String>? validator;
//   final List<TextInputFormatter>? inputFormatter;
//   final bool isShowBorder;
//
//   AppTextField({
//     this.inputController,
//     this.onChanged,
//     this.textInputType,
//     this.hintText,
//     this.bgColor,
//     this.trailingWidgets,
//     this.hintStyle,
//     this.leadingWidgets,
//     this.descriptionWidgets,
//     this.isEnableInput,
//     this.textFieldBorder,
//     this.contentPadding,
//     this.onTextFieldTap,
//     this.obscureText,
//     this.validator,
//     this.inputFormatter,
//     this.leadingText,
//     this.errorText,
//     this.labelText,
//     this.isShowBorder = false,
//   });
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: onTextFieldTap,
//       child: Container(
//         decoration: BoxDecoration(
//             border: Border(
//           bottom: BorderSide(color: leadingText != null ? AppColors.border : AppColors.background, width: AppDimens.divider),
//         )),
//         child: Column(
//           children: [
//             descriptionWidgets ?? SizedBox(),
//             Row(
//               children: [
//                 Expanded(flex: leadingText != null ? 1 : 0, child: leadingText ?? SizedBox()),
//                 Expanded(
//                   child: Container(
//                     child: TextFormField(
//                       controller: inputController,
//                       validator: validator,
//                       inputFormatters: inputFormatter,
//                       decoration: InputDecoration(
//                         errorText: hintText != '' ? errorText : '',
//                         labelText: hintText != '' ? labelText : '',
//                         labelStyle: isFocused?? false ? AppTextStyle.nearBlackS13W600 : AppTextStyle.lightVioletS14,
//                         enabled: isEnableInput ?? true,
//                         counterText: '',
//                         hintText: hintText ?? '',
//                         hintStyle: hintStyle ?? AppTextStyle.lightVioletS14,
//                         contentPadding: contentPadding,
//                         fillColor: bgColor,
//                         filled: true,
//                         border: textFieldBorder ??
//                             UnderlineInputBorder(
//                               borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
//                               borderSide: leadingText != null ? BorderSide.none : BorderSide(color: AppColors.border, width: AppDimens.divider),
//                             ),
//                         errorBorder: errorText == null && errorText == ''
//                             ? null
//                             : OutlineInputBorder(
//                                 borderSide: isShowBorder ? BorderSide(color: AppColors.border, width: AppDimens.divider) : BorderSide.none,
//                                 borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
//                               ),
//                         focusedErrorBorder: OutlineInputBorder(
//                           borderRadius: BorderRadius.all(Radius.circular(AppDimens.cornerRadius15)),
//                           borderSide: isShowBorder ? BorderSide(color: Colors.blue, width: AppDimens.divider) : BorderSide.none,
//                         ),
//                         prefixIcon: leadingWidgets,
//                         suffixIcon: leadingText != null ? null : trailingWidgets,
//                       ),
//                       keyboardType: textInputType,
//                       onChanged: onChanged,
//                       style: AppTextStyle.nearBlackS14W400,
//                       obscureText: obscureText ?? false,
//                     ),
//                   ),
//                 ),
//                 leadingText != null ? trailingWidgets ?? SizedBox() : SizedBox(),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
