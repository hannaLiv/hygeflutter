import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

import '../app_circular_progress_indicator.dart';

class AppButton extends StatelessWidget {
  final String? title;
  final Widget? leadingIcon;
  final Widget? trailingIcon;

  final bool isLoading;
  final bool isEnable;

  final double? height;
  final double? width;
  final double? borderWidth;
  final double? cornerRadius;

  final Color? backgroundColor;
  final Color? borderColor;

  final TextStyle? textStyle;

  final VoidCallback? onPressed;
  final MainAxisAlignment? alignmentItem;

  AppButton({
    this.title,
    this.leadingIcon,
    this.trailingIcon,
    this.isLoading = false,
    this.isEnable = true,
    this.height,
    this.width,
    this.borderWidth,
    this.cornerRadius,
    this.backgroundColor,
    this.borderColor,
    this.textStyle,
    this.onPressed,
    this.alignmentItem,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? AppDimens.buttonHeight,
      width: width ?? double.infinity,
      child: ElevatedButton(
        child: _buildChildWidget(),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(cornerRadius ?? AppDimens.cornerRadius15),
          ),
          side: BorderSide(
            color: borderColor ?? Colors.transparent,
            width: borderWidth ?? 0,
          ),
          primary: isEnable ? backgroundColor : AppColors.greyItemColor,
          padding: EdgeInsets.all(0),
        ),
        onPressed: isEnable ? onPressed : null,
      ),
    );
  }

  Widget _buildChildWidget() {
    if (isLoading) {
      return AppCircularProgressIndicator(color: Colors.white);
    } else {
      return Row(
        mainAxisAlignment: alignmentItem ?? MainAxisAlignment.center,
        children: [
          leadingIcon ?? Container(),
          title != null
              ? Text(
                  title!,
                  style: textStyle ??
                      TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w800,
                        color: Colors.red,
                      ),
                )
              : Container(),
          trailingIcon ?? Container(),
        ],
      );
    }
  }
}

class AppRedButton extends AppButton {
  AppRedButton({
    String title = '',
    bool isLoading = false,
    VoidCallback? onPressed,
    double cornerRadius = 9.0,
    TextStyle? textStyle,
    Color? backgroundColor = AppColors.buttonBGRed,
    Widget? trailingIcon,
  }) : super(
          title: title,
          isLoading: isLoading,
          onPressed: onPressed,
          cornerRadius: cornerRadius,
          textStyle: textStyle ?? AppTextStyle.whiteS16W600,
          backgroundColor: backgroundColor,
          trailingIcon: trailingIcon,
        );
}

class AppBlueButton extends AppButton {
  AppBlueButton({
    String title = '',
    bool isLoading = false,
    VoidCallback? onPressed,
    double cornerRadius = 5.0,
    TextStyle? textStyle,
    Widget? leadingIcon,
    Widget? trailingIcon,
    MainAxisAlignment? alignmentItem,
  }) : super(
          title: title,
          isLoading: isLoading,
          onPressed: onPressed,
          cornerRadius: cornerRadius,
          textStyle: textStyle ?? AppTextStyle.whiteS16W600,
          backgroundColor: AppColors.lightGreyColor,
          leadingIcon: leadingIcon,
          trailingIcon: trailingIcon,
          alignmentItem: alignmentItem,
        );
}

class AppWhiteButton extends AppButton {
  AppWhiteButton({
    String title = '',
    bool isLoading = false,
    VoidCallback? onPressed,
    double cornerRadius = 9.0,
    TextStyle? textStyle,
    Widget? leadingIcon,
    Widget? trailingIcon,
    MainAxisAlignment? alignmentItem,
  }) : super(
          title: title,
          isLoading: isLoading,
          onPressed: onPressed,
          cornerRadius: cornerRadius,
          textStyle: textStyle ?? AppTextStyle.whiteS16W600,
          backgroundColor: AppColors.background,
          leadingIcon: leadingIcon,
          trailingIcon: trailingIcon,
          alignmentItem: alignmentItem,
        );
}

class AppVioletButton extends AppButton {
  AppVioletButton({
    String title = '',
    bool isLoading = false,
    VoidCallback? onPressed,
    double cornerRadius = 9.0,
    TextStyle? textStyle,
    Widget? trailingIcon,
    bool isEnable = false,
  }) : super(
          title: title,
          isLoading: isLoading,
          onPressed: onPressed,
          cornerRadius: cornerRadius,
          textStyle: textStyle ?? AppTextStyle.whiteS16W600,
          backgroundColor: AppColors.buttonViolet,
          trailingIcon: trailingIcon,
          isEnable: isEnable,
        );
}

class AppOrangeButton extends AppButton {
  AppOrangeButton({
    String title = '',
    bool isLoading = false,
    VoidCallback? onPressed,
    double cornerRadius = 5.0,
    TextStyle? textStyle,
    Widget? leadingIcon,
    Widget? trailingIcon,
    MainAxisAlignment? alignmentItem,
    bool? isEnable,
  }) : super(
          title: title,
          isLoading: isLoading,
          onPressed: onPressed,
          cornerRadius: cornerRadius,
          textStyle: textStyle ?? AppTextStyle.whiteS16W600,
          backgroundColor: Colors.orange,
          leadingIcon: leadingIcon,
          trailingIcon: trailingIcon,
          alignmentItem: alignmentItem,
          isEnable: isEnable ?? true,
        );
}
