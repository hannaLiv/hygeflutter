import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/utils/utils.dart';

class LoadingIndicatorWidget extends StatelessWidget {
  const LoadingIndicatorWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Utils.hideKeyBoard(context: context);
    return Positioned(
      top: 0,
      right: 0,
      left: 0,
      bottom: 0,
      child: Container(
        color: Colors.grey.withOpacity(0.8),
        child: Center(
          child: Container(
            width: 120,
            height: 120,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: AppShadow.boxShadow,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                Container(
                  height: 20,
                ),
                Text(S.current.common_button_loading),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
