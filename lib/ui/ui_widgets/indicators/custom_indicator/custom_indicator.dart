import 'package:flutter/material.dart';

import 'draw_indicator.dart';
import 'shape.dart';

enum IndicatorAlign {
  top,
  center,
  bottom,
}

class CustomIndicatorWidget extends StatefulWidget {
  final Widget child;

  final int length;

  final EdgeInsets padding;

  final IndicatorAlign align;

  final IndicatorShape shape;

  final Color indicatorColor;

  final Color indicatorSelectorColor;
  final Color selectedBiggerColor;

  final double indicatorSpace;

  const CustomIndicatorWidget({
    Key? key,
    required this.child,
    required this.length,
    this.padding = const EdgeInsets.only(bottom: 10.0, top: 10.0),
    this.align = IndicatorAlign.bottom,
    this.indicatorColor = Colors.white,
    this.indicatorSelectorColor = Colors.grey,
    this.indicatorSpace = 15.0,
    this.shape = IndicatorShape.defaultCircle,
    this.selectedBiggerColor = Colors.red,
  }) : super(key: key);

  @override
  CustomIndicatorWidgetState createState() => CustomIndicatorWidgetState();
}

class CustomIndicatorWidgetState extends State<CustomIndicatorWidget> {
  double? currentPage;

  @override
  Widget build(BuildContext context) {
    if (widget.child is! PageView) {
      return widget.child;
    }

    double height = widget.shape.height;

    final initPage = pageView.controller.initialPage;

    final currentPage = this.currentPage ?? initPage.toDouble();

    Widget indicator = DrawIndicator(
      length: widget.length,
      color: widget.indicatorColor,
      selectedColor: widget.indicatorSelectorColor,
      indicatorSpace: widget.indicatorSpace,
      indicatorShape: widget.shape,
      align: widget.align,
      reverse: pageView.reverse,
      currentPage: currentPage,
      initialPage: initPage.toDouble(),
      selectedBiggerColor: widget.selectedBiggerColor,
    );

    var align = widget.align;

    if (align == IndicatorAlign.bottom) {
      indicator = Positioned(
        left: 0.0,
        right: 0.0,
        bottom: widget.padding.bottom,
        height: height,
        child: indicator,
      );
    } else if (align == IndicatorAlign.top) {
      indicator = Positioned(
        left: 0.0,
        right: 0.0,
        top: widget.padding.top,
        height: height,
        child: indicator,
      );
    } else if (align == IndicatorAlign.center) {
      indicator = Positioned(
        left: 0,
        right: 0,
        top: MediaQuery.of(context).size.height / 2.05,
        child: Center(
          child: Container(
            child: indicator,
            height: height,
          ),
        ),
      );
    }

    return Stack(
      children: <Widget>[
        NotificationListener<ScrollNotification>(
          child: pageView,
          onNotification: _onScroll,
        ),
        indicator,
      ],
    );
  }

  PageView get pageView => widget.child as PageView;

  bool _onScroll(ScrollNotification notification) {
    if (notification.metrics is PageMetrics) {
      final PageMetrics metrics = notification.metrics as PageMetrics;
      currentPage = metrics.page;
      setState(() {});
    }
    return false;
  }

  void forceRefreshState() {
    this.setState(() {});
  }
}
