import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';

class AppCheckBox extends StatefulWidget {
  final bool? value;
  final ValueChanged<bool?>? onChanged;

  AppCheckBox({this.value, this.onChanged});

  @override
  _AppCheckBoxState createState() => _AppCheckBoxState();
}

class _AppCheckBoxState extends State<AppCheckBox> {
  bool isChecked = false;

  @override
  void initState() {
    setState(() {
      isChecked = widget.value ?? false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isChecked = !isChecked;
        });
        widget.onChanged?.call(isChecked);
      },
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(width: 1.2, color: AppColors.checkBoxBackground),
            color: isChecked ? AppColors.checkBoxBackground : AppColors.background,
          ),
          width: 23,
          height: 23,
          child: Visibility(
            visible: isChecked,
            child: Container(
              width: 16,
              height: 12,
              child: Image.asset(AppImages.icWhiteCheckbox),
            ),
          )),
    );
  }
}
