import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class AppTextView extends StatelessWidget {
  final String? value;
  final String? description;
  final bool isRequired;

  AppTextView({
    this.value,
    this.description,
    this.isRequired = false,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        description != null ? _descriptionWidget() : SizedBox(),
        SizedBox(height: 8),
        Text(
          value ?? '',
          style: AppTextStyle.greyS12W400,
        )
      ],
    );
  }

  Widget _descriptionWidget() {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: description ?? '', style: AppTextStyle.nearBlackS14W600),
          if (isRequired) _requireCharacter(),
        ],
      ),
    );
  }

  WidgetSpan _requireCharacter() {
    return WidgetSpan(
      child: Transform.translate(
        offset: const Offset(0, 0),
        child: Text(
          '*',
          textScaleFactor: 1,
          style: TextStyle(color: AppColors.buttonBGRed),
        ),
      ),
    );
  }
}
