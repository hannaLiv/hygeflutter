import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_images.dart';

class SearchBarWidget extends StatelessWidget {
  final TextEditingController? searchController;
  final String? hintText;
  final TextStyle? textStyle;
  final VoidCallback? onSearchTap;
  final bool isActiveInput;

  SearchBarWidget({
    this.searchController,
    this.hintText,
    this.textStyle,
    this.onSearchTap,
    this.isActiveInput = true,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onSearchTap,
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            border: Border.all(color: AppColors.greyItemColor.withOpacity(0.7)),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, right: 12),
                width: 14,
                height: 14,
                child: Image.asset(
                  AppImages.icSearch,
                  color: AppColors.textBlack343434,
                ),
              ),
              Expanded(
                child: TextFormField(
                  controller: searchController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: hintText,
                    hintStyle: textStyle,
                    enabled: isActiveInput,
                  ),
                  keyboardType: TextInputType.text,
                  style: textStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
