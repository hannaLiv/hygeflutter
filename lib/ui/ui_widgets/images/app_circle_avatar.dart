import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/utils/picker_utils.dart';

class AppCircleAvatar extends StatelessWidget {
  final String url;
  final double? size;
  final OnImagePicked? pickedImage;
  final bool showPickIcon;

  AppCircleAvatar({
    this.url = '',
    this.size,
    this.pickedImage,
    this.showPickIcon = true,
  });

  @override
  Widget build(BuildContext context) {
    bool isValidUrl = Uri.tryParse(url)?.isAbsolute == true;
    return Stack(
      children: [
        Container(
          width: size ?? double.infinity,
          height: size ?? double.infinity,
          child: isValidUrl
              ? ClipRRect(
                  child: CachedNetworkImage(
                    imageUrl: url,
                    progressIndicatorBuilder: (context, url, downloadProgress) {
                      return Container(
                        width: size,
                        height: size,
                        child: CircularProgressIndicator(
                          value: downloadProgress.progress,
                          strokeWidth: 2,
                        ),
                      );
                    },
                    errorWidget: (context, url, error) {
                      return Container(
                        width: double.infinity,
                        height: double.infinity,
                        child: Image.asset(
                          AppImages.icAvatar,
                          fit: BoxFit.fill,
                        ),
                      );
                    },
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.circular((size ?? 0) / 2),
                )
              : ClipRRect(
                  borderRadius: BorderRadius.circular((size ?? 0) / 2),
                  child: Container(
                    width: size,
                    height: size,
                    color: AppColors.buttonViolet,
                    child: Image.asset(
                      url,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
          decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular((size ?? 0) / 2),
          ),
        ),
        pickerIcon(context),
      ],
    );
  }

  Widget pickerIcon(BuildContext context) {
    return Visibility(
      visible: showPickIcon,
      child: Positioned(
        bottom: 0,
        right: 0,
        child: GestureDetector(
          onTap: () => _imagePicker(context),
          child: Container(
            width: 25,
            height: 25,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(AppDimens.buttonCornerRadius90),
              boxShadow: AppShadow.redShadow,
              color: AppColors.buttonViolet,
            ),
            child: Image.asset(
              AppImages.icAddImages,
            ),
          ),
        ),
      ),
    );
  }

  void _imagePicker(BuildContext context) {
    PickerUtils.showImagePickType(context, onImagePicked: (imagePicked) => pickedImage?.call(imagePicked));
  }
}
