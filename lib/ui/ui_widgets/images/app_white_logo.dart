import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class AppWhiteLogo extends StatelessWidget {
  const AppWhiteLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppDimens.logoWidth,
      height: AppDimens.logoHeight,
      child: Image.asset(AppImages.whiteLogo),
    );
  }
}
