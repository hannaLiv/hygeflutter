import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/utils/picker_utils.dart';

class ImagePickerWidget extends StatelessWidget {
  final OnImagePicked? onImagePicked;

  const ImagePickerWidget({this.onImagePicked});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      margin: EdgeInsets.only(top: 15),
      child: Column(
        children: [
          Center(
              child: Text(
            S.of(context).choose_type_image_picker,
            style: AppTextStyle.blackS14Bold,
          )),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () async {
                  Navigator.pop(context);
                  final result = await PickerUtils.pickerImage(context, isCamera: true);
                  onImagePicked?.call(result ?? XFile(AppImages.icAvatar));
                },
                child: Container(
                  width: 100,
                  height: 70,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: AppColors.greyTextColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.camera),
                      SizedBox(height: 5),
                      Text(S.of(context).camera_title),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              GestureDetector(
                onTap: () async {
                  Navigator.pop(context);
                  final result = await PickerUtils.pickerImage(context, isCamera: false);
                  onImagePicked?.call(result ?? XFile(AppImages.icAvatar));
                },
                child: Container(
                  width: 100,
                  height: 70,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: AppColors.greyTextColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.add_photo_alternate),
                      SizedBox(height: 5),
                      Text(S.of(context).gallery_title),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
