import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

typedef OnNumberPicked(double value);

class PickerNumber extends StatefulWidget {
  final int startNumber;
  final int endNumber;
  final double? selectedValue;
  final OnNumberPicked? onItemSelected;

  PickerNumber(
    this.startNumber,
    this.endNumber, {
    this.selectedValue = 0,
    this.onItemSelected,
  });

  @override
  _PickerNumberState createState() => _PickerNumberState();
}

class _PickerNumberState extends State<PickerNumber> {
  final List<String> _number = [];
  int _selectedIndex = 0;

  final List<String> _steps = [];
  int _selectedStep = 0;

  @override
  void initState() {
    for (int i = widget.startNumber; i <= widget.endNumber; i += 1) {
      _number.add(i.toString());
    }
    for (int i = 0; i <= 9; i += 1) {
      _steps.add(i.toString());
    }

    if (widget.selectedValue != null) {
      List<int?> values = widget.selectedValue.toString().split('.').map((text) => int.tryParse(text)).toList();

      if (values.length > 0) {
        _selectedIndex = _number.indexOf(values[0].toString());
        if (values.length == 2) {
          _selectedStep = _steps.indexOf(values[1].toString());
        }
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double kHeight = MediaQuery.of(context).size.height / 3;

    return Container(
      height: kHeight,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              height: 52,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CupertinoButton(
                    pressedOpacity: 0.3,
                    onPressed: () => Get.back(),
                    child: Text('Cancel', style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ),
                  CupertinoButton(
                    pressedOpacity: 0.3,
                    onPressed: () {
                      double value = double.tryParse(_number[_selectedIndex])! + double.tryParse(_steps[_selectedStep])! * 0.1;
                      widget.onItemSelected?.call(value);
                      Get.back();
                    },
                    child: Text('OK', style: TextStyle(color: Colors.blue, fontSize: 16)),
                  ),
                ],
              ),
              color: Colors.white,
            ),
            Expanded(
              child: Container(
                height: kHeight - 52,
                color: AppColors.background,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 120,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        itemExtent: 30,
                        scrollController: FixedExtentScrollController(
                          initialItem: _selectedIndex,
                        ),
                        onSelectedItemChanged: (int index) {
                          _selectedIndex = index;
                        },
                        children: List<Widget>.generate(
                          _number.length,
                          (int index) {
                            final value = _number[index];
                            return Center(
                              child: Text(
                                value,
                                style: TextStyle(fontSize: 16),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Center(
                      child: Container(
                        height: 20.0,
                        child: Text(
                          '.',
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w700,
                            color: AppColors.textBlack221F1F,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Container(
                      width: 120,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        itemExtent: 30,
                        scrollController: FixedExtentScrollController(
                          initialItem: _selectedStep,
                        ),
                        onSelectedItemChanged: (int index) {
                          _selectedStep = index;
                        },
                        children: List<Widget>.generate(
                          _steps.length,
                          (int index) {
                            final value = _steps[index];
                            return Center(
                              child: Text(
                                value,
                                style: TextStyle(fontSize: 16),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
