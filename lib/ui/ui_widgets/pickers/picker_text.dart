import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_text_styles.dart';

typedef ItemPicked(int index);

class PickerText extends StatefulWidget {
  final List<String> itemsString;
  final int? selectedIndex;
  final ItemPicked? onItemSelected;
  final double? itemExtent;

  PickerText(
    this.itemsString, {
    this.selectedIndex = 0,
    this.onItemSelected,
    this.itemExtent,
  });

  @override
  _PickerTextState createState() => _PickerTextState();
}

class _PickerTextState extends State<PickerText> {
  int _selectedIndex = 0;

  @override
  void initState() {
    _selectedIndex = widget.selectedIndex!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double kHeight = MediaQuery.of(context).size.height / 3;
    return Container(
      height: kHeight,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CupertinoButton(
                    pressedOpacity: 0.3,
                    onPressed: () => Get.back(),
                    child: Text('Cancel', style: AppTextStyle.blackS18Bold),
                  ),
                  CupertinoButton(
                    pressedOpacity: 0.3,
                    onPressed: () {
                      widget.onItemSelected?.call(_selectedIndex);
                      Get.back();
                    },
                    child: Text('Done', style: AppTextStyle.blackS18Bold),
                  ),
                ],
              ),
              color: Colors.white,
            ),
            Expanded(
              child: Container(
                height: kHeight - 52,
                child: CupertinoPicker(
                  backgroundColor: Colors.white,
                  itemExtent: widget.itemExtent ?? 30,
                  scrollController: FixedExtentScrollController(
                    initialItem: _selectedIndex,
                  ),
                  onSelectedItemChanged: (int index) {
                    _selectedIndex = index;
                  },
                  children: List<Widget>.generate(
                    widget.itemsString.length,
                    (int index) {
                      final value = widget.itemsString[index];
                      return Center(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 30.0),
                          child: Text(
                            value,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
