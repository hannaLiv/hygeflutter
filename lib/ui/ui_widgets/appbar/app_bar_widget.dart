import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twinkle_colors_flutter_app/commons/export_commons_imp.dart';

class AppBarWidget extends AppBar {
  AppBarWidget({
    Key? key,
    VoidCallback? onBackPressed,
    Widget? title,
    List<Widget> rightActions = const [],
    Brightness? brightness,
    Color? backgroundColor,
    Widget? body,
    Widget? leadingButton,
    bool? automaticallyImplyLeading,
    double? toolbarHeight,
  }) : super(
          key: key,
          title: title ?? Container(),
          toolbarHeight: toolbarHeight ?? 50,
          leading: leadingButton,
          actions: rightActions,
          brightness: brightness ?? Brightness.dark,
          backgroundColor: backgroundColor ?? AppColors.background,
          elevation: 0,
          automaticallyImplyLeading: automaticallyImplyLeading ?? true,
        );
}
