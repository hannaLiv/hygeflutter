import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:twinkle_colors_flutter_app/commons/app_colors.dart';
import 'package:twinkle_colors_flutter_app/generated/l10n.dart';
import 'package:twinkle_colors_flutter_app/ui/ui_widgets/textfields/app_text_field_corner.dart';

class AppDialog {
  static void defaultDialog({
    String title = "Alert",
    String message = "",
    String? textConfirm,
    String? textCancel,
    VoidCallback? onConfirm,
    VoidCallback? onCancel,
  }) {
    Get.defaultDialog(
      title: title,
      content: Text(
        message,
        textAlign: TextAlign.center,
      ),
      onConfirm: onConfirm == null
          ? null
          : () {
              Get.back();
              onConfirm.call();
            },
      onCancel: onCancel == null
          ? null
          : () {
              Get.back();
              onCancel.call();
            },
      textConfirm: textConfirm,
      textCancel: textCancel,
    );
  }

  static void errorDialog({
    String? title,
    String message = "",
    String? textConfirm,
    String? textCancel,
    VoidCallback? onConfirm,
    VoidCallback? onCancel,
  }) {
    Get.dialog(AlertDialog(
      title: Text(title ?? S.current.common_text_error),
      content: Text(message),
      actions: [
        textConfirm != null && textConfirm != ''
            ? GestureDetector(
                onTap: onConfirm,
                child: Container(
                  child: Text(textConfirm),
                ))
            : SizedBox(),
        textCancel != null && textCancel != ''
            ? GestureDetector(
                onTap: () => onCancel != null ? onCancel.call() : Get.back(),
                child: Container(
                  child: Text(textCancel),
                ))
            : SizedBox(),
      ],
    ));
  }

  static void showDatePicker(
    BuildContext context, {
    DateTime? minTime,
    DateTime? maxTime,
    DateChangedCallback? onConfirm,
    locale: LocaleType.en,
    DateTime? currentTime,
  }) {
    DatePicker.showDatePicker(
      context,
      minTime: minTime,
      maxTime: maxTime,
      onConfirm: onConfirm,
      locale: LocaleType.vi,
      currentTime: currentTime,
      theme: DatePickerTheme(),
    );
  }

  static void showDateTimePicker(
    BuildContext context, {
    DateTime? minTime,
    DateTime? maxTime,
    DateChangedCallback? onConfirm,
    locale: LocaleType.en,
    DateTime? currentTime,
  }) {
    DatePicker.showDateTimePicker(
      context,
      minTime: minTime,
      maxTime: maxTime,
      onConfirm: onConfirm,
      locale: LocaleType.vi,
      currentTime: currentTime,
      theme: DatePickerTheme(),
    );
  }

  static void showInputDialog(TextEditingController controller, {ValueChanged<String>? onChanged}) {
    Get.dialog(
      AlertDialog(
        content: Text("Input Address"),
        actions: [
          AppTextFieldCorner(
            inputController: controller,
            bgColor: AppColors.greyBgColor,
            isShowBorder: true,
            contentPadding: EdgeInsets.only(left: 10),
            onChanged: (text) => onChanged?.call(text),
          ),
          GestureDetector(
            onTap: () => Get.back(result: controller),
            child: Container(
              width: 50,
              height: 30,
              child: Center(child: Text("OK")),
            ),
          )
        ],
      ),
      barrierDismissible: false,
    );
  }
}
